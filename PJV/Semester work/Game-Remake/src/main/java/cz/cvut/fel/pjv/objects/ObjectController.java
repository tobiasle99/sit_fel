package cz.cvut.fel.pjv.objects;

import cz.cvut.fel.pjv.main.display.GamePanel;

import java.awt.*;
import java.util.ArrayList;

import static cz.cvut.fel.pjv.main.display.GamePanel.tileSize;

/**
 * This class is responsible for controlling all objects present in the game world, such as chests, finish lines, etc.
 * It manages the positions and visibility of each object and provides a method to draw all visible objects.
 */
public class ObjectController {
    ArrayList<SuperObject> allObjects;




    /**
     * Constructor that creates a new ObjectController object with default objects, such as chests and finish lines,
     * and sets their initial positions.
     */
    public ObjectController() {
        allObjects  = new ArrayList<>();

        OBJ_Chest chest1 = new OBJ_Chest("shield");
        OBJ_Chest chest2 = new OBJ_Chest("potion");
        FinishLine finishLine = new FinishLine();
        Trap trap = new Trap();

        trap.setPosition(new int[]{10 * tileSize, GamePanel.screenHeight - 5 * tileSize});
        chest1.setPosition(new int[]{25 * tileSize, GamePanel.screenHeight - 5 * tileSize});
        chest2.setPosition(new int[]{12 * tileSize, GamePanel.screenHeight - 5 * tileSize});
        finishLine.setPosition(new int[]{45*tileSize, GamePanel.screenHeight - 5 * tileSize});


        allObjects.add(chest1);
        allObjects.add(chest2);
        allObjects.add(finishLine);
        allObjects.add(trap);

    }

    /**
     * Constructor that creates a new ObjectController object with an array of SuperObjects passed as a parameter.
     * @param allObjects an array of SuperObjects
     */
    public ObjectController(ArrayList<SuperObject> allObjects) {
        this.allObjects = allObjects;
    }

    /**
     * Returns an array of all SuperObjects controlled by this ObjectController.
     * @return an array of SuperObjects
     */
    public ArrayList<SuperObject> getAllObjects() {
        return allObjects;
    }

    /**
     * Sets the array of all SuperObjects controlled by this ObjectController to the one passed as a parameter.
     * @param allObjects an array of SuperObjects
     */
    public void setAllObjects(ArrayList<SuperObject> allObjects) {
        this.allObjects = allObjects;
    }

    /**
     * Draws all visible SuperObjects in the game world.
     * @param g2 the Graphics2D object used for drawing
     */
    public void draw(Graphics2D g2){
        for (SuperObject object: allObjects){
            if (object != null && objectIsVisible(object)) {
                int[] worldCoordinates = GamePanel.getPlayer().getWorldPosition();
                object.setPositionOnScreen(new int[]{object.getPosition()[0]-worldCoordinates[0], object.getPosition()[1]});
                g2.drawImage(object.getImage(), object.getPositionOnScreen()[0], object.getPositionOnScreen()[1], null);
            }
        }
    }

    /**
     * Determines whether a SuperObject is currently visible on the screen.
     * @param object the SuperObject to check
     * @return true if the SuperObject is visible, false otherwise
     */
    private boolean objectIsVisible(SuperObject object){
        int[] objectPosition = object.getPosition();
        int[] worldCoordinates = GamePanel.getPlayer().getWorldPosition();
        return (objectPosition[0] < GamePanel.screenWidth + worldCoordinates[0] &&
                objectPosition[0] > worldCoordinates[0]);
    }
}
