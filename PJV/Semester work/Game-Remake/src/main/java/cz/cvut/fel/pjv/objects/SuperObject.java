package cz.cvut.fel.pjv.objects;

import cz.cvut.fel.pjv.main.display.GamePanel;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * A superclass representing a game object.
 * Contains common attributes and methods shared by all objects in the game.
 */
public class SuperObject {
    /**
     * Whether object has already dealt damage to the player
     */
    private boolean damaged;
    /**
     * The hitbox of the object.
     */
    Rectangle hitbox;
    /**
     * The position of the object.
     */
    private int[] position;

    /**
     * The position of the object on the screen.
     */
    private int[] positionOnScreen;
    /**
     * The name of the object.
     */
    String name;
    /**
     * The image of the object.
     */
    private BufferedImage image;

    /**
     * Draws the hitbox of the object.
     * @param g2 the graphics context
     */
    public void drawHitbox(Graphics2D g2){

    }

    /**
     * Gets the image of the object.
     * @return the image of the object
     */
    public BufferedImage getImage() {
        return image;
    }

    /**
     * Gets the position of the object on the screen.
     * @return the position of the object on the screen
     */
    public int[] getPositionOnScreen() {
        return positionOnScreen;
    }

    /**
     * Sets the position of the object on the screen.
     * @param positionOnScreen the new position of the object on the screen
     */
    public void setPositionOnScreen(int[] positionOnScreen) {
        this.positionOnScreen = positionOnScreen;
    }

    /**
     * Gets the hitbox of the object.
     * @return the hitbox of the object
     */
    public Rectangle getHitbox() {
        return hitbox;
    }

    /**
     * Gets the position of the object.
     * @return the position of the object
     */
    public int[] getPosition() {
        return position;
    }

    /**
     * Gets the name of the object.
     * @return the name of the object
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the contents of the object.
     * @return the contents of the object
     */
    public String getContains() {
        return "";
    }

    /**
     * Sets the position of the object.
     * @param position the new position of the object
     */
    public void setPosition(int[] position) {
        this.position = position;
    }

    /**
     * Gets the attribute damaged, which says whether object has damaged the player or not
     * @return true if object has damaged the player, false otherwise
     */
    public boolean hasDamagedPlayer() {
        return damaged;
    }
}
