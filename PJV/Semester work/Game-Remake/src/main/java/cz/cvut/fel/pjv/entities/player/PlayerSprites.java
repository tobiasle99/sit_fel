package cz.cvut.fel.pjv.entities.player;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;
import static cz.cvut.fel.pjv.main.display.GamePanel.tileSize;

/**
 * The PlayerSprites class contains all the player sprite images for the game. The class includes methods for loading
 * and accessing the sprites.
 */
public class PlayerSprites {
    private BufferedImage[][] idleSprites;
    private BufferedImage[][] runSprites;
    private BufferedImage[] idleCombatSprites;
    private BufferedImage currentSprite;
    private BufferedImage[] attackSprites;
    private BufferedImage[] hurtSprites;
    private BufferedImage[] deathSprites;
    private boolean attacking = false;
    private boolean takingDamage = false;
    private boolean healing = false;
    private boolean dying = false;
    private boolean died = false;
    private int spriteTime;
    private int animationIndex;
    private int animationTime;
    private boolean midAnimation = false;
    private int currentSpriteIndex = 0;

    /**
     * This is the constructor method for the PlayerSprites class. It initializes all the sprite arrays and sets the
     * default sprite to the idle right sprite.
     */
    public PlayerSprites() {
        this.idleSprites = new BufferedImage[2][3];
        this.runSprites = new BufferedImage[2][6];
        this.idleCombatSprites = new BufferedImage[4];
        this.attackSprites = new BufferedImage[5];
        this.hurtSprites = new BufferedImage[3];
        this.deathSprites = new BufferedImage[7];
        getPlayerImage();
        currentSprite = idleSprites[1][0];
        spriteTime = 0;
        animationIndex = 0;
        animationTime = 0;
    }

    /**
     * This method returns whether the player is in the middle of a sprite animation.
     * @return a boolean representing whether the player is in the middle of a sprite animation.
     */
    public boolean isMidAnimation() {
        return midAnimation;
    }

    /**
     * This method returns a scaled version of an image.
     * @param original the original image to be scaled.
     * @param width the width of the scaled image.
     * @param height the height of the scaled image.
     * @return a BufferedImage that is a scaled version of the original image.
     */
    public static BufferedImage scaledImage (BufferedImage original, int width, int height) {
        //SCALING IMAGES BEFORE RENDERING
        BufferedImage scaledImage = new BufferedImage(width, height, original.getType());
        Graphics2D g2 = scaledImage.createGraphics();
        g2.drawImage(original, 0, 0, width, height, null);
        g2.dispose();
        return scaledImage;
    }

    /**
     * This method loads all the player sprites into their respective arrays.
     */
    private void getPlayerImage(){
        try{

            //LOAD IDLE SPRITES
            for (int i = 0; i < idleSprites[0].length; i++){
                idleSprites[1][i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/player/playerSprites/adventurer-idle-0" + i + ".png")));
                idleSprites[0][i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/player/playerSprites/adventurer-idle-0" + i + "_left.png")));
                idleSprites[0][i] = scaledImage(idleSprites[0][i], 2*tileSize, 2*tileSize);
                idleSprites[1][i] = scaledImage(idleSprites[1][i], 2*tileSize, 2*tileSize);

            }

            //LOAD RUNNING SPRITES
            for (int i = 0; i < runSprites[0].length; i++){
                runSprites[1][i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/player/playerSprites/adventurer-run-0" + i  + ".png")));
                runSprites[0][i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/player/playerSprites/adventurer-run-0" + i  + "_left.png")));
                runSprites[0][i] = scaledImage(runSprites[0][i], 2*tileSize, 2*tileSize);
                runSprites[1][i] = scaledImage(runSprites[1][i], 2*tileSize, 2*tileSize);
            }

            //LOAD IDLE COMBAT SPRITES
            for (int i = 0; i < idleCombatSprites.length; i++){
                idleCombatSprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/player/playerSprites/adventurer-idle-2-0" + i + ".png")));

                idleCombatSprites[i] = scaledImage(idleCombatSprites[i], 2*tileSize, 2*tileSize);

            }

            //LOAD ATTACK SPRITES
            for (int i = 0; i < attackSprites.length; i++){
                attackSprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/player/playerSprites/adventurer-attack1-0" + i + ".png")));

                attackSprites[i] = scaledImage(attackSprites[i], 2*tileSize, 2*tileSize);

            }

            //LOAD HURT SPRITES
            for (int i = 0; i < hurtSprites.length; i++){
                hurtSprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/player/playerSprites/adventurer-hurt-0" + i + ".png")));

                hurtSprites[i] = scaledImage(hurtSprites[i], 2*tileSize, 2*tileSize);

            }

            for (int i = 0; i < deathSprites.length; i++){
                deathSprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/player/playerSprites/adventurer-die-0" + i + ".png")));

                deathSprites[i] = scaledImage(deathSprites[i], 2*tileSize, 2*tileSize);

            }

            LOGGER.info("Player sprites loaded successfully");
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.warning("Player sprites didn't load properly");
        }
    }

    /**
     * Returns the current sprite image.
     * @return The current sprite image.
     */
    public BufferedImage getCurrentSprite() {
        return currentSprite;
    }

    /**
     * Cycles the sprite depending on the parameters.
     * @param isRunning A boolean value indicating whether the character is running.
     * @param isInCombat A boolean value indicating whether the character is in combat.
     * @param direction A string value indicating the direction of the character.
     */
    public void cycleSprite(boolean isRunning, boolean isInCombat, String direction){
        if (isInCombat){
            //IS IN COMBAT
            if (midAnimation){
                //IS IN THE MIDDLE OF AN ANIMATION
                if (attacking) {
                    attack();
                } else if (takingDamage){
                    takeDamage();
                } else if (healing) {
                    heal();
                } else if (dying) {
                    die();
                }
                animationTime++;
            } else {
                //IS NOT IN THE MIDDLE OF AN ANIMATION
                if (currentSpriteIndex >= idleCombatSprites.length - 1) {
                    currentSpriteIndex = 0;
                }
                if (spriteTime > 20) {
                    if (currentSpriteIndex >= idleCombatSprites.length - 1) {
                        currentSpriteIndex = 0;
                    } else {
                        currentSpriteIndex++;
                    }
                    spriteTime = 0;
                }
                currentSprite = idleCombatSprites[currentSpriteIndex];
            }
        }
        else {
            //ISNT IN COMBAT
            if (midAnimation){
                //IS IN THE MIDDLE OF AN ANIMATION
                if (takingDamage){
                    takeDamage();
                } else if (dying) {
                    die();
                }
                animationTime++;
            } else {

                int directionIndex;
                if (direction.equals("left")) {
                    directionIndex = 0;
                } else {
                    directionIndex = 1;
                }
                if (isRunning) {
                    //IS RUNNING
                    if (spriteTime > 10) {
                        if (currentSpriteIndex >= runSprites[0].length - 1) {
                            currentSpriteIndex = 0;
                        } else {
                            currentSpriteIndex++;
                        }
                        spriteTime = 0;
                    }
                    currentSprite = runSprites[directionIndex][currentSpriteIndex];
                } else {
                    //ISN'T RUNNING
                    if (currentSpriteIndex >= idleSprites[0].length - 1) {
                        currentSpriteIndex = 0;
                    }
                    currentSprite = idleSprites[directionIndex][currentSpriteIndex];
                    if (spriteTime > 25) {
                        {
                            currentSpriteIndex++;
                        }
                        spriteTime = 0;
                    }
                }
            }
        }
        spriteTime++;
    }

    /**
     * Displays the attack animation.
     */
    public void attack(){
        midAnimation = true;
        attacking = true;
        if (animationTime > 13){
            if (animationIndex < attackSprites.length-1){
                //MORE FRAMES TO DISPLAY
                animationIndex++;
                animationTime = 0;
            }
            else {
                //LAST FRAME
                attacking = false;
                midAnimation = false;
                animationIndex = 0;
                animationTime = 0;
            }
        }
        currentSprite = attackSprites[animationIndex];
    }

    /**
     * Displays the healing animation.
     */
    public void heal(){
        //DIDNT HAVE HEALING ANIMATION
        midAnimation = true;
        healing = true;
        if (animationTime > 20){
            if (animationIndex < idleSprites[1].length-1){
                //MORE SPRITES TO SHOW
                animationIndex++;
                animationTime = 0;
            }
            else {
                //LAST SPRITE
                healing = false;
                midAnimation = false;
                animationIndex = 0;
                animationTime = 0;
            }
        }
        currentSprite = idleSprites[1][animationIndex];
    }

    /**
     * Displays the take damage animation.
     */
    public void takeDamage(){
        midAnimation = true;
        takingDamage = true;
        if (animationTime > 13){
            if (animationIndex < hurtSprites.length-1){
                //MORE SPRITES TO SHOW
                animationIndex++;
                animationTime = 0;
            }
            else {
                //LAST SPRITE
                takingDamage = false;
                midAnimation = false;
                animationIndex = 0;
                animationTime = 0;
            }
        }
        currentSprite = hurtSprites[animationIndex];
    }

    public void die(){
        midAnimation = true;
        dying = true;
        if (animationTime > 5){
            if (animationIndex < deathSprites.length-1){
                animationIndex++;
                animationTime = 0;
            }
            else {
                dying = false;
                midAnimation = false;
                died = true;
                animationIndex = 0;
                animationTime = 0;
            }
        }
        currentSprite = deathSprites[animationIndex];
    }

    public boolean getDied() {
        return died;
    }
}
