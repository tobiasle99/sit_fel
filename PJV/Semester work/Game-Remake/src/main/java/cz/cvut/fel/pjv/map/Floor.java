package cz.cvut.fel.pjv.map;

import cz.cvut.fel.pjv.main.display.GamePanel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

import static cz.cvut.fel.pjv.entities.player.PlayerSprites.scaledImage;
import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;
import static cz.cvut.fel.pjv.main.display.GamePanel.*;

/**
 * The Floor class represents the floor of the game map.
 * It includes the loading of floor images, drawing of the floor and the path on the game map.
 */
public class Floor {
    /**
     * The floor tile image.
     */
    BufferedImage floorTile;

    /**
     * The dirt tile image.
     */
    BufferedImage dirtTile;

    /**
     * The amount of tiles needed to fill the screen.
     */
    int tilesAmount = screenWidth/tileSize + 4;

    /**
     * Constructs a Floor object and loads the floor images.
     */
    public Floor() {
        loadFloorImages();
    }

    /**
     * Loads the floor images.
     */
    private void loadFloorImages(){
        try {
            floorTile = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/map/floor/oak_woods_1.png")));
            floorTile = scaledImage(floorTile, tileSize, tileSize);
            dirtTile = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                    "/map/floor/oak_woods_22.png")));
            dirtTile = scaledImage(dirtTile, tileSize, tileSize);

        } catch (IOException e){
            e.printStackTrace();
            LOGGER.warning("UNABLE TO LOAD FLOOR IMAGES");
        }
    }

    /**
     * Draws the floor and path on the game map.
     *
     * @param g2 The graphics object used to draw the floor and path.
     * @param playeroPositionX The X position of the player.
     */
    public void draw(Graphics2D g2, int playeroPositionX) {
        for (int i = 0; i < tilesAmount; i++){
            int widths = 0;
            while ((-playeroPositionX + i*tileSize + widths*screenWidth < playeroPositionX + screenWidth)) {
                //IS TO THE LEFT OF RIGHT SIDE OF SCREEN
                if (floorIsVisible(-playeroPositionX+i*tileSize)) {
                    //IS VISIBLE

                    //DRAW PATH
                    g2.drawImage(floorTile, -playeroPositionX + (i*tileSize) +(widths*screenWidth), screenHeight-4*tileSize, null);

                    //DRAW DIRT
                    for (int j = 3; j > 0; j--){
                        g2.drawImage(dirtTile, -playeroPositionX + (i*tileSize) +(widths*screenWidth), screenHeight-j*tileSize, null);
                    }
                }
                widths++;
            }

        }
    }

    /**
     * Checks if the floor is visible.
     *
     * @param X The X position of the floor.
     * @return true if the floor is visible, false otherwise.
     */
    private boolean floorIsVisible(int X){
        if (X < X - screenWidth){
            return false;
        }
        return true;
    }

    /**
     * Draws the floor of the game during combat,
     * based on the player's position on the x-axis.
     * @param g2 the graphics object used to draw the combat floor
     * @param playeroPositionX the current x-axis position of the player
     */
    public void drawCombat(Graphics2D g2, int playeroPositionX) {
        for (int i = 0; i < tilesAmount; i++){
            int widths = 0;
            while ((-playeroPositionX + i*tileSize + widths*screenWidth < playeroPositionX + screenWidth)) {
                //IS TO THE LEFT OF RIGHT SIDE OF SCREEN
                if (floorIsVisible(-playeroPositionX+i*tileSize)) {
                    //IS VISIBLE

                    //DRAW PATH
                    g2.drawImage(floorTile, -playeroPositionX + (i*tileSize) +(widths*screenWidth), screenHeight-7*tileSize, null);

                    //DRAW DIRT
                    for (int j = 6; j > 0; j--){
                        g2.drawImage(dirtTile, -playeroPositionX + (i*tileSize) +(widths*screenWidth), screenHeight-j*tileSize, null);
                    }
                }
                widths++;
            }

        }
    }
}
