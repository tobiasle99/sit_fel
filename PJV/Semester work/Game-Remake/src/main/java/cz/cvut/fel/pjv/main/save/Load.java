package cz.cvut.fel.pjv.main.save;

import cz.cvut.fel.pjv.entities.enemy.Enemy;
import cz.cvut.fel.pjv.entities.enemy.Goblin;
import cz.cvut.fel.pjv.entities.enemy.Skeleton;
import cz.cvut.fel.pjv.entities.player.ObjectInteraction;
import cz.cvut.fel.pjv.entities.player.Player;
import cz.cvut.fel.pjv.items.*;
import cz.cvut.fel.pjv.main.display.GamePanel;
import cz.cvut.fel.pjv.objects.FinishLine;
import cz.cvut.fel.pjv.objects.OBJ_Chest;
import cz.cvut.fel.pjv.objects.SuperObject;
import cz.cvut.fel.pjv.objects.Trap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Objects;

import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;

/**
 * The Load class provides methods for loading saved games from file.
 */
public class Load {

    /**
     * Creates a button for loading a saved game.
     * @param gamePanel the GamePanel instance to load the saved game into
     * @return the loadButton object
     */
    public JButton createLoadButton(GamePanel gamePanel){
        JButton loadButton = new JButton("Load");

        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadFromFile(gamePanel);
            }
        });

        return loadButton;
    }

    /**
     *Loads a saved game from a file.
     * @param gamePanel the GamePanel instance to load the saved game into
     */
    private void loadFromFile(GamePanel gamePanel) {
        JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showOpenDialog(null); // Open the file picker dialog box

        if (returnValue == JFileChooser.APPROVE_OPTION) { // Check if a file was selected
            File selectedFile = fileChooser.getSelectedFile();
            try {
                JSONObject loadFile = new JSONObject(new JSONTokener(new FileInputStream(selectedFile)));
                // Use the JSONObject to access the data in the file
                gamePanel.getObjectController().setAllObjects(loadObjects(loadFile));
                gamePanel.getUpdateController().setObjectInteraction(new ObjectInteraction(gamePanel.getPlayer(),
                        gamePanel.getObjectController().getAllObjects()));
                gamePanel.getEnemyController().setAllEnemies(loadEnemies(loadFile));
                loadPlayer(gamePanel.getPlayer(), loadFile);
                gamePanel.getPlayer().getInventory().setItems(loadInventory(loadFile));
            } catch (FileNotFoundException | JSONException e) {
                e.printStackTrace();
            }
        }
        LOGGER.info("Loaded game from file");
    }

    /**
     * Loads the player data from a JSON object.
     * @param player the player instance to load the data into
     * @param loadFile the JSON object containing the player data
     * @throws JSONException if the JSON object does not contain the expected data
     */
    void loadPlayer(Player player, JSONObject loadFile) throws JSONException {
        if (loadFile.has("player")) {
            JSONObject playerObject = loadFile.getJSONObject("player");
            player.setArmor(playerObject.getInt("player_armor"));
            player.setAttack(playerObject.getInt("player_attack"));
            player.setWorldPosition(new int[]{
                    playerObject.getJSONArray("player_position").getInt(0),
                    playerObject.getJSONArray("player_position").getInt(1)
            });
            player.setHealth(playerObject.getInt("player_health"));
            player.setSpeed(playerObject.getInt("player_speed"));
        }
        LOGGER.info("Loaded player from file");
    }

    /**
     * Loads the inventory data from a JSON object.
     * @param loadFile the JSON object containing the inventory data
     * @return an array of Item objects representing the player's inventory
     * @throws JSONException if the JSON object does not contain the expected data
     */
    Item[] loadInventory(JSONObject loadFile) throws JSONException {
        Item[] items = new Item[5];
        if (loadFile.has("inventory")) {

            JSONArray arrayOfItems = loadFile.getJSONArray("inventory");
            for (int i = 0; i < arrayOfItems.length(); i++){
                items[i] = constructItem(arrayOfItems.getJSONObject(i));
            }
        } else {
            LOGGER.warning("objects are missing in load file");
        }
        LOGGER.info("Loaded inventory from file");
        return items;
    }

    /**
     * Constructs an Item object from a JSON object.
     * @param object the JSON object containing the item data
     * @return an Item object representing the loaded item
     * @throws JSONException if the JSON object does not contain the expected data
     */
    Item constructItem(JSONObject object) throws JSONException {
        Item constructedItem = null;
        switch (object.getString("item_name")){
            case("Shield"):
                constructedItem = new Shield();
                LOGGER.info("Loaded item is a shield");
                break;
            case("Potion"):
                constructedItem = new Potion();
                LOGGER.info("Loaded item is a potion");
                break;
            case("Key"):
                constructedItem = new Key();
                LOGGER.info("Loaded item is a key");
                break;
            case("Sword"):
                constructedItem = new Sword();
                LOGGER.info("Loaded item is a sword");
                break;
            default:
                LOGGER.warning("Loaded item not recognized");
        }
        return constructedItem;
    }

    /**
     * Load objects from a given JSON file and constructs them as SuperObjects.
     * @param loadFile a JSON object containing an array of objects to be loaded
     * @return an array of SuperObjects constructed from the loaded objects
     * @throws JSONException if the JSON object is invalid
     */
    ArrayList<SuperObject> loadObjects(JSONObject loadFile) throws JSONException {
        ArrayList<SuperObject> loadedObjects = new ArrayList<>();
        if (loadFile.has("objects")){
            JSONArray arrayOfObjects = loadFile.getJSONArray("objects");
            for (int i = 0; i < arrayOfObjects.length(); i++){
                loadedObjects.add(constructObject(arrayOfObjects.getJSONObject(i)));
            }
        } else {
            LOGGER.warning("objects are missing in load file");
        }

        System.out.println(loadedObjects);
        return loadedObjects;
    }

    /**
     * Constructs a SuperObject from a given JSON object
     * @param object a JSON object representing an object to be constructed
     * @return the constructed SuperObject
     * @throws JSONException if the JSON object is invalid
     */
    SuperObject constructObject(JSONObject object) throws JSONException {
        SuperObject constructedObject = null;
        if (Objects.equals(object.getString("object_name"), "chest")) {
            constructedObject = new OBJ_Chest(object.getString("contains"));

            LOGGER.info("Loaded object is a chest containing " + object.getString("contains") + " at position "
                    + constructedObject.getPosition()[0] + " " + constructedObject.getPosition()[1]);
        } else if (Objects.equals(object.getString("object_name"), "finish line")) {
            constructedObject = new FinishLine();
            LOGGER.info("Loaded object is a finish line at position "
                    + constructedObject.getPosition()[0] + " " + constructedObject.getPosition()[1]);
        } else if (Objects.equals(object.getString("object_name"), "trap")) {
            constructedObject = new Trap();
        } else {
            LOGGER.warning("Loaded object not recognized");
        }
        constructedObject.setPosition(new int[]{
                object.getJSONArray("object_position").getInt(0),
                object.getJSONArray("object_position").getInt(1)
        });
        return constructedObject;
    }

    /**
     * Load enemies from a given JSON file and constructs them as Enemy objects.
     * @param loadFile a JSON object containing an array of enemies to be loaded
     * @return an array of Enemy objects constructed from the loaded enemies
     * @throws JSONException if the JSON object is invalid or does not contain any enemies
     */
    Enemy[] loadEnemies(JSONObject loadFile) throws JSONException {
        Enemy[] loadedEnemies = new Enemy[10];
        if (loadFile.has("enemies")){
            JSONArray arrayOfEnemies = loadFile.getJSONArray("enemies");
            for (int i = 0; i < arrayOfEnemies.length(); i++){
                loadedEnemies[i] = constructEnemy(arrayOfEnemies.getJSONObject(i));
            }
        } else {
            LOGGER.warning("Load file missing enemies");
            throw new JSONException("no \"enemies\" object found in loaded file");
        }
        LOGGER.info("finished loading enemies");
        return loadedEnemies;
    }

    /**
     * Constructs an Enemy object from a given JSON object.
     * @param enemy a JSON object representing an enemy to be constructed
     * @return the constructed Enemy object
     * @throws JSONException if the JSON object is invalid
     */
    Enemy constructEnemy(JSONObject enemy) throws JSONException {
        Enemy constructedEnemy = null;
        switch (enemy.getString("enemy_name")){
            case ("Skeleton"):
                constructedEnemy = new Skeleton(constructItem(enemy.getJSONObject("drop")));
                LOGGER.info("loaded enemy of type skeleton who drops " + enemy.getJSONObject("drop"));
                break;
            case ("Goblin"):
                constructedEnemy = new Goblin(constructItem(enemy.getJSONObject("drop")));
                LOGGER.info("loaded enemy of type goblin who drops " + enemy.getJSONObject("drop") );
                break;
            default:
                LOGGER.warning("Enemy type not recognized");
                break;
        }
        if (constructedEnemy != null) {
            constructedEnemy.setHealth(enemy.getInt("enemy_health"));
            constructedEnemy.setArmor(enemy.getInt("enemy_armor"));
            constructedEnemy.setAttack(enemy.getInt("enemy_attack"));
            constructedEnemy.setSpeed(enemy.getInt("enemy_speed"));
            constructedEnemy.setPosition(new int[]{
                    enemy.getJSONArray("enemy_position").getInt(0),
                    enemy.getJSONArray("enemy_position").getInt(1)
            });
        }
        return constructedEnemy;
    }
}
