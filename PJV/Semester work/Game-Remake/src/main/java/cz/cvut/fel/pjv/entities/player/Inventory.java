package cz.cvut.fel.pjv.entities.player;

import cz.cvut.fel.pjv.items.Item;
import cz.cvut.fel.pjv.items.Key;
import cz.cvut.fel.pjv.items.Sword;
import cz.cvut.fel.pjv.main.display.InvetoryUI;

import java.awt.*;

/**
 * This class represents a player's inventory.
 */
public class Inventory {
    private Item[] items;
    private InvetoryUI invetoryUI;

    /**
     * Constructs an inventory and initializes the items array with two items (Sword and Key).
     */
    public Inventory() {
        this.items = new Item[5];
        this.invetoryUI = new InvetoryUI();
        this.items[0] = new Sword();
        this.items[1] = new Key();
    }

    /**
     * Draws the inventory UI.
     * @param g2 the graphics object
     */
    public void draw(Graphics2D g2){
        invetoryUI.draw(g2, items);
    }

    /**
     * Checks if the inventory is full.
     * @return true if the inventory is full, false otherwise
     */
    public boolean inventoryIsFull(){
        for (int i = 0; i < items.length; i++){
            if (items[i] == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Adds an item to the inventory if there is space.
     * @param item the item to add
     */
    public void addItem(Item item){
        if (!inventoryIsFull()) {
            for (int i = 0; i < items.length; i++) {
                if (items[i] == null) {
                    items[i] = item;
                    return;
                }
            }
        }
    }

    /**
     * Gets the items in the inventory.
     * @return the items array
     */
    public Item[] getItems() {
        return items;
    }

    /**
     * Sets the items in the inventory.
     * @param items the new items array
     */
    public void setItems(Item[] items) {
        for (int i = 0; i < items.length; i++) {
            this.items[i] = items[i];
        }
    }

    /**
     * Checks if the inventory contains a key item.
     * @return true if the inventory contains a key, false otherwise
     */
    public boolean hasKey(){
        for (Item item: items){
            if (item != null && item.getName().equals("Key")){
                return true;
            }
        }
        return false;
    }

    /**
     * Uses a key item by removing it from the inventory.
     */
    public void useKey(){
        items[keyIndex()] = null;
    }

    /**
     * Gets the index of the key item in the items array.
     * @return the index of the key item, or 999 if the key item is not found
     */
    private int keyIndex(){
        for (int i = 0; i< items.length; i++){
            if (items[i] != null && items[i].getName().equals("Key")){
                return i;
            }
        }
        return 999;
    }
}
