package cz.cvut.fel.pjv.map;

import cz.cvut.fel.pjv.main.display.GamePanel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

import static cz.cvut.fel.pjv.entities.player.PlayerSprites.scaledImage;
import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;
import static cz.cvut.fel.pjv.main.display.GamePanel.*;

/**
 * This class represents the background of the game. It loads and draws the background images.
 */
public class Background {
    BufferedImage[] backgroundImages = new BufferedImage[3];

    /**
     * Constructs a Background object and loads the background images.
     */
    public Background() {
        loadBackgroundImages();
    }

    /**
     * Loads the background images from the resources folder.
     */
    private void loadBackgroundImages(){
        try {
            for (int i = 0; i < backgroundImages.length; i++) {
                backgroundImages[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/map/background/background_layer_" + (i + 1) + ".png")));
                backgroundImages[i] = scaledImage(backgroundImages[i], screenWidth, screenHeight);
            }
        } catch (IOException e){
            e.printStackTrace();
            LOGGER.warning("UNABLE TO LOAD BACKGROUND IMAGES");
        }
    }

    /**
     * Draws different layers of the background in different indexes.
     * @param g2 the graphics object used to draw the background
     * @param playerPositionX the current position of the player in the X-axis
     */
    public void draw(Graphics2D g2, int playerPositionX) {
        for (int i = 0; i < backgroundImages.length; i++) {
            BufferedImage layer = backgroundImages[i];
            int widths = 0;
            while ((-playerPositionX/((3-i)*10) + screenWidth*widths) < playerPositionX + screenWidth) {
                //IS TO THE LEFT OF RIGHT SIDE OF SCREEN
                if (backgroundIsVisible(-playerPositionX / ((3 - i) * 10))) {
                    //IS VISIBLE
                    g2.drawImage(layer, -playerPositionX / ((3 - i) * 10) + screenWidth * widths, 0, GamePanel.screenWidth, GamePanel.screenHeight, null);
                }
                widths++;
            }
        }
    }

    /**
     * Checks if the background is visible at a given X position.
     * @param X the position to check
     * @return true if the background is visible, false otherwise
     */
    private boolean backgroundIsVisible(int X){
        if (X < X - screenWidth){
            return false;
        }
        return true;
    }
}
