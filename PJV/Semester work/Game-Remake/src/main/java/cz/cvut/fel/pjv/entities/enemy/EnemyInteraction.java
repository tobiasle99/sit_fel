package cz.cvut.fel.pjv.entities.enemy;

import cz.cvut.fel.pjv.entities.player.Player;
import cz.cvut.fel.pjv.objects.SuperObject;

import java.awt.*;

import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;

public class EnemyInteraction {
    Player player;
    Rectangle playerHitbox;
    Enemy[] allEnemies;

    public EnemyInteraction(Player player, Enemy[] allEnemies) {
        this.player = player;
        this.playerHitbox = player.getHitbox();
        this.allEnemies = allEnemies;
    }

    /**
     * Checks if player's hitbox intersects with enemy auras. If yes, then it returns their index in EnemyController.
     * Otherwise, returns 999.
     * @return
     */
    public int checkEnemyInteraction(){
        if (!player.isInCombat()) {
            for (int enemyIndex = 0; enemyIndex < allEnemies.length; enemyIndex++) {
                if (allEnemies[enemyIndex] != null) {
                    if (playerHitbox.intersects(allEnemies[enemyIndex].getAura())) {
                        //PLAYER IS IN AN AURA OF AN EXISTING ENEMY
                        LOGGER.info("Interacting with enemy with index " + enemyIndex);
                        return enemyIndex;
                    }

                }
            }
        }
        return 999;
    }
}
