package cz.cvut.fel.pjv.entities.player;

import cz.cvut.fel.pjv.entities.enemy.Enemy;
import cz.cvut.fel.pjv.main.display.CombatUI;

import java.util.Objects;

import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;

/**
 * The PlayerCombat class represents the combat system of the game, in which a player battles with an enemy.
 */
public class PlayerCombat {
    private final Player player;
    private final Enemy enemy;
    private final int playerSpeed;
    private final int enemySpeed;
    private int playerHealth;
    private int enemyHealth;
    private final int playerAttack;
    private final int playerArmor;
    private final int enemyAttack;
    private final int enemyArmor;
    private final double enemyChanceOfHitting;
    private String turn;
    private final PlayerControls playerControls;
    private boolean combatStarted;
    private final CombatUI combatUI;

    /**
     * Constructs a new PlayerCombat object.
     *
     * @param player the player object
     * @param enemy the enemy object
     * @param playerControls the controls for the player object
     * @param combatUI the user interface for the combat system
     */
    public PlayerCombat(Player player, Enemy enemy, PlayerControls playerControls, CombatUI combatUI) {
        this.combatUI = combatUI;
        this.playerControls = playerControls;
        this.player = player;
        this.enemy = enemy;
        playerSpeed = player.getSpeed();
        enemySpeed = enemy.getSpeed();
        playerHealth = player.getHealth();
        enemyHealth = enemy.getHealth();
        playerAttack = player.getAttack();
        playerArmor = player.getArmor();
        enemyAttack = enemy.getAttack();
        enemyArmor = enemy.getArmor();
        enemyChanceOfHitting = enemy.getChanceOfHitting();
    }

    /**
     * Implements the combat system of the game, in which a player battles with an enemy.
     * Is called until the combat finishes. The combat finishes when one of the fighters drops below 0 health.
     * Implements a turn based combat system, in which a player can choose to attack or use a consumable item.
     * Enemies can only attack.
     */
    public void combat(){
        if (!combatStarted) {
            //FIRST TURN
            if (playerSpeed > enemySpeed) {
                turn = "player";
                combatStarted = true;
            } else {
                turn = "enemy";
                combatStarted = true;
            }

            LOGGER.info("Combat starts and " + turn + " is first");
        }

        if (!player.getPlayerSprites().isMidAnimation() && !enemy.getEnemySprites().getMidAnimation()) {
            //NEITHER THE PLAYER NOR ENEMY ARE IN THE MIDDLE OF AN ANIMATION

            //TURN DISPLAYED
            if (turn.equals("player")) {
                combatUI.setTurn("player");
            } else if (turn.equals("enemy")) {
                combatUI.setTurn(enemy.getName());
            }

            //COMBAT LOOP
            playerHealth = player.getHealth();
            enemyHealth = enemy.getHealth();
            if (playerHealth > 0 && enemyHealth > 0) {
                if (Objects.equals(turn, "player")) {
                    int choice = playerTurn(player);
                    if (choice == 0) {
                        playerAttack();
                        turn = "enemy";
                        LOGGER.info("Player chose to attack");
                    } else if (choice >= 1 && choice <= 5){
                        playerUseItem(choice);
                        LOGGER.info("Player chose to use item number " + choice);
                    }
                } else if (Objects.equals(turn, "enemy")) {
                    if (!player.getPlayerSprites().isMidAnimation()) {
                        enemyAttack();
                        LOGGER.info("Enemy chose to attack");
                    }
                }
            }

            //DEATH
            if (playerHealth <= 0) {
                //PLAYER TOOK FATAL DAMAGE
                combatUI.showMessage("ENEMY WON!");

                if (!player.getPlayerSprites().getDied()) {
                    player.getPlayerSprites().die();
                }
                if (!player.getPlayerSprites().isMidAnimation()) {
                    try {
                        Thread.sleep(800);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    player.setDead(true);
                    player.setPlayerInCombat(false);
                    player.setGame_over(true);
                    LOGGER.info("Player lost combat and is returning to walking screen");
                }
                LOGGER.info("Player lost combat");
            } else if (enemyHealth <= 0) {
                //ENEMY TOOK FATAL DAMAGE
                LOGGER.info("Player won combat");
                combatUI.showMessage("PLAYER WON!");
                if (!enemy.getEnemySprites().getDied()) {
                    enemy.getEnemySprites().die();
                }
                if (!enemy.getEnemySprites().getMidAnimation()) {
                    try {
                        Thread.sleep(800);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (enemy.getDrop() != null && !enemy.isItemDropped()){
                        player.getInventory().addItem(enemy.getDrop());
                        enemy.setItemDropped(true);
                    }
                    enemy.setDead(true);
                    player.setPlayerInCombat(false);
                    LOGGER.info("Player finished combat and is returning to walking screen");
                }
            }
        }
    }

    /**
     * Determines the choice the player makes during their turn.
     * @param player the player object
     * @return the player's choice
     */
    private int playerTurn(Player player){
        int returnVal = 999;
        if (playerControls.attackPressed) {
            returnVal = 0;
        } else if (playerControls.item1Pressed || playerControls.item2Pressed || playerControls.item3Pressed ||
        playerControls.item4Pressed || playerControls.item5Pressed){
            returnVal = playerItemUsage(player);
        }
        return returnVal;
    }

    /**
     * Determines which item the player wants to use from their inventory
     * @param player the player object containing the inventory
     * @return the index of the selected item, or 999 if no item was selected
     */
    private int playerItemUsage(Player player){
        int returnVal = 999;
        if (playerControls.item1Pressed) {
            if (player.getInventory().getItems()[0] != null &&
                    player.getInventory().getItems()[0].isUseable()) {
                returnVal = 1;
            }
        }
        else if (playerControls.item2Pressed) {
            if (player.getInventory().getItems()[1] != null &&
                    player.getInventory().getItems()[1].isUseable()) {
                returnVal = 2;
            }
        }
        else if (playerControls.item3Pressed) {
            if (player.getInventory().getItems()[2] != null &&
                    player.getInventory().getItems()[2].isUseable()) {
                returnVal = 3;
            }
        }
        else if (playerControls.item4Pressed) {
            if (player.getInventory().getItems()[3] != null &&
                    player.getInventory().getItems()[3].isUseable()) {
                returnVal = 4;
            }
        }
        else if (playerControls.item5Pressed) {
            if (player.getInventory().getItems()[4]!= null &&
                    player.getInventory().getItems()[4].isUseable()) {
                returnVal = 5;
            }
        }
        return returnVal;
    }

    /**
     * Uses an item from the player's inventory
     * @param itemIndex the index of the item to be used
     */
    private void playerUseItem(int itemIndex){
        if (player.getInventory().getItems()[itemIndex-1].getName().equals("Potion")){
            int healingValue = 15;
            player.getInventory().getItems()[itemIndex-1] = null;
            player.setHealth(playerHealth+ healingValue);
            combatUI.showMessage("PLAYER HEALS FOR " + healingValue + " USING A POTION!");
            player.getPlayerSprites().heal();
            LOGGER.info("player uses potion and is now at " + player.getHealth() + " health");
        }
        turn = "enemy";
    }

    /**
     * Deals damage to enemy by the player
     * Decreases enemyHealth by a value, that is based on playerAttack and enemyArmor
     */
    private void playerAttack(){
        int attackWithoutArmor = playerAttack-enemyArmor;
        player.getPlayerSprites().attack();
        if (attackWithoutArmor < 0) {
            attackWithoutArmor = 0;
        }
        enemy.takeDamage(attackWithoutArmor);
        enemy.getEnemySprites().takeDamage();
        combatUI.showMessage("PLAYER ATTACK HITS FOR " + attackWithoutArmor + " !");
    }

    /**
     * Decreases playerHealth by a value, that is based on enemyAttack and playerArmor
     */
    private void enemyAttack(){
        double RNG = Math.random();
        enemy.getEnemySprites().attack();
        if (RNG > enemyChanceOfHitting){

            combatUI.showMessage(enemy.getName().toUpperCase() + "'S ATTACK MISSED!");
        } else {

            player.getPlayerSprites().takeDamage();
            int attackWithoutArmor = enemyAttack-playerArmor;
            if (attackWithoutArmor < 0) {
                attackWithoutArmor = 0;
            }
            combatUI.showMessage(enemy.getName().toUpperCase() +"'s ATTACK HITS FOR " + attackWithoutArmor + " !");
            player.takeDamage(attackWithoutArmor);
        }
        turn = "player";
    }
}

