package cz.cvut.fel.pjv.entities.enemy;

import java.awt.*;

/**
 * Interface for enemies
 */
public interface EnemyInterface {

    public void setPositionOnScreen(int[] positionOnScreen);

    public int[] getPositionOnScreen();


    public int[] getPosition();

    public Rectangle getAura();

    public int getHealth();

    public String getName();

    public void takeDamage(int damage);

    public int getSpeed();

    public int getArmor();

    public int getAttack();

    public double getChanceOfHitting();

    public boolean isDead() ;

    public void setPosition(int[] position);

    public void setHealth(int health);

    public void setSpeed(int speed);

    public void setArmor(int armor);

    public void setAttack(int attack);
}
