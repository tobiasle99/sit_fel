package cz.cvut.fel.pjv.main.save;

import cz.cvut.fel.pjv.entities.enemy.Enemy;
import cz.cvut.fel.pjv.entities.enemy.Goblin;
import cz.cvut.fel.pjv.entities.player.Inventory;
import cz.cvut.fel.pjv.entities.player.PlayerControls;
import cz.cvut.fel.pjv.items.Item;
import cz.cvut.fel.pjv.items.Shield;
import cz.cvut.fel.pjv.main.MyLogger;
import cz.cvut.fel.pjv.main.display.GamePanel;
import cz.cvut.fel.pjv.objects.OBJ_Chest;
import cz.cvut.fel.pjv.objects.SuperObject;
import org.json.JSONArray;
import org.json.JSONObject;
import cz.cvut.fel.pjv.entities.player.Player;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

/**
 * The Save class provides methods to save the current game state to a file in JSON format.
 */
public class Save {
    private static final Logger LOGGER = MyLogger.LOGGER;

    /**
     * Private helper method that creates a JSONObject to represent a single Item object.
     * @param item the Item object to be saved
     * @return a JSONObject representing the item
     */
    JSONObject saveItem(Item item){
        JSONObject itemJson = new JSONObject();
        itemJson.put("item_name", item.getName());
        return itemJson;
    }

    /**
     * Private helper method that creates a JSONArray to represent an array of Item objects in an Inventory.
     * @param inventory the Inventory containing the Item objects to be saved
     * @return a JSONArray representing the inventory
     */
    JSONArray saveInventory(Inventory inventory){
        JSONArray itemArray = new JSONArray();
        for (Item item:inventory.getItems()){
            if ( item != null ) {
                itemArray.put(saveItem(item));
            }
        }
        return itemArray;
    }

    /**
     * Private helper method that creates a JSONObject to represent a Player object.
     * @param player the Player object to be saved
     * @return a JSONObject representing the player
     */
    JSONObject savePlayerState(Player player){
        JSONObject playerJson = new JSONObject();
        playerJson.put("player_health", player.getHealth());
        playerJson.put("player_armor", player.getArmor());
        playerJson.put("player_attack", player.getAttack());
        playerJson.put("player_position", player.getWorldPosition());
        playerJson.put("player_speed", player.getSpeed());
        return playerJson;
    }

    /**
     * Private helper method that creates a JSONObject to represent an Enemy object.
     * @param enemy the Enemy object to be saved
     * @return a JSONObject representing the enemy
     */
    JSONObject saveEnemy(Enemy enemy){
        JSONObject enemyJson = new JSONObject();
        enemyJson.put("enemy_name", enemy.getName());
        enemyJson.put("enemy_speed", enemy.getSpeed());
        enemyJson.put("enemy_health", enemy.getHealth());
        enemyJson.put("enemy_armor", enemy.getArmor());
        enemyJson.put("enemy_attack", enemy.getAttack());
        enemyJson.put("enemy_position", enemy.getPosition());
        enemyJson.put("enemy_chance_of_hitting", enemy.getChanceOfHitting());
        enemyJson.put("drop", saveItem(enemy.getDrop()));
        return enemyJson;
    }

    /**
     * Private helper method that creates a JSONArray to represent an array of Enemy objects.
     * @param enemies the array of Enemy objects to be saved
     * @return a JSONArray representing the enemies
     */
    JSONArray saveAllEnemies(Enemy[] enemies){
        JSONArray enemiesArray = new JSONArray();
        for (Enemy enemy : enemies) {
            if (enemy != null) {
                enemiesArray.put(saveEnemy(enemy));
            }
        }
        return enemiesArray;
    }

    /**
     * Private helper method that creates a JSONObject to represent a SuperObject object.
     * @param object the SuperObject object to be saved
     * @return a JSONObject representing the object
     */
    private JSONObject saveObject(SuperObject object){
        JSONObject objectJson = new JSONObject();
        objectJson.put("object_name", object.getName());
        objectJson.put("object_position", object.getPosition());
        if (object.getName().equals("chest")){
            objectJson.put("contains", ((OBJ_Chest) object).getContains());
        }
        return objectJson;
    }

    /**
     * Private helper method that creates a JSONArray to represent an array of SuperObject objects.
     * @param objects the array of SuperObject objects to be saved
     * @return a JSONArray representing the objects
     */
    private JSONArray saveAllObjects(ArrayList<SuperObject> objects){
        JSONArray objectArray = new JSONArray();
        for (SuperObject object:objects){
            if (object != null) {
                objectArray.put(saveObject(object));
            }
        }
        return objectArray;
    }

    /**
     * Returns a JSON object with all necessary information to load up a game next time
     * @param objects an array of SuperObject objects representing the game objects
     * @param player a Player object representing the player's state
     * @param enemies an array of Enemy objects representing the game enemies
     * @return a JSONObject containing all the necessary information to load up the game next time
     */
    public JSONObject saveGame(ArrayList<SuperObject> objects, Player player, Enemy[] enemies){
        JSONObject saveJson = new JSONObject();
        saveJson.put("player", savePlayerState(player));
        saveJson.put("inventory", saveInventory(player.getInventory()));
        saveJson.put("objects", saveAllObjects(objects));
        saveJson.put("enemies", saveAllEnemies(enemies));
        return saveJson;
    }

    /**
     * Creates a save button with the given functionality
     * @param gamePanel a GamePanel object representing the game panel
     * @param window a JFrame object representing the game window
     * @return a JButton object representing the save button
     */
    public JButton createSaveButton(GamePanel gamePanel, JFrame window){
        JButton saveButton = new JButton("Save");
        saveButton.setFocusable(false);
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Saved game");
                JFileChooser fileChooser = new JFileChooser();
                String defaultFileName = "save_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".json";
                fileChooser.setSelectedFile(new File(defaultFileName));
                fileChooser.setFileFilter(new FileNameExtensionFilter("JSON files", "json"));
                int returnValue = fileChooser.showSaveDialog(window);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    try {
                        FileWriter fileWriter = new FileWriter(fileToSave);
                        // Call the save method to get the game output as a JSONObject
                        JSONObject gameOutput = gamePanel.save();
                        // Convert the JSONObject to a formatted string before writing to file
                        String jsonString = gameOutput.toString(4);
                        fileWriter.write(jsonString);
                        fileWriter.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        return saveButton;
    }
}
