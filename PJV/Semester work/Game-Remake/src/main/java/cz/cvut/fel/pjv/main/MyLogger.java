package cz.cvut.fel.pjv.main;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class provides a shared Logger instance that can be used to log information in all classes.
 */
public class MyLogger {
    /**
     * The shared Logger instance used in all classes to log necessary information.
     */
    public static final Logger LOGGER;

    static {
        LOGGER = Logger.getLogger(MyLogger.class.getName());
        LOGGER.setLevel(Level.INFO);
    }
}
