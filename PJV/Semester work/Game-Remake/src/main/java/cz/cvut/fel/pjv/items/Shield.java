package cz.cvut.fel.pjv.items;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

import static cz.cvut.fel.pjv.entities.player.PlayerSprites.scaledImage;
import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;
import static cz.cvut.fel.pjv.main.display.GamePanel.tileSize;

public class Shield extends Item{
    private String name = "Shield";
    private boolean useable = false;
    private BufferedImage image;

    @Override
    public BufferedImage getImage() {
        return image;
    }

    public Shield() {
        loadItemImage();
    }

    private void loadItemImage(){
        try {
            image = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                    "/items/shield.png")));
            image = scaledImage(image, tileSize, tileSize);

        } catch (IOException e){
            e.printStackTrace();
            LOGGER.warning("UNABLE TO LOAD IMAGE FOR " + name);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isUseable() {
        return useable;
    }
}
