package cz.cvut.fel.pjv.entities.player;

import cz.cvut.fel.pjv.entities.enemy.EnemySprites;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Objects;

import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;
import static cz.cvut.fel.pjv.main.display.GamePanel.tileSize;
import static java.lang.Math.round;

public class SkeletonSprites extends EnemySprites {
    private BufferedImage[] idleSprites;
    private BufferedImage currentSprite;
    private BufferedImage[] attackSprites;
    private BufferedImage[] hurtSprites;
    private BufferedImage[] deathSprites;
    private boolean attacking = false;
    private boolean takingDamage = false;
    private boolean dying = false;
    private int spriteTime;
    private int animationIndex;
    private int animationTime;
    private boolean midAnimation = false;
    private int currentSpriteIndex = 0;
    private boolean died = false;

    public SkeletonSprites() {
        this.idleSprites = new BufferedImage[11];
        this.attackSprites = new BufferedImage[18];
        this.hurtSprites = new BufferedImage[8];
        this.deathSprites = new BufferedImage[15];
        getEnemyImage();
        currentSprite = idleSprites[0];
        spriteTime = 0;
        animationIndex = 0;
        animationTime = 0;
    }

    @Override
    public boolean getDied() {
        return died;
    }

    public boolean isMidAnimation() {
        return midAnimation;
    }

    private void getEnemyImage(){
        try{

            //LOAD IDLE SPRITES
            for (int i = 0; i < idleSprites.length; i++){
                idleSprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/skeleton/skeleton_idle_" + i + ".png")));
            }

            //LOAD ATTACK SPRITES
            for (int i = 0; i < attackSprites.length; i++){
                attackSprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/skeleton/skeleton_attack_" + i + ".png")));
            }

            //LOAD HURT SPRITES
            for (int i = 0; i < hurtSprites.length; i++){
                hurtSprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/skeleton/skel_hit_" + i + ".png")));
            }


            //LOAD DEATH SPRITES
            for (int i = 0; i < deathSprites.length; i++){
                deathSprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/skeleton/skel_dead_" + i + ".png")));
            }

            scaleImages(idleSprites);
            scaleImages(attackSprites);
            scaleImages(hurtSprites);
            scaleImages(deathSprites);

            LOGGER.info("Successfully loaded goblin sprites");
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.warning("Failed to load goblin sprites");
        }
    }

    private void scaleImages(BufferedImage[] array){
        for (int i = 0; i < array.length; i++) {
            double ratio = (double) array[i].getWidth()/ array[i].getHeight() ;

            double newHeight = 2*tileSize;
            double newWidth = newHeight*ratio;

            array[i] = scaledImage(array[i], (int) round(newWidth), (int) round(newHeight));
        }
    }

    public BufferedImage getCurrentSprite() {
        return currentSprite;
    }

    @Override
    public void cycleSprite(){
        if (midAnimation){
            if (attacking) {
                attack();
            } else if (takingDamage){
                takeDamage();
            } else if (dying){
                die();
            }
            animationTime++;
        } else {
            if (currentSpriteIndex >= idleSprites.length - 1) {
                currentSpriteIndex = 0;
            }
            if (spriteTime > 20) {
                if (currentSpriteIndex >= idleSprites.length - 1) {
                    currentSpriteIndex = 0;
                } else {
                    currentSpriteIndex++;
                }
                spriteTime = 0;
            }
            currentSprite = idleSprites[currentSpriteIndex];
        }
        spriteTime++;
    }

    @Override
    public void attack(){
        midAnimation = true;
        attacking = true;
        if (animationTime > 5){
            if (animationIndex < attackSprites.length-1){
                animationIndex++;
                animationTime = 0;
            }
            else {
                attacking = false;
                midAnimation = false;
                animationIndex = 0;
                animationTime = 0;
            }
        }
        currentSprite = attackSprites[animationIndex];
    }

    @Override
    public void takeDamage(){

        midAnimation = true;
        takingDamage = true;
        if (animationTime > 13){
            if (animationIndex < hurtSprites.length-1){
                animationIndex++;
                animationTime = 0;
            }
            else {
                takingDamage = false;
                midAnimation = false;
                animationIndex = 0;
                animationTime = 0;
            }
        }
        if (animationIndex >= hurtSprites.length-1){
            animationIndex = 0;
            animationTime = 0;
        }
        currentSprite = hurtSprites[animationIndex];
    }

    @Override
    public void die(){
        midAnimation = true;
        dying = true;
        if (animationTime > 5){
            if (animationIndex < deathSprites.length-1){
                animationIndex++;
                animationTime = 0;
            }
            else {
                dying = false;
                midAnimation = false;
                died = true;
                animationIndex = 0;
                animationTime = 0;
            }
        }
        if (animationIndex < deathSprites.length) {
            currentSprite = deathSprites[animationIndex];
        }
    }
}
