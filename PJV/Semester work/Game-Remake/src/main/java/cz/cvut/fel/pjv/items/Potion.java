package cz.cvut.fel.pjv.items;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

import static cz.cvut.fel.pjv.entities.player.PlayerSprites.scaledImage;
import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;
import static cz.cvut.fel.pjv.main.display.GamePanel.tileSize;

public class Potion extends Item{
    private String name = "Potion";
    private boolean useable = true;
    private BufferedImage image;

    public Potion() {
        loadItemImage();
    }

    @Override
    public BufferedImage getImage() {
        return image;
    }

    private void loadItemImage(){
        try {
            image = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                    "/items/potion.png")));
            image = scaledImage(image, tileSize, tileSize);

        } catch (IOException e){
            e.printStackTrace();
            LOGGER.warning("UNABLE TO LOAD IMAGE FOR " + name);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isUseable() {
        return useable;
    }
}
