package cz.cvut.fel.pjv.main.display;



import cz.cvut.fel.pjv.items.Item;

import java.awt.*;

import static cz.cvut.fel.pjv.main.display.GamePanel.*;

/**
 * The InventoryUI class represents the user interface for the player's inventory.
 */
public class InvetoryUI {
    private Font arial15 = new Font("Arial", Font.PLAIN, 15);
    private int sw = screenWidth/2;

    /**
     * Draws five squares representing the inventory slots and the items inside them.
     *
     * @param g2 The Graphics2D object used to draw the inventory.
     * @param items The array of Item objects that represent the player's inventory.
     */
    public void draw(Graphics2D g2, Item[] items){
        g2.setFont(arial15);
        g2.setColor(Color.white);
        for (int i = 0; i < 5; i++){
            g2.drawRect(sw-4*tileSize+(2*i*tileSize), screenHeight-2*tileSize, tileSize, tileSize);
            if (items[i] != null){
                g2.drawImage(items[i].getImage(), sw-4*tileSize+(2*i*tileSize), screenHeight-2*tileSize, null);
            }
        }
    }
}
