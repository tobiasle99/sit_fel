package cz.cvut.fel.pjv.entities.player;

import cz.cvut.fel.pjv.items.Key;
import cz.cvut.fel.pjv.items.Sword;
import cz.cvut.fel.pjv.objects.OBJ_Chest;
import cz.cvut.fel.pjv.items.Potion;
import cz.cvut.fel.pjv.items.Shield;
import cz.cvut.fel.pjv.objects.SuperObject;

import java.awt.*;
import java.util.ArrayList;
import java.util.Objects;

import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;

/**
 * This class represents the interactions between the player and the objects present in the game.
 * The class checks if the player is interacting with any item and if yes, the item disappears and the objectBehaviour
 * is called to determine what happens.
 */
public class ObjectInteraction {
    private Player player;
    private Rectangle playerHitbox;
    private ArrayList<SuperObject> allObjects;
    private Inventory inventory;

    /**
     * Constructor for the ObjectInteraction class
     * @param player the player interacting with the objects
     * @param allObjects array of all objects present in the game
     */
    public ObjectInteraction(Player player, ArrayList<SuperObject> allObjects) {
        this.player = player;
        this.allObjects = allObjects;
        this.inventory = player.getInventory();
    }

    /**
     * Checks if player is interacting with an object. If yes, the object disappears and objectBehaviour is called to
     * determine what happens.
     */
    public void checkObjectInteraction(){
        playerHitbox = player.getHitbox();
        for (int itemIndex = 0; itemIndex < allObjects.size(); itemIndex++) {
            if(allObjects.get(itemIndex) != null) {
                if (playerHitbox.intersects(allObjects.get(itemIndex).getHitbox())) {
                    //PLAYERS HITBOX IS INSIDE A HITBOX OF AN EXISTING OBJECT
                    boolean interacted = objectBehaviour(allObjects.get(itemIndex));
                    if (interacted) {
                        allObjects.set(itemIndex, null);
                    }

                    LOGGER.info("Interacted with item with index " + itemIndex);

                }
            }
        }
    }

    /**
     * Determines the behavior of the object.
     * @param object the object to check the behavior for
     * @return returns true if the player has interacted with the object, false otherwise
     */
    private boolean objectBehaviour(SuperObject object){
        boolean interacted = false;
        if (Objects.equals(object.getName(), "chest") && player.playerWantsToOpenChest()){
            //OBJECT IS A CHEST
            player.getInventory().useKey();
            String chestContains = ((OBJ_Chest) object).getContains();
            if (chestContains.equals("shield")) {
                player.gainArmor(3);
                inventory.addItem(new Shield());
                LOGGER.info("Interacted with a shield");
            }
            if (chestContains.equals("potion")) {
                inventory.addItem(new Potion());
                LOGGER.info("Interacted with a potion");
            }
            if (chestContains.equals("sword")) {
                inventory.addItem(new Sword());
                player.setAttack(player.getAttack()+5);
                LOGGER.info("Interacted with a sword");
            }
            if (chestContains.equals("key")) {
                inventory.addItem(new Key());
                LOGGER.info("Interacted with a key");
            }
            interacted = true;
        } else if (Objects.equals(object.getName(), "finish line")){
            //OBJECT IS A FINISH LINE
            player.setGame_over(true);
            LOGGER.info("Interacted with a finish line");
            interacted = true;
        } else if (Objects.equals(object.getName(), "trap")) {
            //OBJECT IS A TRAP
            player.takeDamageFromTrap(10);
        } else {
            LOGGER.warning("Unknown item");
        }
        return interacted;
    }
}
