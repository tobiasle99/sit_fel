package cz.cvut.fel.pjv.items;

import java.awt.image.BufferedImage;

/**
 * Represents an item in the game.
 */
public class Item {
    /**
     * The name of the item.
     */
    private String name;
    /**
     * Determines if the item is useable or not.
     */
    private boolean useable = false;
    /**
     * The image representing the item.
     */
    private BufferedImage image;

    /**
     * Returns the image representing the item.
     * @return the image representing the item
     */
    public BufferedImage getImage() {
        return image;
    }

    /**
     * Returns the name of the item.
     * @return the name of the item
     */
    public String getName() {
        return name;
    }

    /**
     * Returns whether the item is useable or not.
     * @return true if the item is useable, false otherwise
     */
    public boolean isUseable() {
        return useable;
    }
}
