package cz.cvut.fel.pjv.entities.enemy;

import cz.cvut.fel.pjv.items.Key;
import cz.cvut.fel.pjv.main.display.GamePanel;

import java.awt.*;

import static cz.cvut.fel.pjv.main.display.GamePanel.tileSize;

/**
 * Class that contains an array of enemies, draws them and updates them.
 */
public class EnemyController {
    private Enemy[] allEnemies;

    /**
     * Constructs an EnemyController with a default array of enemies.
     */
    public EnemyController() {
        //DEFAULT SETTINGS
        allEnemies  = new Enemy[10];
        Skeleton skeleton1 = new Skeleton(new Key());
        allEnemies[0] = skeleton1;
        Goblin goblin1 = new Goblin(new Key());
        allEnemies[1] = goblin1;
    }

    /**
     * Returns the array of all enemies.
     *
     * @return the array of all enemies
     */
    public Enemy[] getAllEnemies() {
        return allEnemies;
    }

    /**
     * Sets the array of all enemies.
     *
     * @param allEnemies the array of all enemies
     */
    public void setAllEnemies(Enemy[] allEnemies) {
        for (int i = 0; i < allEnemies.length; i++){
            this.allEnemies[i] = allEnemies[i];
        }
    }

    /**
     * Draws all visible enemies on the screen.
     *
     * @param g2 the Graphics2D object to use for drawing
     */
    public void draw(Graphics2D g2){
        for (Enemy enemy: allEnemies){
            if (enemy != null && enemyIsVisible(enemy)) {
                int[] worldCoordinates = GamePanel.getPlayer().getWorldPosition();
                enemy.setPositionOnScreen(new int[]{enemy.getPosition()[0]-worldCoordinates[0], enemy.getPosition()[1]});
                g2.drawImage(enemy.getEnemySprites().getCurrentSprite(), enemy.getPositionOnScreen()[0],
                        enemy.getPositionOnScreen()[1]-tileSize, null);
            }
        }
    }

    /**
     * Nullifies an enemy in the array at the given index. Called after the player wins combat against that enemy.
     *
     * @param index the index of the enemy to kill
     */
    public void killEnemy(int index){
        if (index != 999 && allEnemies[index] != null) {
            allEnemies[index] = null;
        }
    }

    /**
     * Determines if the given enemy is currently visible on the screen.
     *
     * @param enemy the enemy to check
     * @return true if the enemy is visible on the screen, false otherwise
     */
    private boolean enemyIsVisible(Enemy enemy){
        int[] enemyPosition = enemy.getPosition();
        int[] worldCoordinates = GamePanel.getPlayer().getWorldPosition();
        return (enemyPosition[0] < GamePanel.screenWidth + worldCoordinates[0] &&
                enemyPosition[0] > worldCoordinates[0]);
    }

    /**
     * Calls the update method for all existing enemies.
     */
    public void update(){
        for (Enemy enemy: allEnemies){
            if (enemy != null){
                enemy.update();
            }
        }
    }
}
