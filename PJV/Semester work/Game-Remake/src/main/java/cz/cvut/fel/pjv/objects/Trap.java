package cz.cvut.fel.pjv.objects;

import cz.cvut.fel.pjv.main.display.GamePanel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

import static cz.cvut.fel.pjv.entities.player.PlayerSprites.scaledImage;
import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;
import static cz.cvut.fel.pjv.main.display.GamePanel.screenHeight;
import static cz.cvut.fel.pjv.main.display.GamePanel.tileSize;

public class Trap extends SuperObject {
    private int[] position = new int[]{15*tileSize, GamePanel.screenHeight-4*tileSize};
    private int[] positionOnScreen = position;
    private BufferedImage image;

    @Override
    public BufferedImage getImage() {
        return image;
    }

    Rectangle hitbox = new Rectangle((int) position[0], (int) position[1], tileSize, tileSize/3);

    String name = "trap";
    String contains;
    boolean damaged = false;

    private void loadObjectImage(){
        try {
            image = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                    "/objects/spike_trap.png")));
            image = scaledImage(image, tileSize, tileSize);

        } catch (IOException e){
            e.printStackTrace();
            LOGGER.warning("UNABLE TO LOAD IMAGE FOR " + name);
        }
    }

    @Override
    public int[] getPositionOnScreen() {
        return positionOnScreen;
    }

    @Override
    public void setPositionOnScreen(int[] positionOnScreen) {
        this.positionOnScreen = positionOnScreen;
        hitbox.x = positionOnScreen[0];
    }

    public Trap() {
        loadObjectImage();
    }

    public void setPosition(int[] position) {
        this.position = position;
        hitbox.x = position[0];
        hitbox.y = position[1];
    }

    public Rectangle getHitbox() {
        return hitbox;
    }

    @Override
    public int[] getPosition() {
        return this.position;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getContains() {
        return contains;
    }

    public boolean hasDamagedPlayer() {
        return damaged;
    }

    public void setDamaged(boolean damaged) {
        this.damaged = damaged;
    }
}
