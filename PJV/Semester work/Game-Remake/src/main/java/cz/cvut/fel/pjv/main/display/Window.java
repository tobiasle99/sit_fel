package cz.cvut.fel.pjv.main.display;

import cz.cvut.fel.pjv.main.save.Load;
import cz.cvut.fel.pjv.main.save.Save;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;

import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;

/**
 * This class creates and manages the main window of the game.
 */
public class Window {
    static JToolBar toolBar;
    private static JLabel loggingLevelLabel = new JLabel("Current Logging Level: "
            + LOGGER.getLevel().toString());

    /**
     * Starts the JFrame and adds a toolbar with a save, load and toggle logging button.
     * Also initializes a GamePanel and adds it to the window.
     */
    public void start(){
        //TODO: Create buttons in JFrame to load and save game
        JFrame window  = new JFrame(); //create an instance of window
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setTitle("REMAKE GAME");

        GamePanel gamePanel = new GamePanel();
        Save save = new Save();
        Load load = new Load();
        window.add(gamePanel);

        toolBar = new JToolBar();
        toolBar.setFocusable(false);
        window.add(toolBar, BorderLayout.NORTH);
        JButton saveButton = save.createSaveButton(gamePanel, window);
        JButton loadButton = load.createLoadButton(gamePanel);
        JButton loggerButton = createLoggerButton();
        saveButton.setFocusable(false);
        loadButton.setFocusable(false);
        loggerButton.setFocusable(false);
        toolBar.add(loadButton);
        toolBar.add(saveButton);
        toolBar.add(loggerButton);
        toolBar.add(loggingLevelLabel);


        window.pack();
        window.setLocationRelativeTo(null); //relative to top left of the screen
        window.setVisible(true);

        gamePanel.startGameThread();
    }

    /**
     * Sets the visibility of the toolbar during combat.
     * @param visible True to make the toolbar visible, false to hide it.
     */
    public static void setToolBarVisibility(boolean visible){
        toolBar.setVisible(visible);
    }

    /**
     * Creates a button to toggle the logging level.
     * @return A JButton object that toggles the logging level when clicked.
     */
    private static JButton createLoggerButton(){
        JButton toggleButton = new JButton("Toggle Logging Level");
        toggleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (LOGGER.getLevel() == Level.WARNING) {
                    LOGGER.setLevel(Level.INFO);
                } else {
                    LOGGER.setLevel(Level.WARNING);
                }
                loggingLevelLabel.setText("Current Logging Level: "
                        + LOGGER.getLevel().toString());
            }
        });
        return toggleButton;
    }
}
