package cz.cvut.fel.pjv.entities.player;

import cz.cvut.fel.pjv.main.display.GamePanel;

import java.awt.*;

import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;
import static cz.cvut.fel.pjv.main.display.GamePanel.*;

/**
 * Represents a player object in the game world. The player has health, attack, armor, inventory,
 * and position in the world.
 * The class contains methods to update the player's position and draw the current sprite,
 * as well as getters and setters for the player's properties.
 */
public class Player{
    private int health = 100;
    private int attack = 10;
    private int tileSize = GamePanel.tileSize;
    private int[] position = new int[]{tileSize, screenHeight-6*tileSize};
    private PlayerControls controls;
    private PlayerSprites playerSprites;
    private int[] worldPosition = new int[]{0, 0};
    private boolean isRunning = false;
    private String direction = "right";
    private boolean game_over = false;
    private boolean dead = false;
    private Font arial40;
    private boolean invincible;
    private int invincibleTime = 0;
    int gravity = 2;            // vertical acceleration due to gravity
    int jumpVelocity = 20;      // initial upward velocity when jumping
    int verticalVelocity = 0;   // current vertical velocity
    boolean canJump = true;
    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public PlayerSprites getPlayerSprites() {
        return playerSprites;
    }

    public int[] getPosition() {
        return position;
    }

    Rectangle hitbox = new Rectangle(position[0]+tileSize, 0, tileSize, tileSize);
    int speed = 5;
    int armor = 3;

    boolean inCombat = false;

    Inventory inventory;

    /**
     * Constructor for Player object.
     * @param controls PlayerControls object to handle player inputs.
     */
    public Player(PlayerControls controls) {
        this.inventory = new Inventory();
        this.controls = controls;
        this.playerSprites = new PlayerSprites();
        arial40 = new Font("Arial", Font.PLAIN, 40);
    }

    /**
     * Updates the player's position in the world and cycles their sprites. Handles movement based on directional keys
     * and jump key presses. Also handles gravity and jumping mechanics. If in combat mode, only sprite cycling will be
     * updated.
     */
    public void update(){
        // Check if any of the directional keys or jump key is pressed and not in combat mode
        if ((controls.leftPressed || controls.rightPressed || controls.jumpPressed) && !inCombat) {
            //LEFT OR RIGHT KEY IS PRESSED
            if (controls.leftPressed) {
                direction = "left";
                if (worldPosition[0] > -50) {
                    worldPosition[0] -= 10;
                    isRunning = true;
                } else if (position[0] > -tileSize/2) {
                    setPosition(new int[]{position[0]-10, position[1]});
                }
            }
            if (controls.rightPressed) {
                direction = "right";
                if (position[0] >= tileSize) {
                    worldPosition[0] += 10;
                    isRunning = true;
                } else {
                    setPosition(new int[]{position[0]+10, position[1]});
                }
            }
        } else {
            isRunning = false;
        }

        // GRAVITY AND JUMPING
        if (controls.jumpPressed && canJump && !inCombat) {

            // set the player's vertical velocity to jumpSpeed
            verticalVelocity = jumpVelocity;
            // can't jump again until the player hits the ground
            canJump = false;
        } else if (!canJump) {
            // apply gravity to the player's vertical velocity
            verticalVelocity -= gravity;
        }
        // update the player's vertical position based on their vertical velocity
        setPosition(new int[]{position[0], position[1]-verticalVelocity}) ;
        // check if the player has hit the ground
        if (position[1] > groundHeight+10) {
            setPosition(new int[]{position[0], groundHeight}) ;
            // reset the player's vertical velocity and allow jumping again
            verticalVelocity = 0;
            canJump = true;
        }

        if (invincible){
            invincibleTime++;
        }

        playerSprites.cycleSprite(isRunning, inCombat, direction);
    }


    /**
     * Draws the player's current sprite.
     * @param g2 Graphics2D object used to draw the player.
     */
    public void draw(Graphics2D g2) {
        g2.setFont(arial40);
        g2.setColor(Color.WHITE);
        g2.drawImage(playerSprites.getCurrentSprite() ,position[0], position[1], null);
        if (!isInCombat()) {
            g2.drawString("Player health: " + getHealth(), tileSize, 50);
        }
    }

    /**
     * Gets the player's hitbox.
     * @return The player's hitbox as a Rectangle object.
     */
    public Rectangle getHitbox() {
        return hitbox;
    }

    /**
     * Gets the player's health.
     * @return The player's health as an integer.
     */
    public int getHealth() {
        return health;
    }

    /**
     * Sets whether the player is currently in combat or not.
     * @param inCombat a boolean value indicating whether the player is in combat or not.
     */
    public void setPlayerInCombat(boolean inCombat){
        this.inCombat = inCombat;
    }

    /**
     * Returns a boolean value indicating whether the player is currently in combat or not.
     * @return true if the player is in combat, false otherwise.
     */
    public boolean isInCombat() {
        return inCombat;
    }

    /**
     * Gets the player's attack.
     * @return The player's attack as an integer.
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Returns the speed of the player.
     * @return the speed of the player
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * Returns the armor value of the player.
     * @return the armor value of the player
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Reduces the health of the player by the given damage value.
     * @param damage the amount of damage to subtract from the player's health
     */
    public void takeDamage(int damage){
        health-= damage;
    }

    /**
     * This method is used to inflict damage on the player when they come into contact with a trap object in the game.
     * If the player is not invincible, their health will be reduced by the amount of damage specified.
     * If the player becomes invincible after taking damage, they will remain invincible for 30 frames.
     * If the player's health falls to or below 0, the game will be over.
     * @param damage the amount of damage to be inflicted on the player
     */
    public void takeDamageFromTrap(int damage){
        if (!invincible && health > 0) {
            playerSprites.takeDamage();
            invincible = true;
            health -= damage;
        }
        if (invincibleTime > 30){
            invincible = false;
            invincibleTime = 0;
        }
        if (health <= 0){
            if (!playerSprites.isMidAnimation()) {
                playerSprites.die();
            }
            if(playerSprites.getDied()) {
                game_over = true;
            }
        }
    }


    /**
     * Increases the armor of the player by the given defense value.
     * @param defense the amount of defense to add to the player's armor
     */
    public void gainArmor(int defense) { armor+= defense;}

    /**
     * Returns the inventory of the player.
     * @return the inventory of the player
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Sets the player's health.
     * @param health The player's new health value.
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * Sets the attack value of the player.
     * @param attack the new attack value of the player
     */
    public void setAttack(int attack) {
        this.attack = attack;
    }

    /**
     * Sets the speed of the player.
     * @param speed the new speed of the player
     * @param speed
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * Sets the armor value of the player.
     * @param armor the new armor value of the player
     */
    public void setArmor(int armor) {
        this.armor = armor;
    }

    /**
     * Sets the inventory of the player.
     * @param inventory the new inventory of the player
     */
    public void setInventory(Inventory inventory) {
        this.inventory.setItems(inventory.getItems());
    }

    /**
     * Sets the position on screen of the player to the given position.
     * @param position the new position on screen of the player as an array of two integers (x and y)
     */
    public void setPosition(int[] position) {
        this.position = position;
        this.hitbox.x = position[0]+tileSize/2;
        this.hitbox.y = position[1]+tileSize;
        LOGGER.finest("Set player's position to " + position[0] + " " + position[1]);
    }

    /**
     * Returns the world position of the player.
     * @return the world position of the player as an array of two integers (x and y)
     */
    public int[] getWorldPosition() {
        return worldPosition;
    }

    /**
     * Sets the world position of the player to the given position.
     * @param worldPosition the new world position of the player as an array of two integers (x and y)
     */
    public void setWorldPosition(int[] worldPosition) {
        this.worldPosition = worldPosition;
    }

    /**
     * Sets the game_over variable to the given value.
     * @param game_over the new value of the game_over variable
     */
    public void setGame_over(boolean game_over) {
        this.game_over = game_over;
        LOGGER.info("Player state = game over " + game_over);
    }

    /**
     * Returns true if the game is over, false otherwise.
     * @return true if the game is over, false otherwise
     */
    public boolean isGame_over() {
        return game_over;
    }

    /**
     * Returns true if the player has a key and the interaction button is pressed, false otherwise.
     * @return true if the player has a key and the interaction button is pressed, false otherwise
     */
    public boolean playerWantsToOpenChest(){
        LOGGER.info("HASKEY = " + inventory.hasKey() + " interactionPressed = " + controls.interactionPressed);
        return inventory.hasKey() && controls.interactionPressed;
    }
}
