package cz.cvut.fel.pjv.entities.enemy;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;
import static cz.cvut.fel.pjv.main.display.GamePanel.tileSize;
import static java.lang.Math.round;

public class GoblinSprites extends EnemySprites{
    private BufferedImage[] idleSprites;
    private BufferedImage currentSprite;
    private BufferedImage[] attackSprites;
    private BufferedImage[] hurtSprites;
    private BufferedImage[] deathSprites;
    private boolean attacking = false;
    private boolean takingDamage = false;
    private boolean dying = false;
    private int spriteTime;
    private int animationIndex;
    private int animationTime;
    private boolean midAnimation = false;
    private int currentSpriteIndex = 0;
    private boolean died = false;

    public GoblinSprites() {
        this.idleSprites = new BufferedImage[4];
        this.attackSprites = new BufferedImage[8];
        this.hurtSprites = new BufferedImage[4];
        this.deathSprites = new BufferedImage[4];
        getEnemyImage();
        currentSprite = idleSprites[0];
        spriteTime = 0;
        animationIndex = 0;
        animationTime = 0;
    }

    private void getEnemyImage(){
        try{

            //LOAD IDLE SPRITES
            for (int i = 0; i < idleSprites.length; i++){
                idleSprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/goblin/goblin_idle" + i + ".png")));
                idleSprites[i] = scaledImage(idleSprites[i], (int) round(3*tileSize), 3*tileSize);
            }

            //LOAD ATTACK SPRITES
            for (int i = 0; i < attackSprites.length; i++){
                attackSprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/goblin/goblin_attack_" + i + ".png")));

                attackSprites[i] = scaledImage(attackSprites[i], (int) round(3*tileSize), (int) round(3*tileSize));

            }

            //LOAD HURT SPRITES
            for (int i = 0; i < hurtSprites.length; i++){
                hurtSprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/goblin/goblin_hit" + i + ".png")));

                hurtSprites[i] = scaledImage(hurtSprites[i], (int) round(3*tileSize), 3*tileSize);

            }


            //LOAD DEATH SPRITES
            for (int i = 0; i < deathSprites.length; i++){
                deathSprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                        "/entities/goblin/goblin_death" + i + ".png")));

                deathSprites[i] = scaledImage(deathSprites[i], (int) round(3*tileSize), 3*tileSize);

            }
            LOGGER.info("Successfully loaded goblin sprites");
        } catch (IOException e) {

            e.printStackTrace();
            LOGGER.warning("Failed to load goblin sprites");
        }
    }

    public BufferedImage getCurrentSprite() {
        return currentSprite;
    }

    @Override
    public void cycleSprite(){
        if (midAnimation){
            if (attacking) {
                attack();
            } else if (takingDamage){
                takeDamage();
            } else if (dying){
                die();
            }
            animationTime++;
        } else {
            if (currentSpriteIndex >= idleSprites.length - 1) {
                currentSpriteIndex = 0;
            }
            if (spriteTime > 20) {
                if (currentSpriteIndex >= idleSprites.length - 1) {
                    currentSpriteIndex = 0;
                } else {
                    currentSpriteIndex++;
                }
                spriteTime = 0;
            }
            currentSprite = idleSprites[currentSpriteIndex];
        }
        spriteTime++;
    }

    @Override
    public void attack(){
        midAnimation = true;
        attacking = true;
        if (animationTime > 5){
            if (animationIndex < attackSprites.length-1){
                animationIndex++;
                animationTime = 0;
            }
            else {
                attacking = false;
                midAnimation = false;
                animationIndex = 0;
                animationTime = 0;
            }
        }
        currentSprite = attackSprites[animationIndex];
    }

    @Override
    public void takeDamage(){
        midAnimation = true;
        takingDamage = true;
        if (animationTime > 13){
            if (animationIndex < hurtSprites.length-1){
                animationIndex++;
                animationTime = 0;
            }
            else {
                takingDamage = false;
                midAnimation = false;
                animationIndex = 0;
                animationTime = 0;
            }
        }
        currentSprite = hurtSprites[animationIndex];
    }

    @Override
    public boolean getDied(){
        return died;
    }

    @Override
    public boolean getMidAnimation() {
        return midAnimation;
    }

    @Override
    public void die(){
        midAnimation = true;
        dying = true;
        if (animationTime > 10){
            if (animationIndex < deathSprites.length-1){
                animationIndex++;
                animationTime = 0;
            }
            else {
                dying = false;
                midAnimation = false;
                died = true;
                animationIndex = 0;
                animationTime = 0;
            }
        }
        currentSprite = deathSprites[animationIndex];
    }
}
