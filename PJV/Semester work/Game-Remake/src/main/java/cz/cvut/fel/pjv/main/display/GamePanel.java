package cz.cvut.fel.pjv.main.display;

import cz.cvut.fel.pjv.entities.enemy.Enemy;
import cz.cvut.fel.pjv.entities.enemy.EnemyController;
import cz.cvut.fel.pjv.entities.player.Player;
import cz.cvut.fel.pjv.entities.player.PlayerCombat;
import cz.cvut.fel.pjv.entities.player.PlayerControls;
import cz.cvut.fel.pjv.main.save.Save;
import cz.cvut.fel.pjv.main.update.UpdateController;
import cz.cvut.fel.pjv.objects.ObjectController;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;

import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;

/**
 * The GamePanel class represents a panel that displays the game and handles the game logic.
 * It extends JPanel and implements Runnable, allowing it to handle key events and run the game loop in a separate thread.
 * The panel has various screen settings, including the tile size, screen width, and screen height.
 * It also has fields for the player, current enemy, and combat user interface.
 * The panel handles updates to the game state, drawing of the game, and saving of the game using different controllers.
 */
public class GamePanel extends JPanel implements Runnable {

    //SCREEN SETTINGS
    static final int originalTileSize = 16; //tile is a 16x16 pixels
    static final int scale = 3; //scale factor of originalTileSize
    public static final int tileSize = originalTileSize*scale; // scaling up to 48x48
    private static final int maxScreenCol = 2*16; //how many tiles to display on the screen
    private static final int maxScreenRow = (maxScreenCol/16) * 9; // 16:9 ratio
    public static final int screenWidth = tileSize * maxScreenCol; // 1536px
    public static final int screenHeight = tileSize * maxScreenRow; // 864px
    public static final int groundHeight = screenHeight-6*tileSize;     // y-coordinate of ground level

    static PlayerControls playerControls = new PlayerControls();
    static Player player = new Player(playerControls);
    Enemy currentEnemy;
    CombatUI combatUI;
    PlayerCombat playerCombat;
    boolean combatStarted = false;

    //THREADS
    Thread gameThread = new Thread(this);

    /**
     * Source: RyiSnow
     */
    //GAME SETTINGS
    private int FPS = 60;
    private double drawInterval = 1000000000 / FPS; // 0.016 seconds

    private EnemyController enemyController = new EnemyController();
    private ObjectController objectController = new ObjectController();

    private UpdateController updateController = new UpdateController(player, objectController, enemyController);
    private DrawController drawController = new DrawController(player, objectController, enemyController);

    private Save save = new Save();

    /**
     * Source: RyiSnow
     * Constructs a GamePanel object with default settings for the displayed window.
     * Adds a key listener to the panel and sets it to be focusable.
     */
    public GamePanel() {
        //constructor for the displayed window
        this.setPreferredSize(new Dimension(screenWidth, screenHeight));
        this.setBackground(Color.black);
        this.setDoubleBuffered(true); //better rendering performance
        this.addKeyListener(playerControls); //add our key listener to this panel
        this.setFocusable(true); //our window can now receive key inputs



    }

    /**
     * Source: RyiSnow
     * Starts a new thread for the game.
     */
    public void startGameThread() {
        gameThread = new Thread(this); //start a thread in this panel
        gameThread.start(); //automatically calls run()
    }

    /**
     * Source: RyiSnow
     * The game loop that runs in a separate thread.
     * Calls update() and repaint() methods for the panel at the appropriate intervals.
     */
    @Override
    public void run() {
        double delta = 0;
        long lastTime = System.nanoTime();
        long currentTime;
        while (gameThread != null) {
            currentTime = System.nanoTime();
            delta += (currentTime - lastTime)/drawInterval;
            lastTime = currentTime;

            if (delta >= 1) {
                delta--;
                update();
                repaint();
            }

        }
    }

    /**
     * Updates the game state by calling updateController.update(), and handles combat logic if the player is in combat.
     */
    public void update() {
        if (!player.isGame_over()) {
            updateController.update();
            if (player.isInCombat()) {
                if (!combatStarted) {
                    startCombat();
                }
                playerCombat.combat();
                if (currentEnemy.isDead()) {
                    finishCombat();
                }

            }
        }
    }

    /**
     * Paints the game by calling drawController.draw() or drawController.drawCombat() depending on whether the player is in combat.
     * Also handles game over scenarios by calling drawController.drawDeathMessage() or drawController.drawGameOverMessage().
     * @param g the Graphics object to be painted.
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g); //super is JPanel
        Graphics2D g2 = (Graphics2D) g;
        if(!player.isGame_over()) {
            if (!player.isInCombat()) {
                drawController.draw(g2);
            } else {
                if (combatUI != null) {
                    drawController.drawCombat(g2, combatUI);
                } else {
                    startCombat();
                }
            }
        } else {
            drawController.draw(g2);
            if (player.getHealth() <= 0){
                drawController.drawDeathMessage(g2);
            } else {
                drawController.drawGameOverMessage(g2);
            }
            gameThread = null;
        }
    }

    /**
     * Sets the combat UI for the current combat.
     * @param combatUI : the CombatUI to set for the combat.
     */
    public void setCombatUI(CombatUI combatUI) {
        this.combatUI = combatUI;
    }


    /**
     * Starts the combat sequence in the game.
     * This method sets the toolbar visibility to false and sets the current enemy in combat.
     * If the enemy is not null, it sets the combat UI for the player and starts the player combat.
     */
    private void startCombat(){

        Window.setToolBarVisibility(false);
        currentEnemy = enemyController.getAllEnemies()[updateController.getEnemyIndex()];

        if (updateController.getEnemyIndex() != 999) {
            //ENEMY IS NOT NULL
            LOGGER.info("Starting combat with enemy number " + updateController.getEnemyIndex());
            combatStarted = true;
            setCombatUI(new CombatUI(player, currentEnemy));
            playerCombat = new PlayerCombat(player, currentEnemy, playerControls, combatUI);
        }
    }

    /**
     * Finishes the combat sequence in the game.
     * This method sets the toolbar visibility to true, nullifies the combat UI,
     * kills the current enemy in combat, nullifies the player combat object, and sets the combat started boolean to false.
     */
    private void finishCombat(){
        Window.setToolBarVisibility(true);
        combatUI = null;
        enemyController.killEnemy(updateController.getEnemyIndex());
        playerCombat = null;
        combatStarted = false;

    }

    /**
     * Calls the save class to save the game in a JSON format.
     * @return JSONObject a JSON object containing all the saved data.
     */
    public JSONObject save(){
        return save.saveGame(objectController.getAllObjects(), player, enemyController.getAllEnemies());
    }

    /**
     * Gets the object controller for the game.
     * @return ObjectController the ObjectController object for the game.
     */
    public ObjectController getObjectController() {
        return objectController;
    }

    /**
     * Gets the enemy controller for the game.
     * @return EnemyController the EnemyController object for the game.
     */
    public EnemyController getEnemyController() {
        return enemyController;
    }

    /**
     * Gets the player object for the game.
     * @return Player the Player object for the game.
     */
    public static Player getPlayer() {
        return player;
    }

    public UpdateController getUpdateController() {
        return updateController;
    }
}
