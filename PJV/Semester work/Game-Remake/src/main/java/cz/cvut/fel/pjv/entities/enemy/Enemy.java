package cz.cvut.fel.pjv.entities.enemy;

import cz.cvut.fel.pjv.entities.player.SkeletonSprites;
import cz.cvut.fel.pjv.items.Item;

import java.awt.*;

/**
 * Superclass for enemies
 */
public class Enemy implements EnemyInterface{
    private String name;
    private int health;
    private int[] position;
    private int[] positionOnScreen;
    private Rectangle aura;
    private int speed, armor, attack;
    private double chanceOfHitting;
    private boolean isDead;
    private EnemySprites enemySprites;
    private Item drop;
    private boolean itemDropped = false;

    /**
     * Checks if an item has been dropped.
     *
     * @return True if an item has been dropped, false otherwise
     */
    public boolean isItemDropped() {
        return itemDropped;
    }

    /**
     * Sets the flag indicating if an item has been dropped.
     *
     * @param itemDropped True if an item has been dropped, false otherwise
     */
    public void setItemDropped(boolean itemDropped) {
        this.itemDropped = itemDropped;
    }

    /**
     * Gets the item that the enemy drops.
     *
     * @return The item that the enemy drops
     */
    public Item getDrop() {
        return drop;
    }

    public Enemy() {
        this.name = "enemy";
        this.health = 1;
        this.position = new int[]{1000, 1000};
        this.positionOnScreen = new int[]{1000, 1000};
        this.aura = new Rectangle();
        this.speed = 1;
        this.armor = 1;
        this.attack = 1;
        this.chanceOfHitting = 0;
        this.isDead = true;
        this.enemySprites = new EnemySprites();
    }

    /**
     * Gets the sprites class for the enemy.
     *
     * @return The sprites for the enemy
     */
    public EnemySprites getEnemySprites() {
        return enemySprites;
    }

    /**
     * Updates the state of the enemy.
     */
    public void update(){

    }


    /**
     * Sets the name of the enemy.
     *
     * @param name The name of the enemy
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the chance of hitting of the enemy.
     *
     * @param chanceOfHitting The chance of hitting of the enemy
     */
    public void setChanceOfHitting(double chanceOfHitting) {
        this.chanceOfHitting = chanceOfHitting;
    }

    /**
     * Gets the position of the enemy on screen.
     *
     * @return The position of the enemy on screen
     */
    public int[] getPositionOnScreen() {
        return positionOnScreen;
    }

    /**
     * Sets the position of the enemy on screen.
     *
     * @param positionOnScreen The position of the enemy on screen
     */
    public void setPositionOnScreen(int[] positionOnScreen) {
        this.positionOnScreen = positionOnScreen;
    }

    /**
     * Gets the position of the enemy.
     *
     * @return The position of the enemy
     */
    public int[] getPosition() {
        return position;
    }

    /**
     * Gets the aura rectangle of the enemy.
     *
     * @return The aura rectangle of the enemy
     */
    public Rectangle getAura() {
        return aura;
    }

    /**
     * Gets the health of the enemy.
     *
     * @return The health of the enemy
     */
    public int getHealth() {
        return health;
    }

    /**
     * Gets the name of the enemy.
     *
     * @return The name of the enemy
     */
    public String getName() {
        return name;
    }

    /**
     * Takes damage from an attack.
     *
     * @param damage The amount of damage to take
     */
    public void takeDamage(int damage) {
        this.health -= damage;
    }

    /**
     * Returns the speed of the enemy.
     *
     * @return the speed of the enemy
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * Returns the armor of the enemy.
     *
     * @return the armor of the enemy
     */
    public int getArmor() {
        return armor;
    }

    /**
     *Returns the attack of the enemy.
     *
     * @return the attack of the enemy
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Returns the chance of hitting of the enemy.
     *
     * @return the chance of hitting of the enemy
     */
    public double getChanceOfHitting() {
        return chanceOfHitting;
    }

    /**
     * Returns whether the enemy is dead or not.
     *
     * @return true if the enemy is dead, false otherwise
     */
    public boolean isDead() {
        return isDead;
    }

    /**Sets the state of the enemy to dead or alive.
     *
     *@param dead true if the enemy is dead, false otherwise
     */
    public void setDead(boolean dead) {
        isDead = dead;
    }

    /**
     * Sets the position of the enemy.
     *
     * @param ints an array containing the x and y coordinates of the position
     */
    public void setPosition(int[] ints) {
        this.position = ints;
    }

    /**
     * Sets the health of the enemy.
     *
     * @param health the new health value of the enemy
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * Sets the aura of the enemy.
     *
     * @param aura the new aura rectangle of the enemy
     */
    public void setAura(Rectangle aura) {
        this.aura = aura;
    }

    /**
     * Sets the speed of the enemy.
     *
     * @param speed the new speed value of the enemy
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * Sets the armor of the enemy.
     *
     * @param armor the new armor value of the enemy
     */
    public void setArmor(int armor) {
        this.armor = armor;
    }

    /**
     * Sets the attack of the enemy.
     *
     * @param attack the new attack value of the enemy
     */
    public void setAttack(int attack) {
        this.attack = attack;
    }

    /**
     * Draws the aura of the enemy using the given Graphics2D object.
     *
     * @param g2 the Graphics2D object used to draw the aura
     */
    public void drawAura(Graphics2D g2){
        g2.drawRect(aura.x, aura.y, aura.width, aura.height);
    }

    /**
     * Sets the attack of the enemy.
     *
     * @param drop item to be dropped by the enemy
     */
    public void setDrop(Item drop) {
        this.drop = drop;
    }
}
