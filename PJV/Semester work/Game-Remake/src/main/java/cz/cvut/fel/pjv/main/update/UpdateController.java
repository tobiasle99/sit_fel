package cz.cvut.fel.pjv.main.update;

import cz.cvut.fel.pjv.entities.enemy.Enemy;
import cz.cvut.fel.pjv.entities.enemy.EnemyController;
import cz.cvut.fel.pjv.entities.enemy.EnemyInteraction;
import cz.cvut.fel.pjv.entities.player.ObjectInteraction;
import cz.cvut.fel.pjv.entities.player.Player;
import cz.cvut.fel.pjv.entities.player.PlayerControls;
import cz.cvut.fel.pjv.main.display.GamePanel;
import cz.cvut.fel.pjv.objects.ObjectController;
import cz.cvut.fel.pjv.objects.SuperObject;

public class UpdateController {
    Player player;
    ObjectController objectController;
    ObjectInteraction objectInteraction;
    EnemyController enemyController;
    EnemyInteraction enemyInteraction;
    int enemyIndex;;

    public UpdateController(Player player, ObjectController objectController, EnemyController enemyController) {
        this.player = player;
        this.objectController = objectController;
        this.objectInteraction = new ObjectInteraction(player, objectController.getAllObjects());
        this.enemyController = enemyController;
        this.enemyInteraction = new EnemyInteraction(player, enemyController.getAllEnemies());
    }

    public void update(){
        player.update();
        enemyController.update();
        objectInteraction.checkObjectInteraction();
        enemyIndex = enemyInteraction.checkEnemyInteraction();
        if (enemyIndex != 999){
            player.setPlayerInCombat(true);
        }
    }

    public int getEnemyIndex() {
        return enemyIndex;
    }

    public void setObjectInteraction(ObjectInteraction objectInteraction) {
        this.objectInteraction = objectInteraction;
    }
}
