package cz.cvut.fel.pjv.entities.enemy;

import cz.cvut.fel.pjv.items.Item;
import cz.cvut.fel.pjv.main.display.GamePanel;

import java.awt.*;

import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;
import static cz.cvut.fel.pjv.main.display.GamePanel.screenHeight;

public class Goblin extends Enemy implements EnemyInterface{
    private String name = "Goblin";
    private int health = 60;
    private int attack = 10;
    private int speed = 3;
    private int armor = 5;
    private boolean isDead = false;
    private double chanceOfHitting = 0.7;
    private int tileSize = GamePanel.tileSize;
    private int[] position = new int[]{30*tileSize, screenHeight-6*tileSize};
    private int[] positionOnScreen = position;
    private Rectangle aura = new Rectangle(position[0]- tileSize, 0, 3*tileSize, screenHeight);
    private GoblinSprites goblinSprites = new GoblinSprites();
    private Item drop;
    private boolean itemDropped = false;

    public boolean isItemDropped() {
        return itemDropped;
    }

    public void setItemDropped(boolean itemDropped) {
        this.itemDropped = itemDropped;
    }

    public Goblin(Item drop) {
        this.drop = drop;
    }

    @Override
    public Item getDrop() {
        return drop;
    }

    @Override
    public EnemySprites getEnemySprites() {
        return goblinSprites;
    }

    @Override
    public int[] getPositionOnScreen() {
        return positionOnScreen;
    }

    public void setPositionOnScreen(int[] positionOnScreen) {
        this.positionOnScreen = positionOnScreen;
        aura.x = positionOnScreen[0]- tileSize;
    }

    public void update() {
        goblinSprites.cycleSprite();
    }

    public int[] getPosition() {
        return position;
    }

    public Rectangle getAura() {
        return aura;
    }

    public String getName() {
        return name;
    }

    @Override
    public int getHealth() {
        return health;
    }

    public int getAttack() {
        return attack;
    }

    public void takeDamage(int damage) {
        this.health -= damage;
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    @Override
    public int getArmor() {
        return armor;
    }

    @Override
    public double getChanceOfHitting() {
        return chanceOfHitting;
    }

    @Override
    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    @Override
    public void setHealth(int health) {
        this.health = health;
    }

    @Override
    public void setAttack(int attack) {
        this.attack = attack;
    }

    @Override
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public void setArmor(int armor) {
        this.armor = armor;
    }

    public void setChanceOfHitting(double chanceOfHitting) {
        this.chanceOfHitting = chanceOfHitting;
    }

    @Override
    public void setPosition(int[] position) {
        this.position = position;
        this.positionOnScreen = position;
        LOGGER.info("Set goblin's position to " + position[0] + " " + position[1]);
    }

    public void drawAura(Graphics2D g2){
        g2.drawRect(aura.x, aura.y, aura.width, aura.height);
    }
}
