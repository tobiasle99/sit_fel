package cz.cvut.fel.pjv.objects;

import cz.cvut.fel.pjv.main.display.GamePanel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

import static cz.cvut.fel.pjv.entities.player.PlayerSprites.scaledImage;
import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;
import static cz.cvut.fel.pjv.main.display.GamePanel.screenHeight;
import static cz.cvut.fel.pjv.main.display.GamePanel.tileSize;

public class OBJ_Chest extends SuperObject{

    private int[] position = new int[]{6*tileSize, GamePanel.screenHeight-4*tileSize};
    private int[] positionOnScreen = position;
    private BufferedImage image;

    @Override
    public BufferedImage getImage() {
        return image;
    }

    private void loadObjectImage(){
        try {
            image = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(
                    "/objects/chest_2.png")));
            image = scaledImage(image, tileSize, tileSize);

        } catch (IOException e){
            e.printStackTrace();
            LOGGER.warning("UNABLE TO LOAD IMAGE FOR " + name);
        }
    }

    Rectangle hitbox = new Rectangle((int) position[0], (int) position[1], tileSize, screenHeight);

    String name = "chest";
    String contains;

    @Override
    public int[] getPositionOnScreen() {
        return positionOnScreen;
    }

    @Override
    public void setPositionOnScreen(int[] positionOnScreen) {
        this.positionOnScreen = positionOnScreen;
        hitbox.x = positionOnScreen[0];
    }

    public OBJ_Chest(String contains) {
        this.contains = contains;
        loadObjectImage();
    }

    public void setPosition(int[] position) {
        this.position = position;
        hitbox.x = position[0];
        hitbox.y = position[1];
    }

    public Rectangle getHitbox() {
        return hitbox;
    }

    @Override
    public int[] getPosition() {
        return this.position;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getContains() {
        return contains;
    }

    public void drawHitbox(Graphics2D g2){
        g2.draw(hitbox);
    }

}
