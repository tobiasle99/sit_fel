package cz.cvut.fel.pjv.objects;

import cz.cvut.fel.pjv.main.display.GamePanel;

import java.awt.*;
import java.awt.image.BufferedImage;

import static cz.cvut.fel.pjv.main.display.GamePanel.screenHeight;
import static cz.cvut.fel.pjv.main.display.GamePanel.tileSize;

public class FinishLine extends SuperObject {
    private int[] position = new int[]{15*tileSize, GamePanel.screenHeight-4*tileSize};
    private int[] positionOnScreen = position;
    private BufferedImage image;

    @Override
    public BufferedImage getImage() {
        //RETURNS NOTHING INTENTIONALLY INVISIBLE FINISH:))
        return image;
    }

    Rectangle hitbox = new Rectangle((int) position[0], (int) position[1], tileSize, screenHeight);

    String name = "finish line";
    String contains;

    @Override
    public int[] getPositionOnScreen() {
        return positionOnScreen;
    }

    @Override
    public void setPositionOnScreen(int[] positionOnScreen) {
        this.positionOnScreen = positionOnScreen;
        hitbox.x = positionOnScreen[0];
    }

    public FinishLine() {
        this.contains = contains;
    }

    public void setPosition(int[] position) {
        this.position = position;
        hitbox.x = position[0];
        hitbox.y = position[1];
    }

    public Rectangle getHitbox() {
        return hitbox;
    }

    @Override
    public int[] getPosition() {
        return this.position;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getContains() {
        return contains;
    }
}
