package cz.cvut.fel.pjv.entities.enemy;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

import static cz.cvut.fel.pjv.main.display.GamePanel.tileSize;
import static java.lang.Math.round;

/**
 * Class for handling enemy sprites and animations.
 */
public class EnemySprites {

    public boolean getMidAnimation(){return false;}

    private void getEnemyImage(){

    }


    /**
     * Gets the current sprite image for the enemy.
     *
     * @return the current sprite image
     */
    public BufferedImage getCurrentSprite() {
        return null;
    }


    /**
     * Cycles through the enemy's sprites for animation.
     */
    public void cycleSprite(){

    }

    /**
     * Executes an attack animation for the enemy.
     */
    public void attack(){

    }


    /**
     * Executes a damage-taking animation for the enemy.
     */
    public void takeDamage(){

    }

    /**
     * Executes a death animation for the enemy.
     */
    public void die(){

    }

    /**
     * Gets the value of the died variable, which indicates if the enemy is dead or not.
     *
     * @return true if the enemy is dead, false otherwise
     */
    public boolean getDied() {
        return false;
    }

    /**
     * Scales the given image to the specified dimensions.
     *
     * @param original the original image to be scaled
     * @param width the desired width of the scaled image
     * @param height the desired height of the scaled image
     * @return the scaled image
     */
    public static BufferedImage scaledImage (BufferedImage original, int width, int height) {
        //SCALING IMAGES BEFORE RENDERING
        BufferedImage scaledImage = new BufferedImage(width, height, original.getType());
        Graphics2D g2 = scaledImage.createGraphics();
        g2.drawImage(original, 0, 0, width, height, null);
        g2.dispose();
        return scaledImage;
    }
}
