package cz.cvut.fel.pjv.main.display;

import cz.cvut.fel.pjv.entities.enemy.Enemy;
import cz.cvut.fel.pjv.entities.player.Inventory;
import cz.cvut.fel.pjv.entities.player.Player;
import cz.cvut.fel.pjv.main.display.GamePanel;

import java.awt.*;

import static cz.cvut.fel.pjv.main.display.GamePanel.screenWidth;
import static cz.cvut.fel.pjv.main.display.GamePanel.tileSize;

/**
 * A class representing the user interface during combat, responsible for displaying necessary information
 *
 * such as health bars, turn information, enemy information, and messages.
 */
public class CombatUI {
    private Font arial40;
    public boolean messageOn = false;
    public String message = "";
    private int messageCounter = 0;
    private Player player;
    private Enemy enemy;
    private String turn = "blabla";
    private Inventory inventory;
    private String enemyName;
    private String enemyHealth;

    /**
     * Constructs a new CombatUI object with the specified player and enemy.
     * @param player The player object in combat
     * @param enemy The enemy object in combat
     */
    public CombatUI(Player player, Enemy enemy) {
        this.player = player;
        this.enemy = enemy;
        arial40 = new Font("Arial", Font.PLAIN, 40);
        this.inventory = player.getInventory();
        enemyName = "Enemy name: " + enemy.getName();
        enemyHealth = "Enemy health: " + enemy.getHealth();
    }

    /**
     * Displays a message at the top of the screen for a limited time.
     * @param text The text to be displayed
     */
    public void showMessage(String text) {
        message = text;
        messageOn = true;
        messageCounter = 0;
    }

    /**
     * Changes the displayed turn in the user interface.
     * @param turn The new turn to be displayed
     */
    public void setTurn(String turn) {
        this.turn = turn;
    }

    /**
     * Draws all necessary user interface elements during combat.
     * @param g2 The graphics object to be drawn on
     */
    public void draw(Graphics2D g2) {
        String turnString = (turn+"'s turn").toUpperCase();
        enemyHealth = "Enemy health: " + enemy.getHealth();
        inventory.draw(g2);
        g2.setFont(arial40);
        g2.setColor(Color.WHITE);

        //DRAWING NECESSARY INFORMATION
        g2.drawString("Player health: " + player.getHealth(), tileSize, 120);
        g2.drawString(enemyName, screenWidth-tileSize-g2.getFontMetrics(arial40).stringWidth(enemyName), 50);
        g2.drawString(enemyHealth, screenWidth-tileSize-g2.getFontMetrics(arial40).stringWidth(enemyHealth), 120);
        g2.drawString(turnString, (screenWidth-g2.getFontMetrics(arial40).stringWidth(turnString))/2, 50);
        g2.drawImage(player.getPlayerSprites().getCurrentSprite(), screenWidth/3,
                GamePanel.screenHeight-(7*tileSize)-player.getPlayerSprites().getCurrentSprite().getHeight(), null);
        g2.drawImage(enemy.getEnemySprites().getCurrentSprite(), screenWidth- screenWidth/3,
                GamePanel.screenHeight-(7*tileSize)-enemy.getEnemySprites().getCurrentSprite().getHeight(), null);


        if (messageOn) {
            //TEMPORARY MESSAGE RECEIVED
            g2.drawString(message, (screenWidth-g2.getFontMetrics(arial40).stringWidth(message))/2, 700);

            messageCounter++;
            if (messageCounter > 100) {
                messageOn = false;
                message = "";
                messageCounter = 0;
            }
        }
    }
}
