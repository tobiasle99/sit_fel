package cz.cvut.fel.pjv.main.display;

import cz.cvut.fel.pjv.entities.enemy.EnemyController;
import cz.cvut.fel.pjv.entities.player.Inventory;
import cz.cvut.fel.pjv.entities.player.Player;
import cz.cvut.fel.pjv.map.Background;
import cz.cvut.fel.pjv.map.Floor;
import cz.cvut.fel.pjv.objects.ObjectController;

import java.awt.*;

import static cz.cvut.fel.pjv.main.MyLogger.LOGGER;
import static cz.cvut.fel.pjv.main.display.GamePanel.*;

/**
 * The DrawController class is responsible for managing the graphical rendering of the game. It contains methods
 * to draw the game elements in order, including the background, floor, player, enemies, objects, and inventory.
 * It also contains methods to draw the combat UI and game over/death messages.
 */
public class DrawController {
    Background background = new Background();
    Floor floor = new Floor();
    Player player;
    ObjectController objectController;
    EnemyController enemyController;
    Inventory inventory;
    private Font Arial110 = new Font("Arial", Font.BOLD, 110);

    /**
     * Constructs a new DrawController object with the specified player, object controller, and enemy controller.
     * @param player the player object
     * @param objectController the object controller object
     * @param enemyController the enemy controller object
     */
    public DrawController(Player player, ObjectController objectController, EnemyController enemyController) {
        this.objectController = objectController;
        this.player = player;
        this.enemyController = enemyController;
        this.inventory = player.getInventory();
    }

    /**
     * Calls all other classes with a draw method and draws them in order.
     * @param g2 the graphics object
     */
    public void draw(Graphics2D g2) {
        background.draw(g2, player.getWorldPosition()[0]);
        floor.draw(g2, player.getWorldPosition()[0]);
        if (!player.isDead()) {
            player.draw(g2);
        }
        enemyController.draw(g2);
        objectController.draw(g2);
        inventory.draw(g2);
    }

    /**
     * Draws the combat UI.
     * @param g2 the graphics object
     * @param combatUI the combat UI objec
     */
    public void drawCombat(Graphics2D g2, CombatUI combatUI) {
        background.draw(g2, player.getWorldPosition()[0]);
        floor.drawCombat(g2, player.getWorldPosition()[0]);
        combatUI.draw(g2);
    }

    /**
     * Draws a game over message.
     * @param g2 the graphics object
     */
    public void drawGameOverMessage(Graphics2D g2){
        g2.setFont(Arial110);
        g2.setColor(Color.WHITE);
        g2.drawString("CONGRATULATIONS", 3*tileSize, screenHeight/2);
        g2.drawString("YOU WON!", 8*tileSize, screenHeight/2+2*tileSize);
        LOGGER.info("Drawing game over");
    }

    /**
     * Draws a death message.
     * @param g2 the graphics object
     */
    public void drawDeathMessage(Graphics2D g2){
        g2.setFont(Arial110);
        g2.setColor(Color.WHITE);
        g2.drawString("YOU LOST", 8*tileSize, screenHeight/2);
        g2.drawString("TRY AGAIN", 8*tileSize, screenHeight/2+2*tileSize);
        LOGGER.info("Drawing game lost");
    }
}
