package cz.cvut.fel.pjv.main;

import cz.cvut.fel.pjv.main.display.GamePanel;
import cz.cvut.fel.pjv.main.display.Window;

import javax.swing.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Main class serves as the entry point of the game application. It contains a main method that initializes the game
 * window and starts the game loop. This class also provides a command line argument to enable or disable logging.
 */
public class Main{
    private static final Logger LOGGER = MyLogger.LOGGER;

    /**
     * The main method initializes the game window and starts the game loop. It also provides a command line argument to
     * enable or disable logging.
     *
     * @param args command line arguments passed to the application
     *             --no-logging: optional argument to disable logging
     */
    public static void main(String[] args) {
        boolean enableLogger = true; // default value

        // check if the "--no-logging" argument is present
        for (String arg : args) {
            if (arg.equals("--no-logging")) {
                enableLogger = false;
                break;
            }
        }

        // configure logger based on the flag
        if (enableLogger) {
            LOGGER.info("Started game with logging enabled");
        } else {
            LOGGER.setUseParentHandlers(false); // disable console output
            LOGGER.setLevel(Level.OFF); // turn off logging
            LOGGER.info("Started game with logging disabled");
        }
        Window window = new Window();
        window.start();
    }
}