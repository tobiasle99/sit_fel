package cz.cvut.fel.pjv.main.save;

import cz.cvut.fel.pjv.entities.enemy.Enemy;
import cz.cvut.fel.pjv.entities.enemy.Goblin;
import cz.cvut.fel.pjv.entities.enemy.Skeleton;
import cz.cvut.fel.pjv.entities.player.Player;
import cz.cvut.fel.pjv.items.Item;
import cz.cvut.fel.pjv.items.Key;
import cz.cvut.fel.pjv.items.Sword;
import cz.cvut.fel.pjv.main.save.Load;
import cz.cvut.fel.pjv.objects.FinishLine;
import cz.cvut.fel.pjv.objects.OBJ_Chest;
import cz.cvut.fel.pjv.objects.SuperObject;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

public class LoadTest {

    static Load load;

    @BeforeAll
    public static void setup(){
        load = new Load();
    }

    @Test
    public void constructItem_constructsChestFromJson_returnsChest(){
        //ARRANGE
        String chestJson =
                        "{\n" +
                        "   \"contains\": \"potion\",\n" +
                        "   \"object_name\": \"chest\",\n" +
                        "   \"object_position\": [\n" +
                        "        576,\n" +
                        "        624\n" +
                        "    ]\n" +
                        "}";
        JSONObject chestObject = new JSONObject(chestJson);

        //ACT
        SuperObject loadedObject = load.constructObject(chestObject);
        OBJ_Chest chest = (OBJ_Chest) loadedObject;

        //ASSERT
        Assertions.assertEquals("chest", chest.getName());
        Assertions.assertEquals("potion", chest.getContains());
        Assertions.assertArrayEquals(new int[]{576, 624}, chest.getPosition());
    }

    @Test
    public void constructItem_constructsItemFromJson_returnsSword(){
        //ARRANGE
        String swordJson = "{\"item_name\": \"Sword\"}";
        JSONObject swordObject = new JSONObject(swordJson);

        //ACT
        Item loadedItem = load.constructItem(swordObject);
        Sword sword = (Sword) loadedItem;

        //ASSERT
        Assertions.assertEquals("Sword", sword.getName());
        Assertions.assertFalse(sword.isUseable());
    }

    @Test
    public void constructEnemy_constructsEnemyFromJson_returnsSkeleton(){
        //ARRANGE
        String skeletonJson =
                "{\n" +
                "    \"enemy_speed\": 10,\n" +
                "    \"drop\": {\"item_name\": \"Key\"},\n" +
                "    \"enemy_position\": [\n" +
                "        816,\n" +
                "        624\n" +
                "    ],\n" +
                "    \"enemy_name\": \"Skeleton\",\n" +
                "    \"enemy_health\": 30,\n" +
                "    \"enemy_attack\": 8,\n" +
                "    \"enemy_armor\": 1,\n" +
                "    \"enemy_chance_of_hitting\": 0.8\n" +
                "}";
        JSONObject skeletonObject = new JSONObject(skeletonJson);

        //ACT
        Enemy loadedEnemy = load.constructEnemy(skeletonObject);
        Skeleton skeleton = (Skeleton) loadedEnemy;
        Item drop = skeleton.getDrop();

        //ASSERT
        Assertions.assertEquals("Skeleton", skeleton.getName());
        Assertions.assertEquals(10, skeleton.getSpeed());
        Assertions.assertEquals("Key", drop.getName());
        Assertions.assertFalse(drop.isUseable());
        Assertions.assertArrayEquals(new int[]{816, 624}, skeleton.getPosition());
        Assertions.assertEquals(30, skeleton.getHealth());
        Assertions.assertEquals(8, skeleton.getAttack());
        Assertions.assertEquals(1, skeleton.getArmor());
        Assertions.assertEquals(0.8, skeleton.getChanceOfHitting());
    }

    @Test
    public void loadEnemies_constructsEnemiesFromJson_returnsEnemyArray(){
        //ARRANGE
        String enemiesJson =
                        "{\n" +
                        "     \"enemies\": [\n" +
                        "        {\n" +
                        "            \"enemy_speed\": 10,\n" +
                        "            \"drop\": {\"item_name\": \"Key\"},\n" +
                        "            \"enemy_position\": [\n" +
                        "                816,\n" +
                        "                624\n" +
                        "            ],\n" +
                        "            \"enemy_name\": \"Skeleton\",\n" +
                        "            \"enemy_health\": 30,\n" +
                        "            \"enemy_attack\": 8,\n" +
                        "            \"enemy_armor\": 1,\n" +
                        "            \"enemy_chance_of_hitting\": 0.8\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"enemy_speed\": 3,\n" +
                        "            \"drop\": {\"item_name\": \"Key\"},\n" +
                        "            \"enemy_position\": [\n" +
                        "                1440,\n" +
                        "                576\n" +
                        "            ],\n" +
                        "            \"enemy_name\": \"Goblin\",\n" +
                        "            \"enemy_health\": 60,\n" +
                        "            \"enemy_attack\": 15,\n" +
                        "            \"enemy_armor\": 5,\n" +
                        "            \"enemy_chance_of_hitting\": 0.7\n" +
                        "        }\n" +
                        "    ]\n"+
                        "}";
        JSONObject enemiesObject = new JSONObject(enemiesJson);

        //ACT
        Enemy[] loadedEnemies = load.loadEnemies(enemiesObject);
        Skeleton skeleton = (Skeleton) loadedEnemies[0];
        Item skeletonDrop = skeleton.getDrop();
        Goblin goblin = (Goblin) loadedEnemies[1];
        Item goblinDrop = goblin.getDrop();

        Enemy[] expectedArray = new Enemy[]{skeleton, goblin, null, null, null, null, null, null, null, null};

        //ASSERT
        Assertions.assertEquals("Skeleton", skeleton.getName());
        Assertions.assertEquals(10, skeleton.getSpeed());
        Assertions.assertEquals("Key", skeletonDrop.getName());
        Assertions.assertFalse(skeletonDrop.isUseable());
        Assertions.assertArrayEquals(new int[]{816, 624}, skeleton.getPosition());
        Assertions.assertEquals(30, skeleton.getHealth());
        Assertions.assertEquals(8, skeleton.getAttack());
        Assertions.assertEquals(1, skeleton.getArmor());
        Assertions.assertEquals(0.8, skeleton.getChanceOfHitting());

        Assertions.assertEquals("Goblin", goblin.getName());
        Assertions.assertEquals(3, goblin.getSpeed());
        Assertions.assertEquals("Key", goblinDrop.getName());
        Assertions.assertFalse(goblinDrop.isUseable());
        Assertions.assertArrayEquals(new int[]{1440, 576}, goblin.getPosition());
        Assertions.assertEquals(60, goblin.getHealth());
        Assertions.assertEquals(15, goblin.getAttack());
        Assertions.assertEquals(5, goblin.getArmor());
        Assertions.assertEquals(0.7, goblin.getChanceOfHitting());

        Assertions.assertArrayEquals(expectedArray, loadedEnemies);
    }

    @Test
    public void loadInventory_constructsItemsFromJson_returnsItemArray(){
        //ARRANGE
        String inventoryJson =
                "{\n"+
                "   \"inventory\": [\n" +
                "        {\"item_name\": \"Sword\"},\n" +
                "        {\"item_name\": \"Key\"}\n" +
                "    ]\n"+
                "}";
        JSONObject inventoryObject = new JSONObject(inventoryJson);

        //ACT
        Item[] loadedInventory = load.loadInventory(inventoryObject);
        Item[] expectedInventory = new Item[]{loadedInventory[0], loadedInventory[1], null, null, null};

        Sword sword = (Sword) loadedInventory[0];
        Key key = (Key) loadedInventory[1];

        //ASSERT
        Assertions.assertEquals("Sword", sword.getName());
        Assertions.assertFalse(sword.isUseable());

        Assertions.assertEquals("Key", key.getName());
        Assertions.assertFalse(key.isUseable());

        Assertions.assertArrayEquals(expectedInventory, loadedInventory);
    }

    @Test
    public void loadObjects_loadsObjectsFromJson_returnsSuperObjectArray(){
        //ARRANGE
        String objectsJson =
                "{\n" +
                        "    \"objects\": [\n" +
                        "        {\n" +
                        "            \"contains\": \"shield\",\n" +
                        "            \"object_name\": \"chest\",\n" +
                        "            \"object_position\": [\n" +
                        "                1200,\n" +
                        "                624\n" +
                        "            ]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"contains\": \"potion\",\n" +
                        "            \"object_name\": \"chest\",\n" +
                        "            \"object_position\": [\n" +
                        "                576,\n" +
                        "                624\n" +
                        "            ]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"object_name\": \"finish line\",\n" +
                        "            \"object_position\": [\n" +
                        "                2160,\n" +
                        "                624\n" +
                        "            ]\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}";
        JSONObject objectObject = new JSONObject(objectsJson);

        //ACT
        ArrayList<SuperObject> loadedObjects = load.loadObjects(objectObject);
        ArrayList<SuperObject> expectedObjects = new ArrayList<>();
        expectedObjects.add(loadedObjects.get(0));
        expectedObjects.add(loadedObjects.get(1));
        expectedObjects.add(loadedObjects.get(2));
        OBJ_Chest chest1 = (OBJ_Chest) loadedObjects.get(0);
        OBJ_Chest chest2 = (OBJ_Chest) loadedObjects.get(1);
        FinishLine finishLine = (FinishLine) loadedObjects.get(2);

        //ASSERT
        Assertions.assertEquals("chest", chest1.getName());
        Assertions.assertEquals("shield", chest1.getContains());
        Assertions.assertArrayEquals(new int[]{1200, 624}, chest1.getPosition());

        Assertions.assertEquals("chest", chest2.getName());
        Assertions.assertEquals("potion", chest2.getContains());
        Assertions.assertArrayEquals(new int[]{576, 624}, chest2.getPosition());

        Assertions.assertEquals("finish line", finishLine.getName());
        Assertions.assertNull(finishLine.getContains());
        Assertions.assertArrayEquals(new int[]{2160, 624}, finishLine.getPosition());

        Assertions.assertEquals(expectedObjects, loadedObjects);
    }

    @Test
    public void loadPlayer_setsPlayersStats_changedPlayerStats(){
        //ARRANGE
        String playerJson =
                "{\n" +
                "\"player\": {\n" +
                "        \"player_attack\": 15,\n" +
                "        \"player_armor\": 3,\n" +
                "        \"player_position\": [\n" +
                "            0,\n" +
                "            0\n" +
                "        ],\n" +
                "        \"player_health\": 100,\n" +
                "        \"player_speed\": 5\n" +
                "    }\n" +
                "}";
        JSONObject playerObject = new JSONObject(playerJson);
        Player mockedPlayer = mock(Player.class);

        //ACT
        load.loadPlayer(mockedPlayer, playerObject);

        //ASSERTIONS
        verify(mockedPlayer,times(1)).setArmor(anyInt());
        verify(mockedPlayer,times(1)).setAttack(anyInt());
        verify(mockedPlayer,times(1)).setWorldPosition(any());
        verify(mockedPlayer,times(1)).setHealth(anyInt());
        verify(mockedPlayer,times(1)).setSpeed(anyInt());
    }
}
