package cz.cvut.fel.pjv.main.save;

import cz.cvut.fel.pjv.entities.enemy.Enemy;
import cz.cvut.fel.pjv.entities.enemy.Goblin;
import cz.cvut.fel.pjv.entities.player.Inventory;
import cz.cvut.fel.pjv.entities.player.Player;
import cz.cvut.fel.pjv.entities.player.PlayerControls;
import cz.cvut.fel.pjv.items.*;
import cz.cvut.fel.pjv.objects.OBJ_Chest;
import cz.cvut.fel.pjv.objects.SuperObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SaveTest {

    @Test
    void saveItem() {
        Item item = new Shield();
        JSONObject expected = new JSONObject();
        expected.put("item_name", item.getName());
        JSONObject actual = new Save().saveItem(item);
        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    void saveInventory() {
        Inventory inventory = new Inventory();
        inventory.addItem(new Shield());
        inventory.addItem(new Shield());
        JSONArray expected = new JSONArray();
        expected.put(new Save().saveItem(new Sword()));
        expected.put(new Save().saveItem(new Key()));
        expected.put(new Save().saveItem(new Shield()));
        expected.put(new Save().saveItem(new Shield()));
        JSONArray actual = new Save().saveInventory(inventory);
        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    void savePlayerState() {
        Player player = new Player(new PlayerControls());
        player.setHealth(50);
        player.setArmor(10);
        player.setAttack(5);
        player.setSpeed(2);
        JSONObject expected = new JSONObject();
        expected.put("player_health", player.getHealth());
        expected.put("player_armor", player.getArmor());
        expected.put("player_attack", player.getAttack());
        expected.put("player_position", player.getWorldPosition());
        expected.put("player_speed", player.getSpeed());
        JSONObject actual = new Save().savePlayerState(player);
        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    void saveEnemy() {
        Enemy enemy = new Goblin(new Key());
        enemy.setHealth(30);
        enemy.setArmor(2);
        enemy.setAttack(5);
        enemy.setSpeed(1);
        enemy.setChanceOfHitting(0.5);
        enemy.setDrop(new Shield());
        JSONObject expected = new JSONObject();
        expected.put("enemy_name", enemy.getName());
        expected.put("enemy_speed", enemy.getSpeed());
        expected.put("enemy_health", enemy.getHealth());
        expected.put("enemy_armor", enemy.getArmor());
        expected.put("enemy_attack", enemy.getAttack());
        expected.put("enemy_position", enemy.getPosition());
        expected.put("enemy_chance_of_hitting", enemy.getChanceOfHitting());
        expected.put("drop", new Save().saveItem(enemy.getDrop()));
        JSONObject actual = new Save().saveEnemy(enemy);
        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void testSaveAllEnemies_withEmptyEnemiesArray() {
        Save save = new Save();
        Enemy[] enemies = new Enemy[0];
        JSONArray result = save.saveAllEnemies(enemies);
        assertEquals(new JSONArray().toString(), result.toString());
    }

    @Test
    public void testSaveAllEnemies_withOneEnemy() {
        Save save = new Save();
        Goblin goblin = new Goblin(new Sword());
        Enemy[] enemies = {goblin};
        JSONArray result = save.saveAllEnemies(enemies);
        JSONArray expected = new JSONArray();
        expected.put(save.saveEnemy(goblin));
        assertEquals(expected.toString(), result.toString());
    }

    @Test
    public void testSaveAllEnemies_withMultipleEnemies() {
        Save save = new Save();
        Goblin goblin = new Goblin(new Key());
        Goblin anotherGoblin = new Goblin(new Potion());
        Enemy[] enemies = {goblin, anotherGoblin};
        JSONArray result = save.saveAllEnemies(enemies);
        JSONArray expected = new JSONArray();
        expected.put(save.saveEnemy(goblin));
        expected.put(save.saveEnemy(anotherGoblin));
        assertEquals(expected.toString(), result.toString());
    }
}