/*
 * File name: Lab06.java
 * Date:      2014/08/26 21:39
 * Author:    @author
 */

package cz.cvut.fel.pjv;


import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Lab02 {

   public void start(String[] args) {
      homework();

      }


   private void homework() {
      TextIO textio = new TextIO();
      int lineNum = 0;
      int currentStackLength = 0;
      double[] array = new double[10];
      while (textio.hasNext()) {
         lineNum += 1;
         String currentValue = textio.getLine();
         if (textio.isInteger(currentValue) ||
         textio.isFloat(currentValue) || textio.isDouble(currentValue)) {
            array[currentStackLength] = Double.parseDouble(currentValue);
            currentStackLength += 1;
            if (currentStackLength == 10) {
               output(currentStackLength, array);
               currentStackLength = 0;
            }
         } else {
            System.err.println(String.format("A number has not been parsed from line %d", lineNum));
         }
      }
      System.err.println("End of input detected!");
      if (currentStackLength > 1) {
         for (int m = currentStackLength; m < 10; m++) { array[m] = 0; }
         output(currentStackLength, array);
      };
   }

   private void output(int stackLength, double[] ar) {
      double average;
      double standardDeviation;
      double sum = 0;
      double sumOfDeviations = 0;
      for (int i = 0; i < stackLength; i++) { sum += ar[i]; }
      average = sum / stackLength;
      for (int j = 0; j < stackLength; j++) {
         sumOfDeviations += pow((ar[j] - average), 2);
         //System.out.println(String.format("%.2f - %.2f = %.2f", ar[j], average, ar[j]-average));
         //System.out.println(String.format("%.2f^2 = %.2f", ar[j]-average, pow(ar[j]-average, 2)));
      }
      standardDeviation = sqrt(sumOfDeviations/stackLength);
      System.out.println(String.format("%2d %.3f %.3f", stackLength, average, standardDeviation));
   }
}
