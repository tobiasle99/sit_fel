package cz.cvut.fel.pjv;

public class NodeImpl implements Node {
    NodeImpl left = null;
    NodeImpl right = null;
    int value = 0;

    public NodeImpl() {
    }

    public NodeImpl(int value) {
        this.value = value;
    }

    public void setLeft(NodeImpl left) {
        this.left = left;
    }

    public void setRight(NodeImpl right) {
        this.right = right;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public NodeImpl getLeft() {
        return left;
    }

    @Override
    public NodeImpl getRight() {
        return right;
    }

    public int getValue() {return this.value;}


    static String toPrint = "";
    private void recursivePrint(int depth) {
        if (depth == 0) { toPrint += String.format("- %d\n", this.value);}
        else { toPrint += String.format("%" + depth + "s- %d\n", " ", this.value); }
        //System.out.println(String.format("adding value: %d", value));
        //System.out.println(toPrint);
        if (getLeft() != null && getLeft().getValue() != 0) {
            getLeft().recursivePrint(depth + 1);
        }
        if (getRight() != null && getRight().getValue() != 0 ) {
            getRight().recursivePrint(depth + 1);
        }
    }

    public String getToPrint() {
        if (value == 0) {return null;}
        else {
            recursivePrint(0);
            String copyToPrint = toPrint;
            toPrint = "";
            return copyToPrint;
        }
    }
}
