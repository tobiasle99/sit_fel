package cz.cvut.fel.pjv;

public class TreeImpl implements Tree{
    public TreeImpl() {
    }

    NodeImpl root = null;
    @Override
    public void setTree(int[] values) {
        if (values.length > 0) { root = new NodeImpl();
        recursion(values);
        }
    }

    public void printArray(int[] ar) {
        String result = "[";
        for (int character : ar) {
            result += String.format(" %s ", character);
        }
        result += " ] ";
        System.out.println(result);
    }
    private NodeImpl recursion(int[] values) {
        if (values.length == 0) {
            this.root = new NodeImpl(-1);
            return this.root;
        }
        else if (values.length == 1) {
            int currentValue = values[0];
            this.root.setValue(currentValue);
            //System.out.println("END VALUE " + currentValue);
            return this.root;
        } else {
            int middle;
            if ((values.length % 2) == 0) {middle = values.length/2;}
            else {middle = (values.length-1)/2;}
            this.root = new NodeImpl(values[middle]);
            int[] left = new int[middle];
            int[] right = new int[values.length-(middle+1)];
            for (int i = 0; i < middle; i++) {left[i] = values[i];}
            for (int j = middle+1; j < values.length; j++) {right[j-(middle+1)] = values[j];}
            //System.out.println(this.root.getValue());
            //printArray(left);
            //printArray(right);
            if (left.length > 0) {
                TreeImpl leftTree = new TreeImpl();
                leftTree.setTree(left);
                this.root.setLeft(leftTree.getRoot());
            }
            if (right.length > 0) {
                TreeImpl rightTree = new TreeImpl();
                rightTree.setTree(right);
                this.root.setRight(rightTree.getRoot());
            }
            //System.out.println("Creating left branch");

            //System.out.println("Creating right branch");



            return this.root;
        }
    }
    public NodeImpl getRoot() {
        if (root != null) {return this.root;}
        else {return null;}
    }

    public String toString() {
        if (this.root != null && this.root.getValue() != -1) {
            return root.getToPrint();
        } else {
            return "";
        }
    }




}
