package cz.cvut.fel.pjv;

import java.util.Formatter;
import java.util.Scanner;

public class Division {
    public static void division() {
        Scanner sc = Scan.sc;
        System.out.println("Zadej delenec: ");
        double operand1 = sc.nextDouble();
        System.out.println("Zadej delitel: ");
        double operand2 = sc.nextDouble();
        if (operand2 == 0) {
            System.out.println("Pokus o deleni nulou!");
            return;
        }
        System.out.println("Zadej pocet desetinnych mist: ");
        int decimals = sc.nextInt();
        if (decimals < 0) {
            System.out.println("Chyba - musi byt zadane kladne cislo!");
            return;
        }
        double result = operand1 / operand2;

        String dec="%."+decimals+"f";
        Formatter fmt1 = new Formatter();
        Formatter fmt2 = new Formatter();
        Formatter fmt3 = new Formatter();
        System.out.println(String.format("%s / %s = %s",
                fmt1.format(dec, operand1), fmt2.format(dec, operand2), fmt3.format(dec, result)));
    }

}
