package cz.cvut.fel.pjv;


import java.util.Scanner;

public class Lab01 {
   
   public void start(String[] args) {

      homework();
   }
   private void homework() {
      Scanner sc = Scan.sc;
      System.out.println("Vyber operaci (1-soucet, 2-rozdil, 3-soucin, 4-podil):");
      int operation = sc.nextInt();
      switch (operation) {
         case 1:
            Addition.addition();
            break;
         case 2:
            Subtraction.subtraction();
            break;
         case 3:
            Multiplication.multiplication();
            break;
         case 4:
            Division.division();
            break;
         default:
            System.out.println("Chybna volba!");
      }


   }







}