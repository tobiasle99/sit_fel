package cz.cvut.fel.pjv;

import java.util.Formatter;
import java.util.Scanner;

public class Addition {
    public static void addition() {
        Scanner sc = Scan.sc;
        System.out.println("Zadej scitanec: ");
        double operand1 = sc.nextDouble();
        System.out.println("Zadej scitanec: ");
        double operand2 = sc.nextDouble();
        System.out.println("Zadej pocet desetinnych mist: ");
        int decimals = sc.nextInt();
        if (decimals < 0) {
            System.out.println("Chyba - musi byt zadane kladne cislo!");
            return;
        }
        double result = operand1 + operand2;

        String dec="%."+decimals+"f";
        Formatter fmt1 = new Formatter();
        Formatter fmt2 = new Formatter();
        Formatter fmt3 = new Formatter();
        System.out.println(String.format("%s + %s = %s",
                fmt1.format(dec, operand1), fmt2.format(dec, operand2), fmt3.format(dec, result)));
    }
}
