package cz.cvut.fel.pjv;

public class BruteForceAttacker extends Thief {

    @Override
    public void breakPassword(int sizeOfPassword) {
        char[] array = new char[sizeOfPassword];
        recursion(array, 0);
    }

    private void printArray(char[] ar) {
        String result = "[";
        for (char character : ar) {
            result += String.format(" %s ", character);
        }
        result += " ] ";
        System.out.println(result);
    }

    private boolean recursion(char[] ar, int digit) {
        if (digit == ar.length) {
            return tryOpen(ar);
        } else {
            for (char character : getCharacters()) {
                ar[digit] = character;
                if (recursion(ar, digit + 1)) {
                    return true;
                }
            }
            return false;

        }


    }
}