package cz.cvut.fel.pjv;

/**
 * Implementation of the {@link Queue} backed by fixed size array.
 */
public class CircularArrayQueue implements Queue {
    private String[] cq;
    private int head; private int tail;
    /**
     * Creates the queue with capacity set to the value of 5.
     */
    public CircularArrayQueue() {
        cq = new String[5];
        head = 0;
        tail = 0;
    }


    /**
     * Creates the queue with given {@code capacity}. The capacity represents maximal number of elements that the
     * queue is able to store.
     * @param capacity of the queue
     */
    public CircularArrayQueue(int capacity) {
        cq = new String[capacity];
        head = 0;
        tail = 0;
    }

    @Override
    public int size() {
        int size = 0;
        for (int i = 0; i < cq.length; i++) {
            if (cq[i] != null) { size += 1; }
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0) { return true ;}
        else { return false; }
    }

    @Override
    public boolean isFull() {
        for (int i = 0; i < cq.length; i++) {
            if (cq[i] == null) { return false; }
        }
        return true;
    }

    @Override
    public boolean enqueue(String obj) {
        if (isFull()) { return false; }
        else {
            if (isEmpty()) {
                cq[head] = obj;
                tail = head;
            } else if (tail == cq.length-1) {
                tail = 0;
                cq[tail] = obj;
            } else {
                tail += 1;
                cq[tail] = obj;
            }
            return true;
        }
    }

    @Override
    public String dequeue() {
        if (isEmpty()) { return null; }
        else {
            String stringToReturn = cq[head];
            cq[head] = null;
            if (head == cq.length-1) { head = 0; }
            else { head += 1; }
            return stringToReturn;
        }

    }

    @Override
    public void printAllElements() {
        if (tail < head) {
            for (int i = head; i < cq.length; i++) {
                System.out.println(cq[i]);
            }
            for (int j = 0; j <= tail; j++) {
                System.out.println(cq[j]);
            }
        } else {
            for (int i = head; i <= tail; i++) {
                System.out.println(cq[i]);
            }
        }
        }
}
