<h1> Software Engineering and Technology </h1>
<h2> Faculty of Electrical Engineering - Czech Technical University </h2>

<h3> Semester 1 </h3>
<h4> ZAL - Základy Algoritmizace - Basics of Algorithmization </h4>

<p>Basic algorithms and data structures practiced in Python3. Went over loops, conditionals, working with files, linked lists, binary trees and 
objects. <a href="https://gitlab.com/tobiasle99/sit_fel/-/tree/main/ZAL"> More details inside </a> </p>

<h4> ZWA - Základy webových aplikací - Basics of Web Applications </h4>

<p> Learned the basics of HTML, CSS, JavaScript and PHP. Used all of these to create a <a href="https://wa.toad.cz/~letobias/semestralka_php/static/template/home.php"> website. </a> Documentation included. <a href="https://gitlab.com/tobiasle99/sit_fel/-/tree/main/ZWA/semestralka_php"> More details inside </a> </p>
<img src="https://gitlab.com/tobiasle99/sit_fel/-/raw/main/ZWA/homepage.jpg" alt="alt" style="width:50%">

<h4> PJV - Programování v Java - Programming in JAVA </h4>

<p> Currently has records of learning Java. <a href="https://gitlab.com/tobiasle99/sit_fel/-/tree/main/PJV/Semester%20work">Semester work (game engine).</a> </p>
<img src="https://gitlab.com/tobiasle99/sit_fel/-/raw/main/PJV/Semester%20work/Progress%20pics/30.4.2023/Screenshot%20from%202023-04-30%2022-28-20.png" alt="alt" style="width:50%>