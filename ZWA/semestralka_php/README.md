<h4> ZWA - Základy webových aplikací - Basics of Web Applications </h4>
<h4> Features <a href="https://wa.toad.cz/~letobias/semestralka_php/static/template/home.php"> website. </a></h4>
<ul>
    <li>Sign up, Log in - prevention against XSS, validation on the client side and server side</li>
    <li>Liking and unliking pictures - verification with CSRF tokens</li>
    <li>Filtering by liked pictures and sorting them from least recently liked</li>
    <li>Changing between 2 layouts of the homepage</li>
    <li>Forms are fully accessible</li>
    <li>Mobile layout</li>
</ul>

<h4> Files explained </h4>
<ul>
    <li>
        <h5>/app</h5>
        <p>Different files that control the logic of the website, be it on the client side, or the server side</p>
        <ul>
            <li>
                <h5>/app/js</h5>
                <p>Logic required on the client side</p>
                <ul>
                    <li>app.js - simple functions to control some events on the layout 1, layout 1 is fully functional without JS</li>
                    <li>app2.js - simple functions to control some events on the layout 2, layout 2 is not functional without JS</li>
                    <li>validacel.js - validation of inputs while logging in</li>
                    <li>validacer.js - validation of inputs while signing up</li>
                    <li>libs - gsap framework and libraries used for animations</li>
                </ul>
            </li>
            <li>
                <h5>/app/php</h5>
                <p>Logic required on the server side</p>
                <ul>
                    <li>account_info.php - fetch account info using id</li>
                    <li>add_user.php - add user to our database upon successful sign up</li>
                    <li>create_db.php - create a new database with sample data if it's empty</li>
                    <li>csrf_token_validation.php - when liking or unliking, check for CSRF</li>
                    <li>easter_egg.php - random cat pictures for people testing my website</li>
                    <li>filter.php - filter website by liked pictures only</li>
                    <li>fullsizemain.php - create the body for /static/template/fullsize.php</li>
                    <li>home_default.php - create the body for /static/template/home.php</li>
                    <li>home_filtered.php - create the body for /static/template/home.php with the applied filter</li>
                    <li>is_liked.php - function that checks whether a picture is liked</li>
                    <li>like.php - function that adds to our database, that you liked that picture</li>
                    <li>myaccmain.php - create the body for /static/template/my_account.php</li>
                    <li>prev_next_pic.php - create arrows to navigate between pictures in /static/template/fullsize.php</li>
                    <li>sorting.php - sorts pictures from the end in /static/template/my_account.php</li>
                    <li>unlike.php - function that deletes a given picture from your liked in our database</li>
                    <li>validacel.php - function that checks, whether the given email and password match a user</li>
                    <li>validacer.php - function that checks, whether the given data in the form are valid</li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <h5>/data</h5>
        <p>Data required for the website</p>
        <ul>
            <li>
                <h5>/data/database</h5>
                <p>Database of users saved in a json file</p>
            </li>
            <li>
                <h5>/data/fullsize</h5>
                <p>Gallery pictures in their full size</p>
            </li>
            <li>
                <h5>/data/img</h5>
                <p>Thumbnail pictures</p>
            </li>
        </ul>
    </li>
    <li>
        <h5>/static</h5>
        <p>Files that function as controllers, pulling from data and app</p>
        <ul>
            <li>
                <h5>/static/css</h5>
                <p>Styles</p>
                <ul>
                    <li>main.css - style rules for layout 2</li>
                    <li>print.css - style for print for both layouts</li>
                    <li>styles.css - style rules layour 1</li>
                </ul>
            </li>
            <li>
                <h5>/static/html</h5>
                <p>Static files that are mostly pure html</p>
                <ul>
                    <li>body2.php - body for layout2</li>
                    <li>footer.php - footer for layout 1</li>
                    <li>footer2.php - footer for layout 2</li>
                    <li>head.php - html head for layout 1</li>
                    <li>head2.php - html head for layout 2</li>
                    <li>header.php - header for layout 1</li>
                    <li>instructionsr.php - instructions for signup</li>
                    <li>loginform.php - form for signing in</li>
                    <li>nav.php - navbar for layout 1</li>
                    <li>nav_loggedin.php - navbar for users, who are signed in</li>
                    <li>signupform.php - form for signing up</li>
                </ul>
            </li>
             <li>
                <h5>/static/template</h5>
                <p>files that function as the controller in MVC</p>
                <ul>
                    <li>fullsize.php - view picture in fullsize</li>
                    <li>home.php - layout 1</li>
                    <li>home2.php - layout2</li>
                    <li>login.php - a page where you can log in</li>
                    <li>my_account.php - users, who are signed in, can view information about their account</li>
                    <li>sign_out.php - signs out users and redirects them home</li>
                    <li>signup.php - a page where you can sign up</li>
                </ul>
            </li>
        </ul>
    </li>
</ul>