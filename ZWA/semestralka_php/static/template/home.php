<?php
    if (!isset($_SESSION)){
        session_start();
        
    }
    $_SESSION['layout'] = 1;

?>

<!DOCTYPE html>
<html lang="en">
<?php require("../html/head.php"); ?>

    <body>

        





        <?php 
        require("../html/header.php");
        
        
        if (!empty($_SESSION) && array_key_exists('loggedin', $_SESSION)) {
            require("../html/nav_loggedin.php");
        } else {
            require("../html/nav.php");
        }
        require("../html/footer.php");
        
        
        if (array_key_exists('filter', $_SESSION) && $_SESSION['filter'] == true) {
            require("../../app/php/home_filtered.php");
        } else {
            require("../../app/php/home_default.php");
        }

        
        ?>
    </body>
</html>