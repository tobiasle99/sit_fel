<?php
    if (!isset($_SESSION)){
        session_start();
        
    }



    require("../../app/php/validacel.php");


    if (!empty($_SESSION) && array_key_exists("loggedin", $_SESSION)) {
        if ($_SESSION && array_key_exists('layout', $_SESSION) && $_SESSION['layout'] == 2) { 
            header("Location: https://wa.toad.cz/~letobias/semestralka_php/static/template/home2.php");
        } else {
        header("Location: https://wa.toad.cz/~letobias/semestralka_php/static/template/home.php");
    }} elseif (!empty($_SESSION) && array_key_exists('registered', $_SESSION)) {
        $succesful_message = "<div id='success_message'><h1> You have successfully signed up, please log in. </h1></div>";
    } 

?>
<!DOCTYPE html>
<html lang="en">
<?php require("../html/head.php") ?>
    <body id="log_in">
        <?php 
        require("../html/header.php");
        require("../html/nav.php");
        if (isset($succesful_message)) {
            //prichazi zprava z registrace
            echo $succesful_message;
        }  
        require("../html/loginform.php");  
        require("../html/footer.php");  
        ?>
    </body>
</html>