<?php
    if (!isset($_SESSION)){
        session_start();
        
    }


    require("../../app/php/validacer.php");
    if (!empty($_POST)) {
        validate($_POST);
    }
    if (!empty($_SESSION) && array_key_exists('registered', $_SESSION)) {
        header("Location: https://wa.toad.cz/~letobias/semestralka_php/static/template/login.php");
    }
?>

<!DOCTYPE html>
<html lang="en">
<?php require("../html/head.php") ?>

    <body id="sign_up">
        <?php 
        require("../html/header.php");
        require("../html/nav.php");
        require("../html/instructionsr.php");  
        require("../html/signupform.php");
        require("../html/footer.php");    
        ?>
    </body>
</html>