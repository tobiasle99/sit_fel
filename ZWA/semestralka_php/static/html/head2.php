<head>

	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Architecture Gallery</title>

	<link rel="stylesheet" href="../css/main.css">
	<link href="../css/print.css" rel="stylesheet" media="print">
	<script src="../../app/js/libs/gsap.min.js" defer></script>
	<script src="../../app/js/libs/Draggable.min.js" defer></script>
	<script src="../../app/js/libs/InertiaPlugin.min.js" defer></script>
	<script src="../../app/js/app2.js" defer></script>
	
	
	
</head>