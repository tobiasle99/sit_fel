<nav id="nav">

    <?php 
        if (!isset($_SESSION)){
        session_start();
        
        }
        if ($_SESSION && array_key_exists('layout', $_SESSION) && $_SESSION['layout'] == 2) {
            echo '<a class="button" href="./home2.php">Home</a> ';
        } else {
            echo '<a class="button" href="./home.php">Home</a>';
        }
    ?>
    
    <a class="button" id="loginbutton" href="./login.php">Log in</a>
    <a class="button" id="signupbutton" href="./signup.php">Sign up</a>
</nav>