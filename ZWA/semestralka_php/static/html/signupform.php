<?php 
require_once("../../app/php/validacer.php");











$email = "";
$firstName = "";
$lastName = "";
$sex="";
if (count($_POST) > 0) {
    $email = trim($_POST['email']);
    $firstName = trim($_POST['firstName']);
    $lastName = trim($_POST['lastName']);
    if (isset($_POST['sex'])) {
        $sex = trim($_POST['sex']);
    } else {
        $sex = null;
    }
    
    validate($_POST);
}


?>




<script src="../../app/js/validacer.js" defer></script>
<!-- <form id="myForm2" action="https://wa.toad.cz/~xklima/vypisform.php" method="post"> -->
<form method="post" id="myForm2">
    <fieldset>
        <legend>Account Information</legend>
        <div class="formline">
            <label for="email">*E-mail: </label>
            <input type="email" name="email" id="email" autocomplete="off" required <?php 
             if(count($_POST)) {
                echo"value=".htmlspecialchars($email);
                } 
            ?>>
        </div>
        <?php 
        if (isset($emailErr)) {
            
            echo $emailErr;
        }
        ?>
        <div class="formline">
            <label for="password">*Password: </label>
            <input type="password" name="password" id="password" pattern =".{5,}" required>
             
        </div>
        <?php 
        if (isset($passwordErr)) {
            echo $passwordErr;
        }
        ?>
        <div class="formline">
            <label for="passwordConfirm">*Confirm Password: </label>
            <input type="password" name="passwordConfirm" id="passwordConfirm" pattern =".{5,}" required> 
        </div>
        <?php 
        if (isset($confirmErr)) {
            echo $confirmErr;
        }
        ?>
    </fieldset>
    <fieldset>
        <legend>User Information</legend>
        <div class="formline">
            <label for="firstName">*First Name: </label>
            <input type="text" name="firstName" id="firstName" autocomplete="off" pattern ="[a-zA-Z]{2,}" required 
            <?php 
             if(count($_POST)) {
                echo"value=".htmlspecialchars($firstName);
                } 
            ?>>
        </div>
        <?php 
        if (isset($firstNameErr)) {
            echo $firstNameErr;
        }
        ?>
        <div class="formline">
            <label for="lastName">*Last Name: </label>
            <input type="text" name="lastName" id="lastName" autocomplete="off" pattern ="[a-zA-Z]{2,}" required 
            <?php 
             if(count($_POST)) {
                echo"value=".htmlspecialchars($lastName);
                } 
            ?>>
        </div>
        <?php 
        if (isset($lastNameErr)) {
            echo $lastNameErr;
        }
        ?>
        
        <div class=radioouter>
            
            <div class="radio" id="sexradio">
                <div id="radiolabel"> *Gender: </div>    
                <input type="radio" name="sex" id="m" class="radioi" value='m'
                required <?php if ($sex=="m") {echo htmlspecialchars('checked="checked"');}?> >
                <label for="m" id='mlabel'>Male</label>
                <input type="radio" name="sex" id="f" class="radioi" value='f' 
                required <?php if ($sex=="f") {echo htmlspecialchars('checked="checked"');}?> >
                <label for="f" id='flabel'>Female</label>
                <input type="radio" name="sex" id="o" class="radioi" value='o' 
                required <?php if ($sex=="o") {echo htmlspecialchars('checked="checked"');}?> >
                <label for="o" id='olabel'>Other</label>
            </div>
        </div>
        <?php 
        if (isset($sexErr)) {
            echo $sexErr;
        }
        ?>
        <div class="formline">
            <label for="agree" id="agreelabel">*I agree to be silly: </label>
            <input type="checkbox" name="agree" id="agree" required <?php if (array_key_exists('agree', $_POST)) {echo htmlspecialchars('checked="checked"');}?>>
        </div>
        <?php 
        if (isset($agreeErr)) {
            echo $agreeErr;
        }
        ?>
        <div class="formline">
            <div class="clown">
                🤡 Please fill out all required fields
            </div>
        </div>

    </fieldset>
    
    <span> * These fields are required </span>
    <input type="submit" value="Sign Up">
</form>