<nav id="nav">
    <?php 
        //navbar, pokud jsme prihlaseni

        if (!isset($_SESSION)){
        session_start();
        
        }
        if ($_SESSION && array_key_exists('layout', $_SESSION) && $_SESSION['layout'] == 2) {
            echo '<a class="button" href="./home2.php">Home</a> ';
        } else {
            echo '<a class="button" href="./home.php">Home</a>';
        }
    ?>
    <a class="button" id="loginbutton" href="../template/my_account.php">My Account</a>
    <?php 
        if (array_key_exists('liked', $_SESSION) && count($_SESSION['liked'])) {
            if (isset($_SESSION['filter'])) {
                echo '<a class="filtered" href="../../app/php/filter.php">Filter</a>';
            } else {
                echo '<a class="button" id="loginbutton" href="../../app/php/filter.php">Filter</a>';
            }
            
        }
    ?>
    <a class="button" id="signupbutton" href="./sign_out.php">Sign Out</a>
</nav>