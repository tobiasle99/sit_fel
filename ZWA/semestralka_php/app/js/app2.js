window.onload = function() {

	setTimeout(function() {
		/* po 2 sekundach nacteceme body tridu "loaded" a tim se galerie zobrazi 
		take zde implementujeme knihovny draggable a inertia na body */
		document.body.classList.add('loaded')

		if (window.matchMedia('(min-width: 1079px)').matches) { // If not mobile

			Draggable.create('.gallery', {
				bounds: 'body',
				inertia: true
			})
			
		}
	}, 200)
	
}

document.querySelectorAll('img').forEach(item => {
	item.addEventListener('click', event => {
		var picture = (event.target.getAttribute("class"));
		console.log(picture);
		/*  
		Kod pro kontrolu, jestli jsme ve standard nebo quirk mode
		window.alert('You are in ' + (document.compatMode==='CSS1Compat'?'Standards':'Quirks') + ' mode.') 
		*/
		
		location.href = 'https://wa.toad.cz/~letobias/semestralka_php/static/template/fullsize.php?picture='+picture;
	  
	})
  })
