function main() {
    
    const formElement = document.querySelector("form");
    formElement.addEventListener("submit", validate);
}

function validate(e) {
    /* input je event
    output je manipulace s DOM a preventovani poslani 
    HTTP dotazu pri chybnem poslani dat */
    var email = document.querySelector("#email").value;
    var pw1 = document.querySelector("#password").value;

    var mailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    // jestli email neni ve validni forme, ani neposilame
    // formular na server
    if(email.match(mailformat)) { 
        console.log("VALID MAIL");
    } else {
        console.log("INVALID MAIL");
        document.querySelector("#email").classList.add("WRONG");
        e.preventDefault();
    }


    // jestli heslo nema aspon 5 znaku, ani neposilame
    // formular na server
    if ((pw1.length < 5)) {
        console.log("PWBAD");
        document.querySelector("#password").classList.add("WRONG")
        document.querySelector("#passwordConfirm").classList.add("WRONG")
        e.preventDefault();
        document.querySelector(".clown").style.display = "flex";
    }

}

main()