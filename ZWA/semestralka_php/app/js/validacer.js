function main() {
    console.log("WE IN LADS")
    const formElement = document.querySelector("#myForm2");
    formElement.addEventListener("keyup", validate);
    formElement.addEventListener("click", validate2);
    formElement.addEventListener("submit", validate);
    document.querySelector(".clown").style.display = "none";
}



function validate(e) {
    /* input je event
    output je manipulace s DOM a preventovani poslani 
    HTTP dotazu pri chybnem poslani dat */
    var fn = document.querySelector("#firstName").value;
    var ln = document.querySelector("#lastName").value;
    var email = document.querySelector("#email").value;
    var pw1 = document.querySelector("#password").value;
    var pw2 = document.querySelector("#passwordConfirm").value;
    var letters = /^[A-Za-z]+$/;


    //kontrola emailu pomoci match
    var mailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(email.match(mailformat)) { 
        console.log("VALID MAIL");
        document.querySelector("#email").className = '';
    } else {
        console.log("INVALID MAIL");
        document.querySelector("#email").classList.add("WRONG");
        e.preventDefault();
        document.querySelector(".clown").style.display = "flex";
    }

    //jmena musi mit aspon 2 znaky a byt pismena
    if (fn.length < 2 || !fn.match(letters)) {
        console.log("FNBAD");
        document.querySelector("#firstName").classList.add("WRONG")
        e.preventDefault();
        document.querySelector(".clown").style.display = "flex";
    } else {
        document.querySelector("#firstName").className = '';
    }

    if (ln.length < 2 || !ln.match(letters)) {
        console.log("LNBAD");
        document.querySelector("#lastName").classList.add("WRONG")
        e.preventDefault;
        document.querySelector(".clown").style.display = "flex";
    } else {
        document.querySelector("#lastName").className = '';
    }

    //heslo musi mit aspon 5 znaku a kontrola se musi rovnat
    if (pw1.length < 5) {
        console.log("PWBAD");
        document.querySelector("#password").classList.add("WRONG")
        e.preventDefault();
        document.querySelector(".clown").style.display = "flex";
    } else if (pw1 != pw2) {
        document.querySelector("#passwordConfirm").classList.add("WRONG")
        e.preventDefault();
    } else {
        document.querySelector("#password").className = '';
        document.querySelector("#passwordConfirm").className = '';
    }

    //je povinne vybrat aspon jedno z radio buttonu
    if (document.querySelector("#m").checked ||
        document.querySelector("#f").checked ||
        document.querySelector("#o").checked) {
        console.log("GUT");
        
    } else {
        document.querySelector("#mlabel").classList.add("WRONG");
        document.querySelector("#flabel").classList.add("WRONG");
        document.querySelector("#olabel").classList.add("WRONG");
        document.querySelector(".clown").style.display = "flex";
        e.preventDefault();
    }

    //je treba zaskrtnout checkbox
    if (document.querySelector("#agree").checked) {
        console.log("GUT");
        document.querySelector("#agreelabel").className = '';
    } else {
        document.querySelector("#agreelabel").classList.add("WRONG")
        console.log("UNCHECKED");
        document.querySelector(".clown").style.display = "flex";
        e.preventDefault();
    }
    


}

function validate2(e) {
    /* input je event
    output je manipulace s DOM a preventovani poslani 
    HTTP dotazu pri chybnem poslani dat */

    // preventdefault zakazovalo zaskrtavani policek

    //je povinne vybrat aspon jedno z radio buttonu
    if (document.querySelector("#m").checked ||
        document.querySelector("#f").checked ||
        document.querySelector("#o").checked) {
        console.log("GUT");
        document.querySelector("#mlabel").classList.remove("WRONG");
        document.querySelector("#flabel").classList.remove("WRONG");
        document.querySelector("#olabel").classList.remove("WRONG");
    } else {
        document.querySelector("#mlabel").classList.add("WRONG");
        document.querySelector("#flabel").classList.add("WRONG");
        document.querySelector("#olabel").classList.add("WRONG");
        document.querySelector(".clown").style.display = "flex";
    }

    //je treba zaskrtnout checkbox
    if (document.querySelector("#agree").checked) {
        console.log("GUT");
        document.querySelector("#agreelabel").className = '';
    } else {
        document.querySelector("#agreelabel").classList.add("WRONG")
        console.log("UNCHECKED");
        document.querySelector(".clown").style.display = "flex";
    }
    


}

main()