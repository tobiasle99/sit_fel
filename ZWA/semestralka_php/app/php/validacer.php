<?php
    //validacer == validace registrace
    require("../../app/php/add_user.php");

    
    $firstNameErr = $emailErr = $lastNameErr = "";
    $firstName = $email = $lastName = $sex = $password = "";
    
    function validate($post) {
        
        $email_passed = email_validation($post);
        $first_name_passed = first_name_validation($post);
        $last_name_passed = last_name_validation($post);
        $password_passed = password_validation($post);
        if (isset($post["agree"])) {
            //checkbox je zaskrtnuty
            $agree_passed = true;
        } else {
            //checkbox se neposlal
            $agree_passed = false;
            global $agreeErr;
            $agreeErr = "You must agree to be silly";
        };

        if (isset($post["sex"])) {
            //poslal se radio button
            $sex_passed = true;
        } else {
            //neposlal se radio button
            $sex_passed = false;
            global $sexErr;
            $sexErr = "You must select a gender";
        };

        if ($email_passed && first_name_validation($post) && 
        last_name_validation($post) && password_validation($post)
        && $agree_passed && $sex_passed) {
            //jestli vsechno proslo
            add_user($post);
            if (!isset($_SESSION)){
                session_start();
                
            }
            $_SESSION['registered'] = true;
        }
    }

    function email_validation($post) {
        if (empty($post["email"])) {
            //prazdny emial
            global $emailErr;
            $emailErr = "Email is required";
            return false;
        } else if (!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
            //neni platna forma emailu
            global $emailErr;
            $emailErr = "Invalid email format";
            return false;
        } else if (email_in_use($post)) {
            //email je uz pouzivany
            global $emailErr;
            $emailErr = "Email already in use";
            
            return false;
        } else {
            return true;
        }
    }

    function first_name_validation($post) {
        if (empty($post["firstName"])) {
            //krestni jmeno je prazdne
            global $firstNameErr;
            $firstNameErr = "First name is required";
            return false;
        } else if(!preg_match("^[a-zA-Z]^",$post['firstName'])) {
            //krestni jmeno ma znaky, ktere nechci
            global $firstNameErr;
            $firstNameErr = "Only letters of English alphabet are allowed";
            return false;

        } else if(strlen($post["firstName"]) < 2) {
            //krestni jmeno je moc kratke
            global $firstNameErr;
            $firstNameErr = "Must be 2 or more characters long";
            return false; 
        } else {
            //krestni jmeno je v poradku
            return true;
        }
    }

    function last_name_validation($post) {
        if (empty($post["lastName"])) {
            //prijmeni je prazdne
            global $lastNameErr;
            $lastNameErr = "Last name is required";
            return false;
        } else if(!preg_match("^[a-zA-Z]^",$post['lastName'])) {
            //prijmeni ma znaky, co nechci
            global $lastNameErr;
            $lastNameErr = "Only letters of English alphabet are allowed";
            return false;
        } else if(strlen($post["lastName"]) < 2) {
            //prijmeni je moc kratke
            global $lastNameErr;
            $lastNameErr = "Must be 2 or more characters long";
            return false; 
        } else {
            return true;
        }
    }

    function password_validation($post) {
        if ((strlen($post['password']) < 5)) {
            //heslo je moc kratke
            echo(strlen($post['password']));
            global $passwordErr;
            $passwordErr = "Password must be at least 5 characters long";
            return false;
        } else if ($post['password'] != $post['passwordConfirm']) {
            //hesla nejsou stejna
            global $confirmErr;
            $confirmErr = "Passwords do not match";
            return false;
        } else {
            return true;
        }
    }

    function email_in_use($post) {
        
        $data = file_get_contents("../../data/database/_users.json");
        
        $decoded_data = json_decode($data, JSON_OBJECT_AS_ARRAY);
        $users = $decoded_data["users"];
        foreach ($users as $user) {
            //email uz je pouzivany
            if ($user["email"] == $post["email"]) {
                return true;
            }
        }
        return false;
    }
    
?>