<?php



    require("../../app/php/is_liked.php");
	//pocet obrazku je pocet obrazku, ktere se nam libi
    $amount_of_pictures = count($_SESSION['liked']);
	$highest_index_liked = array_key_last($_SESSION['liked']);
    $limit = 4;
	$total = $amount_of_pictures;
	if ($total > 0) {
		$pages = ceil($total/$limit);
		$page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
			'options' => array(
				'default'   => 1,
				'min_range' => 1,
			),
		)));
		$offset = ($page - 1) * $limit;
		$current_picture = $offset;
		
		echo '<main id="filteredmain"> <div class="gallery">';
		for ($column = 0; $column <= 3; $column++){
			echo '<div class="column">';
			
			for ($i = 1; $i <= $limit/4; $i++) {
				if ($current_picture > $highest_index_liked) {
					// jestli nas current picture uz je za polem liked
					break;
				} elseif (!array_key_exists((int) $current_picture, $_SESSION["liked"])) {
					// jestli tento obrazek neni v liked
					continue;
				
				} elseif (is_liked($_SESSION['liked'][$current_picture])) {
					//jestli obrazek je v liked
					echo '<a class="thumb_pic" href="https://wa.toad.cz/~letobias/semestralka_php/static/template/fullsize.php?picture='.$_SESSION['liked'][$current_picture].'"><div class="gallery__item"><img class="thumb_pic" src="../../data/img/'.$_SESSION['liked'][$current_picture].'_thumb.jpg" id="'.$_SESSION['liked'][$current_picture].'" alt="Alt"></div></a>';
					$current_picture++;
				} else {
						$current_picture++;
				}
			}
			echo '</div>';
		}
		echo '</div></main>';
		


		//tvorba strankovani

						
		echo '<div id="pages">';
		if ($page > 1) {
			//nejsme na prvni strance
			$prevlink = '<a href="?page=1">&laquo;</a> <a href="?page='.($page-1).'">&lsaquo;</a>';
		} else {
			//jsme na prvni strance
			$prevlink = "<div class='unclickable'> &laquo; </div> <div class='unclickable'> &lsaquo; </div>";
		}

		$links = "";
		for ($i=0; $i < $pages; $i++) {
			if ($i == $page-1 ) {
				// tvorime odkaz pro stranku, na ktere jsme
				$links .= " <div class='unclickable'>".($i+1)."</div> ";
			} else {
				// tvorime odkaz pro jinou stranku
				$links .= " <a href=\"?page=".($i+1)."\">".($i+1)."</a> ";
			}
		}


		if ($page < $pages) {
			// nejsme na posledni strance
			$nextlink = '<a href="?page='.($page+1).'">&rsaquo;</a> <a href="?page='.$pages.'">&raquo;</a>';
		} else {
			// jsme na posledni strance
			$nextlink = "<div class='unclickable'> &rsaquo;</div> <div class='unclickable'> &raquo; </div>";
		}


		echo $prevlink, $links, $nextlink; 
		echo '</div>';
		}

    ?>