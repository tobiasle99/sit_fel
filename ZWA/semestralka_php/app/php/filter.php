<?php
    if (!isset($_SESSION)){
        session_start();
        
    }

    //prirazeni filteru do session
    if (array_key_exists('filter', $_SESSION)) {
        unset($_SESSION['filter']);
    } else {
        $_SESSION['filter'] = true;
    }
    header("Location: https://wa.toad.cz/~letobias/semestralka_php/static/template/home.php")
?>