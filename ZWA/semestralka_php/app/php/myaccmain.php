<main id="myaccmain">
            <section class="myaccsection">
                <table id='pctable'>
                    <tr><th>E-Mail</th><th>First Name</th> <th>Last Name</th><th>Liked pictures</th> </tr>
                    <tr><td><?=htmlspecialchars($user_data['email'])?></td><td><?=htmlspecialchars($user_data['first_name'])?>
                </td><td><?=htmlspecialchars($user_data['last_name'])?></td><td><?=count($_SESSION['liked'])?></td></tr>
                </table>

                <table id='mobiletable'>
                    <tr><th>E-Mail</th><td><?=htmlspecialchars($user_data['email'])?></td></tr>
                    <tr><th>First Name</th><td><?=htmlspecialchars($user_data['first_name'])?></td></tr>
                    <tr><th>Last Name</th><td><?=htmlspecialchars($user_data['last_name'])?></td></tr>
                    <tr><th>Liked pictures</th><td><?=count($_SESSION['liked'])?></td></tr>
                </table>

                <h1 id='likedpicsh1'>Liked pictures</h1>
                <?php
                
                if (!empty($_SESSION) && array_key_exists('sorted', $_SESSION) && $_SESSION['sorted'] == true) {
                    echo '<a class="filtered" href="../../app/php/sorting.php"> From least recently liked </a>';
                } else {
                    echo '<a class="button" href="../../app/php/sorting.php"> From least recently liked </a>';
                }
                
                ?>
                
                </section>
        </main>