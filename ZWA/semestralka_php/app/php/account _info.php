<?php
if (!isset($_SESSION)){
    session_start();
    
}
function user_info() {
        //chceme ziskat informace o uzivateli pomoci id, ktere mame ulozene v session
        $data = file_get_contents("../../data/database/_users.json");
        $decoded_data = json_decode($data, JSON_OBJECT_AS_ARRAY);
        $users = $decoded_data["users"];
        
        if (isset($_SESSION) && array_key_exists('loggedin', $_SESSION)) {
            foreach ($users as $user) {
                
                if ($user["id"] == $_SESSION["id"]) {
                    // jestli id je spravne, tak vracime uzivatele jako array
                    return $user;
                }
            }
        } else {
            // jestli nenalezneme uzivatele s danym id, vratime false
            return false;
        }
    }

    ?>