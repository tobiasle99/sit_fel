<?php
    require("./account _info.php");
    require("./csrf_token_validation.php");
    if (user_info() == false) {
        //jestli user neexistuje nebo neni prihlaseny
        header('Location: https://wa.toad.cz/~letobias/semestralka_php/static/template/login.php');
    } elseif (CSRF()) {
        

        $picture = filter_input(INPUT_GET, 'picture', FILTER_VALIDATE_INT);
        define ('DBFILE', "../../data/database/_users.json");
        $data = file_get_contents(DBFILE);
        $decoded_data = json_decode($data, JSON_OBJECT_AS_ARRAY);
        $users = $decoded_data["users"];


        $replace = array();
        foreach ($users as $user) {

            //hledame uzivatele v databazi
            if ($user["id"] == $_SESSION['id']) {
                //nasli jsme spravneho uzivatele

                if (isset($user['liked'])){
                    //jestli v databazi ma napsano, ze se mu neco libilo
                    $_SESSION['liked'] = $user['liked'];
                } else {
                    // jestli nikdy nepouzival funkci like
                    $_SESSION['liked'] = array();
                }

                //pridavame do liked novy obrazek
                array_push($_SESSION['liked'], $picture);
                $user['liked'] = $_SESSION['liked'];

                //vypiname sortovani
                if (array_key_exists('sorted', $_SESSION)) {
                    $_SESSION['sorted'] = false;
                }
            }
            //do replace pridavame vsechny uzivatele, vcetne toho,
            // ktereho jsme prave upravili
            array_push($replace, $user);

            //odebirame csrf token
            unset($_COOKIE['token']);
        }

        
        $new_data = array("users" => ($replace));
        $encoded_data = json_encode($new_data);
        file_put_contents("../../data/database/_users.json", $encoded_data);
        header('Location: https://wa.toad.cz/~letobias/semestralka_php/static/template/fullsize.php?picture='.$picture);
    }
?>