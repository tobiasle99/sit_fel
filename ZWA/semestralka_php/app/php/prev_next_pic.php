<?php
    //tato funkce pridava sipky do fullsize.php
    //abychom mohli prechazet mezi obrazky a
    //nemusime stale klikat home










    $picture = filter_input(INPUT_GET, 'picture', FILTER_VALIDATE_INT, array(
            'options' => array(
                'default'   => 1,
                'min_range' => 1,
            ),));
    if (!isset($_SESSION['filter'])) {
    $amount_of_pictures = iterator_count(new FilesystemIterator('../../data/img/', FilesystemIterator::SKIP_DOTS));
        if ($picture > 1) {
            $prevlink = '<a id="prevlink" href="?picture='.($picture-1).'">&lsaquo;</a>';
        } else {
            $prevlink = "<div id='prevlink' class='unclickablebig'> &lsaquo; </div>";
        }

        if ($picture < $amount_of_pictures) {
            $nextlink = '<a id="nextlink" href="?picture='.($picture+1).'">&rsaquo;</a>';
        } else {
            $nextlink = "<div id='nextlink'class='unclickablebig'> &rsaquo; </div>";
        }
    } else {
        $amount_of_pictures = count($_SESSION['liked']);
        $picture = array_search($picture,$_SESSION['liked']);
        if ($picture > 0) {
            $prevlink = '<a id="prevlink" href="?picture='.($_SESSION['liked'][$picture-1]).'">&lsaquo;</a>';
        } else {
            $prevlink = "<div id='prevlink' class='unclickablebig'> &lsaquo; </div>";
        }

        if ($picture < $amount_of_pictures-1) {
            $nextlink = '<a id="nextlink" href="?picture='.($_SESSION['liked'][$picture+1]).'">&rsaquo;</a>';
        } else {
            $nextlink = "<div id='nextlink'class='unclickablebig'> &rsaquo; </div>";
        }
    }
    echo $prevlink;
    echo $nextlink;
?>