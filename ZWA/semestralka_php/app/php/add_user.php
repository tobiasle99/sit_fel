<?php

define ('DBFILE', "../../data/database/_users.json");

function add_user($post) {
    //pridat uzivatele z postu


    $user =
        array(
            'id' => uniqid(),
            'email'=>$post['email'],
            'password'=>password_hash($post['password'], PASSWORD_DEFAULT),
            'first_name'=>$post['firstName'],
            'last_name'=>$post['lastName'],
            'gender'=>$post['sex'],
            'agreed'=>$post['agree']
        );

    $data = file_get_contents(DBFILE);
    
    if ($data == "[null]") {
        //jestli data jsou prazdna, tak zavolat tvorbu db
        
        require '../../app/php/create_db.php';
        $data = file_get_contents(DBFILE);
        
    }
    
    $decoded_data = json_decode($data, JSON_OBJECT_AS_ARRAY);
    $users = $decoded_data["users"];
    array_push($users, $user);


    $new_data = array("users" => ($users));

    $encoded_data = json_encode($new_data);
    file_put_contents(DBFILE, $encoded_data);
    

    
}   
?>