<main>
    <?php
    $picture = filter_input(INPUT_GET, 'picture', FILTER_VALIDATE_INT, array(
'options' => array(
    'default'   => 1,
    'min_range' => 1,
),));?>
    <img class="fullsize_img" src="../../data/fullsize/<?=$picture?>.jpg" alt="alt">
    <div id="buttonbar">
    <?php
        if (!empty($_SESSION) && array_key_exists('liked', $_SESSION) && is_liked($picture) ) {
            echo ('<a class="liked" href="../../app/php/unlike.php?picture='.$picture."&token=".$_COOKIE['token'].'">Like</a>');
        }else{
            echo ('<a class="button" href="../../app/php/like.php?picture='.$picture."&token=".$_COOKIE['token'].'">Like</a>');
        }
    ?>

        <a class="button" href="../../data/fullsize/<?=$picture?>.jpg" download>Save</a>
    </div>
</main>