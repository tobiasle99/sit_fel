<?php

	// pocet obazku je pocet souboru ve slozce img
    $amount_of_pictures = iterator_count(new FilesystemIterator('../../data/img/', FilesystemIterator::SKIP_DOTS));
    // limit je pocet obrazku, ktere chceme dat na jednu stranku
	$limit = 8;
	$total = $amount_of_pictures;
	if ($total > 0) {
		// pocet stranek je zaokrouhleni nahoru total/limit
		$pages = ceil($total/$limit);

		// page je minimum z pages nebo z page v GET
		$page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
			'options' => array(
				'default'   => 1,
				'min_range' => 1,
			),
		)));

		//offset je prvni obrazek, ktery se nam zobrazi na strance
		$offset = ($page - 1) * $limit+1;
		$current_picture = $offset;
		
		//vytvorit 4 sloupce, ktere budou naplneny limit/4 obrazky kazdy
		echo '<main id="defaultmain"> <div class="gallery">';
		for ($column = 0; $column <= 3; $column++){
			echo '<div class="column">';

			for ($i = 1; $i <= $limit/4; $i++) {
				if ($current_picture > $amount_of_pictures) {
					break;
				} else {
				echo '<a class="thumb_pic" href="https://wa.toad.cz/~letobias/semestralka_php/static/template/fullsize.php?picture='.$current_picture.'"><div class="gallery__item"><img src="../../data/img/'.$current_picture.'_thumb.jpg" id="'.$current_picture.'" alt="Alt"></div></a>';
				$current_picture++;
				}
			}
			echo '</div>';
		}
		echo '</div></main>';
		


		//tvorba strankovani

		
		echo '<div id="pages">';
		if ($page > 1) {
			//nejsme na prvni strance
			$prevlink = '<a href="?page=1">&laquo;</a> <a href="?page='.($page-1).'">&lsaquo;</a>';
		} else {
			//jsme na prvni strance
			$prevlink = "<div class='unclickable'> &laquo; </div> <div class='unclickable'> &lsaquo; </div>";
		}
		
		$links = "";
		for ($i=0; $i < $pages; $i++) {
			if ($i == $page-1 ) {
				// tvorime odkaz pro stranku, na ktere jsme
				$links .= " <div class='unclickable'>".($i+1)."</div> ";
			} else {
				// tvorime odkaz pro jinou stranku
				$links .= " <a href=\"?page=".($i+1)."\">".($i+1)."</a> ";
			}
		}
		
		
		if ($page < $pages) {
			// nejsme na posledni strance
			$nextlink = '<a href="?page='.($page+1).'">&rsaquo;</a> <a href="?page='.$pages.'">&raquo;</a>';
		} else {
			// jsme na posledni strance
			$nextlink = "<div class='unclickable'> &rsaquo;</div> <div class='unclickable'> &raquo; </div>";
		}
		
		
		echo $prevlink, $links, $nextlink; 
		echo '</div>';
	}
            
    ?>