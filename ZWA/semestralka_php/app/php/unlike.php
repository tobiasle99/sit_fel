<?php
    require("./account _info.php");
    require("./csrf_token_validation.php");
    if (user_info() == false) {
        header('Location: https://wa.toad.cz/~letobias/semestralka_php/static/template/login.php');
    } elseif (CSRF()) {
        
        //dostavam obrazek, ktery chci unlike z GETu
        $picture = filter_input(INPUT_GET, 'picture', FILTER_VALIDATE_INT);
        array_push($_SESSION['liked'], $picture);
        define ('DBFILE', "../../data/database/_users.json");
        $data = file_get_contents(DBFILE);
        $decoded_data = json_decode($data, JSON_OBJECT_AS_ARRAY);
        $users = $decoded_data["users"];
        $replace = array();
        $replace_liked = array();


        foreach ($users as $user) {
            if ($user['id'] == $_SESSION['id']) {
                //spravny user

                if (($key = array_search($picture, $_SESSION['liked'])) !== false) {
                    //jestli dany obrazek je v array liked

                    $_SESSION['liked'] = $user['liked'];
                    $unliked_pic = $_SESSION['liked'][$key];


                    foreach($_SESSION['liked'] as $likedpic) {
                        if (!($likedpic == $unliked_pic)) {
                            //do replace liked davame vse krome obrazku,
                            // ktery se nam uz nelibi
                            array_push($replace_liked, $likedpic);
                    }}


                    $_SESSION['liked'] = $replace_liked;
                    $user['liked'] = $_SESSION['liked'];
                    if (array_key_exists('sorted', $_SESSION)) {
                        $_SESSION['sorted'] = false;
                    }
            }}
            array_push($replace, $user);
            unset($_COOKIE['token']);
        }

        $new_data = array("users" => ($replace));
        $encoded_data = json_encode($new_data);
        file_put_contents("../../data/database/_users.json", $encoded_data);
        header('Location: https://wa.toad.cz/~letobias/semestralka_php/static/template/fullsize.php?picture='.$unliked_pic);
        
    }
?>