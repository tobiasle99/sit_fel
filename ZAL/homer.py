"""
Homer's fridge
Course: B0B36ZAL
"""

#nasledujici kod nijak nemodifikujte!
class Food:
    def __init__(self, name, expiration):
        self.name = name
        self.expiration = expiration
#predesly kod nijak nemodifikujte!

def openFridge(fridge):
    print("Following items are in Homer's fridge:")
    for food in fridge:
        print("{0} (expires in: {1} days)".format(
            str(food.name), str(food.expiration))
        )
    print("")

#test vypisu - pri odevzdani smazte, nebo zakomentujte
fridge = [Food("beer", 4), Food("steak", 1), Food("hamburger", 1), Food("donut", 3)]
#openFridge(fridge)


"""
Task #1 DONE
"""
def maxExpirationDay(fridge):
    if len(fridge) == 0:
        return -1
    else:
        highest = -float('inf')
        for item in fridge:
            if item.expiration > highest:
                highest = item.expiration
        return highest

# test vypisu - pri odevzdani smazte, nebo zakomentujte
# print(maxExpirationDay(fridge))
# The command should print 4


"""
Task #2 DONE
"""
def histogramOfExpirations(fridge):
    result = [0]*(maxExpirationDay(fridge)+1)
    for item in fridge:
        result[item.expiration] += 1
    return result

# test vypisu - pri odevzdani smazte, nebo zakomentujte
# print(histogramOfExpirations(fridge))
# The command should print [0, 2, 0, 1, 1]


"""
Task #3 DONE
"""
def cumulativeSum(histogram):
    histcopy = histogram[::]
    result = histcopy[::]
    for i in range(len(histcopy)):
        cnt = 0
        for j in range(i+1):
            cnt += histcopy[j]
        result[i] = cnt
    return result

# test vypisu - pri odevzdani smazte, nebo zakomentujte
# print(cumulativeSum([0, 2, 0, 1, 1]))
# The command should print [0, 2, 2, 3, 4]


"""
Task #4
"""
def sortFoodInFridge(fridge):
    cumuSum = cumulativeSum(histogramOfExpirations(fridge))
    result = fridge[::]
    for item in fridge:
        exp = item.expiration
        cumuSum[exp] -= 1
        poslnd = cumuSum[exp]
        result[poslnd] = item
    return result
        

# test vypisu - pri odevzdani smazte, nebo zakomentujte
# openFridge(sortFoodInFridge(fridge))
# The command should print
# Following items are in Homer's fridge:
# hamburger (expires in: 1 days)
# steak (expires in: 1 days)
# donut (expires in: 3 days)
# beer (expires in: 4 days)


"""
Task #5
"""
def reverseFridge(fridge):
    fridgeCopy = fridge[::]
    result = fridgeCopy[::-1]
    return result

# test vypisu - pri odevzdani smazte, nebo zakomentujte
# penFridge(reverseFridge(fridge))
# The command should print
# Following items are in Homer's fridge:
# donut (expires in: 3 days)
# hamburger (expires in: 1 days)
# steak (expires in: 1 days)
# beer (expires in: 4 days)

# test vypisu - pri odevzdani smazte, nebo zakomentujte
# openFridge(sortFoodInFridge(reverseFridge(fridge)))
# The command should print
# Following items are in Homer's fridge:
# steak (expires in: 1 days)
# hamburger (expires in: 1 days)
# donut (expires in: 3 days)
# beer (expires in: 4 days)


"""
Task #6
"""
def eatFood(name, fridge):
    fridgeCopy = fridge[::]
    index = None
    exp = None
    found = False
    for i in range(len(fridgeCopy)):
        if fridgeCopy[i].name == name:
            if index == None:
                index = i
                exp = fridgeCopy[i].expiration
                found = True
            elif fridgeCopy[i].expiration < exp:
                index = i
                exp = fridgeCopy[i].expiration
    if found:
        del fridgeCopy[index]
    return fridgeCopy

# test vypisu - pri odevzdani smazte, nebo zakomentujte
""" openFridge(
    eatFood("donut",
        [Food("beer", 4), Food("steak", 1), Food("hamburger", 1),
        Food("donut", 3), Food("donut", 1), Food("donut", 6)]
    )) """
# The command should print
# Following items are in Homer's fridge:
# beer (expires in: 4 days)
# steak (expires in: 1 days)
# hamburger (expires in: 1 days)
# donut (expires in: 3 days)
# donut (expires in: 6 days)
