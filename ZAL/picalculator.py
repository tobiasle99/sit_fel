from unittest import result
from math import *

def newtonPi(init):
    xk = init
    equals = False
    current = "sin"
    while not equals:
        if current == "sin":
            xk1 = xk - sin(xk)/cos(xk)
            if xk == xk1:
                equals = True
                return xk
            else:
                xk = xk1


def leibnizPi(iterations):
    if iterations == 1:
        #print(4.0)
        return 4.0
    else:
        resultLeib = 4.0
        denominator = 3
        posOrNeg = -1
        for i in range(iterations-1):
            resultLeib += 4.0/(denominator*posOrNeg)
            denominator += 2
            posOrNeg *= -1
        #print(resultLeib)
        return resultLeib
        


def nilakanthaPi(iterations):
    if iterations == 1:
        #print(3.0)
        return 3.0
    else:
        resultNila = 3
        posOrNeg = 1
        for i in range(iterations-1):
            d1 = (i+1)*2
            d2 = d1+1
            d3 = d1+2
            #print(d1,d2,d3)
            resultNila += posOrNeg*4.0/(d1*d2*d3)
            posOrNeg *= -1
        #print(resultNila)
        return resultNila



#leibnizPi(int(input("input an int")))
#nilakanthaPi(int(input("input an int")))
#newtonPi(-3)