def permutations(array):
    #what is the base case
    if len(array) <= 1:
        return [array]
    
    inner = array
    poppedElement = inner.pop(0)
    result = []
    
    perms = permutations(inner)
    
    for perm in perms:
        for i in range(len(perm)+1):
            
            tmp = perm[::]
             
            tmp.insert(i, poppedElement)

            result.append(tmp)
    
    return result