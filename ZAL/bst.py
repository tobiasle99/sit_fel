class Node:
	def __init__(self, value):
		self.value = value
		self.left = None
		self.right = None

class BinarySearchTree:
	def __init__(self):
		self.head = None
		self.internalCounter = 0

	def insert(self, value):
		if self.head == None:
			self.head = Node(value)
			
		else:
			current = self.head
			while True:
				if value < current.value:
					if not current.left:		
						current.left = Node(value)
						break
					else:
						current = current.left		
				elif value > current.value:
					if current.right == None:
						current.right = Node(value)
						break
					elif current.right != None:
						current = current.right
     

	def fromArray(self, array):
		for value in array:
			if self.head == None:
				self.head = Node(value)
				
			else:
				current = self.head
				while True:
					if value < current.value:
						if not current.left:		
							current.left = Node(value)
							break
						else:
							current = current.left		
					elif value > current.value:
						if current.right == None:
							current.right = Node(value)
							break
						elif current.right != None:
							current = current.right
		
							
		
							

	def search(self, value):
		if self.head != None:
			self.internalCounter = 1
			current = self.head
			while True:
				if current.value == value:
					return True
				elif value < current.value:
					if current.left == None:
						return False
					elif current.left != None:
						self.internalCounter += 1
						current = current.left
						continue
				elif value > current.value:
					if current.right == None:
						return False
					elif current.right != None:
						self.internalCounter += 1
						current = current.right
						continue

	def min(self):
		min = None
		if self.head == None:
			return None
		else:
			self.internalCounter = 1
			current = self.head
			min = current.value
			while current.left:
				self.internalCounter += 1
				current = current.left
				min = current.value 
			return min

	
	def max(self):
		max = None
		if self.head == None:
			return None
		else:
			current = self.head
			max = current.value
			self.internalCounter = 1
			while current.right:
				self.internalCounter += 1
				current = current.right
				max = current.value
			return max     


	def visitedNodes(self):
		return(self.internalCounter)