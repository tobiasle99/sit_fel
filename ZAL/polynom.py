from operator import index


def polyEval(poly, x):
    res = 0
    for i in range(len(poly)):
        res += poly[i]*(x**i)
    return res
        


def polySum(poly1, poly2):
    res = []
    if len(poly1) == len(poly2):
        for i in range(len(poly2)):
            res.append(poly1[i] + poly2[i])
    elif len(poly1) > len(poly2):
        for i in range(len(poly2)):
            res.append(poly1[i] + poly2[i])
        for j in range(len(poly2), len(poly1)):
            res.append(poly1[j])
    else:
        for i in range(len(poly1)):
            res.append(poly1[i] + poly2[i])
        for j in range(len(poly1), len(poly2)):
            res.append(poly2[j])
    
    
    for m in range(len(res)-1,0 ,-1):
        print(index)
        if res[m] != 0:
            break
        elif res[m] == 0:
            res.pop(m)
    return res


def polyMultiply(poly1, poly2):
    res = [0]*(len(poly1)+len(poly2))
    for i in range(len(poly1)):
        for j in range(len(poly2)):
            res[i+j] += poly1[i]*poly2[j]
    for m in range(len(res)-1,0 ,-1):
        print(index)
        if res[m] != 0:
            break
        elif res[m] == 0:
            res.pop(m)
    return res