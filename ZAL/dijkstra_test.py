from dijkstra import *

#Test graph
vertexes = [
  Vertex(0, 'City0'),
  Vertex(1, 'City1'),
  Vertex(2, 'City2'),
  Vertex(3, 'City3'),
  Vertex(4, 'City4'),
  Vertex(5, 'City5'),
  Vertex(6, 'City6')
]
edges = [
  Edge(0, 6, 81),
  Edge(0, 4, 24),
  Edge(0, 1, 84),
  Edge(1, 3, 32),
  Edge(1, 5, 49),
  Edge(1, 6, 3),
  Edge(1, 2, 36),
  Edge(1, 4, 23),
  Edge(2, 5, 34),
  Edge(2, 1, 43),
  Edge(2, 4, 76),
  Edge(2, 3, 64),
  Edge(2, 0, 37),
  Edge(2, 6, 31),
  Edge(3, 2, 41),
  Edge(3, 1, 29),
  Edge(3, 0, 9),
  Edge(3, 6, 74),
  Edge(4, 6, 78),
  Edge(4, 1, 64),
  Edge(5, 2, 73),
  Edge(6, 0, 84),
  Edge(6, 3, 20)
]
#New Dijkstra created
dijkstra = Dijkstra()
#Graph created
dijkstra.createGraph(vertexes,edges)
#Getting all vertexes
dijkstraVertexes = dijkstra.getVertexes()
#Computing min distance for each vertex in graph
for vertexToCompute in dijkstraVertexes:
    dijkstra.computePath(vertexToCompute.id)
    print('Printing min distance from vertex:'+str(vertexToCompute.name))
    #Print minDitance from current vertex to each other
    for vertex in dijkstraVertexes:
        print('Min distance to:'+str(vertex.name)+' is: '+str(vertex.minDistance))
    #Reset Dijkstra between counting
    dijkstra.resetDijkstra()
#Distance with path
for vertexToCompute in dijkstraVertexes:
    dijkstra.computePath(vertexToCompute.id)
    print('Printing min distance from vertex:'+str(vertexToCompute.name))
    #Print minDitance and path from current vertex to each other
    for vertex in dijkstraVertexes:
        print('Min distance to:'+str(vertex.name)+' is: '+str(vertex.minDistance))
        print('Path is:',end=" ")
        #Get shortest path to target vertex
        path = dijkstra.getShortestPathTo(vertex.id)
        for vertexInPath in path:
            print(str(vertexInPath.name),end=" ")
        print()
    #Reset Dijkstra between counting
    dijkstra.resetDijkstra()