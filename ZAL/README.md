<h4> ZAL - Základy Algoritmizace - Basics of Algorithmization </h4>
<ol>
    <li>
        <h5>linewriter.py (working with files) </h5>
        <p>Create a function that contencates input with a static text</p>
    </li>
    <li>
        <h5>calculator.py (functions) </h5>
        <p>Create a controller that takes a string and integers as parameters and calls the correct function
        to return the correct result. </p>
    </li>
    <li>
        <h5>picalculator.py (loops) </h5>
        <p>Calculate the number pi using 3 different methods. </p>
    </li>
    <li>
        <h5>polynom.py (conditionals) </h5>
        <p>Calculate the sum and the product of two polynomials. </p>
    </li>
    <li>
        <h5>sort.py (sorting) </h5>
        <p>Sorting arrays using sorting algorithms. I used insert sort. </p>
    </li>
    <li>
        <h5>bst.py (binary trees) </h5>
        <p>Create binary trees out of arrays and then searching in them. </p>
    </li>
    <li>
        <h5>perms.py (recursion) </h5>
        <p>Create all different permutations of an array using recursion. </p>
    </li>
    <li>
        <h5>dijkstra.py (search algorithms) </h5>
        <p>Utilising all previous knowledge to implement the Dijkstra algorithm using Python3</p>
    </li>
    <li>
        <h5>homer.py (basics) </h5>
        <p>Exam preparation</p>
    </li>
</ol>