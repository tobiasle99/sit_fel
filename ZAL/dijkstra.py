from unicodedata import name


class Vertex:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.edges = []
        self.minDistance = float('inf')
        self.previousVertex = None


class Edge:
    def __init__(self, source, target, weight):
        self.source = source
        self.target = target
        self.weight = weight


class Dijkstra:
    def __init__(self):
        self.vertexesObjects = []
        self.vertexes = {}
        self.edgesToVertexes = []
        self.graph = []
        self.sourceId2 = None
        self.paths = []
        self.pathsIds = []


#MYSLIM ZE JE POTREBA REKURZE:(

    def computePath(self, sourceId):
        self.sourceId2 = sourceId
        queue = self.vertexes[sourceId].edges[::-1]
        visited = [sourceId]
        sourceEdge = self.vertexes[sourceId]
        sourceEdge.minDistance = 0
        
        while len(queue) > 0:
            currentEdge = queue.pop()
            #print("CURRENT EDGE IS (%d %d %d) " %(currentEdge.source, currentEdge.target, currentEdge.weight))
            if currentEdge.target not in visited:
                
                visited.append(currentEdge.target)
                currentVertex = self.vertexes[currentEdge.target]
                for edge in currentVertex.edges:
                    edgecopy = Edge(edge.source, edge.target, edge.weight)
                    queue.insert(0, edgecopy)
                    #print("ADDDING EDGE (%d %d %d)" %(edge.source,edge.target,edge.weight))
                
            
            if (self.graph[sourceId][currentEdge.source] + currentEdge.weight) < (self.graph[sourceId][currentEdge.target]):
                #print("PATH FROM %d TO %d is CHANGING BECAUSE %f < %f " %(sourceId, currentEdge.target,(self.graph[sourceId][currentEdge.source] + currentEdge.weight), (self.graph[sourceId][currentEdge.target]) ))
                self.graph[sourceId][currentEdge.target] = self.graph[sourceId][currentEdge.source] + currentEdge.weight
                self.vertexes[currentEdge.target].minDistance = self.graph[sourceId][currentEdge.source] + currentEdge.weight
                self.vertexes[currentEdge.target].previousVertex = self.vertexes[currentEdge.source]
                if currentEdge.target in visited:
                    for edge in self.vertexes[currentEdge.target].edges:
                        edgecopy = Edge(edge.source, edge.target, edge.weight)
                        queue.insert(0, edgecopy)
                        #print("RE-ADDDING EDGE (%d %d %d)" %(edge.source,edge.target,edge.weight))
            

    def getShortestPathTo(self, targetId):
        current = self.vertexes[targetId]
        self.paths[targetId].insert(0, current)
        self.pathsIds[targetId].insert(0, current.id)
        while current != self.vertexes[self.sourceId2]:
            self.paths[targetId].insert(0, current.previousVertex)
            self.pathsIds[targetId].insert(0, current.previousVertex.id)
            current = current.previousVertex
        return self.paths[targetId]

    def createGraph(self, vertexes, edgesToVertexes):
        #save vertexes as a key:value pair where key == id and value == vertex
        self.vertexesObjects = vertexes
        for vertex in vertexes:
            self.vertexes[vertex.id] = vertex
        
        self.edgesToVertexes = edgesToVertexes

            
        #assign each vertex a list of shortest paths, path to yourself is 0
        for i in range(len(vertexes)):
            self.graph.append([float('inf')]*len(vertexes))
            self.graph[i][i] = 0
            self.paths.append([])
            self.pathsIds.append([])
            
        
        #assign vertexes their edges
        for edge in self.edgesToVertexes:
            self.vertexes[edge.source].edges.append(edge)
        
        
        
    def resetDijkstra(self):
        self.paths = []
        for vertex in self.vertexesObjects:
            vertex.minDistance = float('inf')
            vertex.previousVertex = None
        self.graph = []
        for i in range(len(self.vertexes)):
            self.graph.append([float('inf')]*len(self.vertexes))
            self.graph[i][i] = 0
            self.paths.append([])
            self.pathsIds.append([])
        
            

    def getVertexes(self):
        
        return self.vertexesObjects
