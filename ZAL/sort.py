def sortNumbers(data, condition):
    #TODO Simple insert sort implementation
    result = []
    if len(data) < 1:
        return result
    
    elif condition == "ASC":
        for i in range(len(data)):
            if len(result) < 1:
                result.append(data[i])
            else:
                for j in range(len(result)):   
                    if data[i] < result[j]:
                        result.insert(j, data[i])

                        break
                    elif data[i]>= result[j]:
                        if j+1 == len(result):
                            result.append(data[i])

    elif condition == "DESC":
        for i in range(len(data)):
            if len(result) < 1:
                result.append(data[i])
            else:
                for j in range(len(result)):             
                    if data[i] > result[j]:
                        result.insert(j, data[i])

                        break
                    elif data[i]<= result[j]:
                        if j+1 == len(result):
                            result.append(data[i])

    return result
        

def sortData(weights, data, condition):
    result = []
    resultweights = []
    if len(weights) != len(data):
        raise ValueError('Invalid input data')
    elif len(data) < 1:
        return result
    
    elif condition == "ASC":
        for i in range(len(weights)):
            if len(result) < 1:
                result.append(data[i])
                resultweights.append(weights[i])
            else:
                for j in range(len(result)):   
                    if weights[i] < resultweights[j]:
                        result.insert(j, data[i])
                        resultweights.insert(j, weights[i])
                        break
                    elif weights[i] >= resultweights[j]:
                        if j+1 == len(result):
                            result.append(data[i])
                            resultweights.append(weights[i])
                            
    elif condition == "DESC":
        for i in range(len(weights)):
            if len(result) < 1:
                result.append(data[i])
                resultweights.append(weights[i])
            else:
                for j in range(len(result)):   
                    if weights[i] > resultweights[j]:
                        result.insert(j, data[i])
                        resultweights.insert(j, weights[i])
                        break
                    elif weights[i] <= resultweights[j]:
                        if j+1 == len(result):
                            result.append(data[i])
                            resultweights.append(weights[i])
    return result