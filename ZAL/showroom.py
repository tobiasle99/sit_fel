from cgi import print_exception


class Node:
    def __init__(self, nextNode, prevNode, data):
        self.nextNode = nextNode
        self.prevNode = prevNode
        self.data = data


class LinkedList:
    def __init__(self):
        #create an empty linked list
        self.head = None


class Car:
    def __init__(self, identification, name, brand, price, active):
        self.identification = identification
        self.name = name
        self.brand = brand
        self.price = price
        self.active = active
    
    def is_empty(self):
        if (self.name == None) or (self.brand == None) or (self.price == None) or (self.active == None):
            return True


db = LinkedList()


def init(cars):
    for car in cars:
        #if car data is not empty
        if not car.is_empty():
            #if the linked list is empty, make the car the head of the list
            if db.head == None:
                newNode = Node(None, None, car)
                db.head = newNode
            elif db.head.nextNode == None:
                if car.price >= db.head.data.price:
                    newNode = Node(None, db.head, car)
                    db.head.nextNode = newNode    
                else:
                    newNode = Node(db.head, None, car)
                    db.head.prevNode = newNode
                    db.head = newNode
            #otherwise start at the head
            else:
                current = db.head 
                #as long as there is a next node, check if price is larger or equal(for stable sorting)
                while current.nextNode != None:
                    if car.price >= current.data.price:
                        current = current.nextNode
                          
                    #if car's price is lower than the current node, insert it in front and connect car to current node
                    else:
                        #if current node is linkedlist head, then replace it as head
                        if current == db.head:
                            newNode = Node(current, None, car)
                            current.prevNode = newNode
                            db.head = newNode
                            break
                        else:
                            newNode = Node(current, current.prevNode, car)
                            current.prevNode.nextNode = newNode
                            current.prevNode = newNode
                            break
                #if we reach the last node, we connect it to the end
                if current.nextNode == None:
                    if car.price >= current.data.price: 
                        newNode = Node(None, current, car)
                        current.nextNode = newNode
                    else:
                        newNode = Node(current, current.prevNode, car)
                        current.prevNode.nextNode = newNode
                        current.prevNode = newNode
        #if car data is empty, then we move onto the next car in cars
        else:
            continue
                


def add(car):
    if not car.is_empty():
    #if the linked list is empty, make the car the head of the list
        if db.head == None:
            newNode = Node(None, None, car)
            db.head = newNode
        elif db.head.nextNode == None:
            if car.price >= db.head.data.price:
                newNode = Node(None, db.head, car)
                db.head.nextNode = newNode    
            else:
                newNode = Node(db.head, None, car)
                db.head.prevNode = newNode
                db.head = newNode
        #otherwise start at the head
        else:
            current = db.head 
            #as long as there is a next node, check if price is larger or equal(for stable sorting)
            while current.nextNode != None:
                if car.price >= current.data.price:
                    current = current.nextNode
                        
                #if car's price is lower than the current node, insert it in front and connect car to current node
                else:
                    #if current node is linkedlist head, then replace it as head
                    if current == db.head:
                        newNode = Node(current, None, car)
                        current.prevNode = newNode
                        db.head = newNode
                        break
                    else:
                        newNode = Node(current, current.prevNode, car)
                        current.prevNode.nextNode = newNode
                        current.prevNode = newNode
                        break
            #if we reach the last node, we connect it to the end
            if current.nextNode == None:
                if car.price >= current.data.price: 
                    newNode = Node(None, current, car)
                    current.nextNode = newNode
                else:
                    newNode = Node(current, current.prevNode, car)
                    current.prevNode.nextNode = newNode
                    current.prevNode = newNode

def updateName(identification, name):
    current = db.head
    #while there is a nextNode, we check if id matches the given identification, if yes we change the name, if not, we move onto the next Node
    while current.nextNode != None:
        if int(current.data.identification) == int(identification):
            current.data.name = name
            break
        else:
            current = current.nextNode     
    #if we reach the end of the list and the last car matches the id, then we change it's name
    if current.data.identification == identification:
        current.data.name = name


def updateBrand(identification, brand):
    current = db.head
    #while there is a nextNode, we check if id matches the given identification, if yes we change the brand, if not, we move onto the next Node
    while current.nextNode:
        if current.data.identification == identification:
            current.data.brand = brand
            break
        else:
            current = current.nextNode
    #if we reach the end of the list and the last car matches the id, then we change it's brand
    if current.data.identification == identification:
        current.data.brand = brand


def activateCar(identification):
    current = db.head
    #while there is a nextNode, we check if id matches the given identification, we activate it, otherwise we move on
    while current.nextNode:
        if current.data.identification == identification:
            current.data.active = True
            break
        else:
            current = current.nextNode
    #if we reach the end of the list and the last car matches the id, then we activate it
    if current.data.identification == identification:
        current.data.active = True


def deactivateCar(identification):
    current = db.head
    #while there is a nextNode, we check if id matches the given identification, we deactivate it, otherwise we move on
    while current.nextNode:
        if current.data.identification == identification:
            current.data.active = False
            break
        else:
            current = current.nextNode
    #if we reach the end of the list and the last car matches the id, then we deactivate it
    if current.data.identification == identification:
        current.data.active = False


def getDatabaseHead():
    return db.head


def getDatabase():
    return db


def calculateCarPrice():
    total = 0
    if db.head == None:
        return total
    else:
        current = db.head
        while current.nextNode:
            if current.data.active:
                total+= current.data.price
            current = current.nextNode
        if current.data.active:
                total+= current.data.price
    return total

def clean():
    db.head = None
