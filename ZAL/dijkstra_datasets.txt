DATASET 1 
vertexes = [
  Vertex(0, 'Redville'),
  Vertex(1, 'Blueville'),
  Vertex(2, 'Greenville'),
  Vertex(3, 'Orangeville'),
  Vertex(4, 'Purpleville')
]
edges = [
  Edge(0, 1, 5),
  Edge(0, 2, 10),
  Edge(0, 3, 8),
  Edge(1, 0, 5),
  Edge(1, 2, 3),
  Edge(1, 4, 7),
  Edge(2, 0, 10),
  Edge(2, 1, 3),
  Edge(3, 0, 8),
  Edge(3, 4, 2),
  Edge(4, 1, 7),
  Edge(4, 3, 2)
]


------------------------------------------------------------------------------------------------------------------------------------------------------
DATASET 2
vertexes = [
  Vertex(0, 'Redville'),
  Vertex(1, 'Blueville'),
  Vertex(2, 'Greenville'),
  Vertex(3, 'Orangeville'),
  Vertex(4, 'Purpleville')
]
edges = [
  Edge(0, 1, 9),
  Edge(0, 2, 1),
  Edge(0, 3, 4),
  Edge(1, 0, 5),
  Edge(1, 2, 3),
  Edge(1, 4, 3),
  Edge(2, 0, 3),
  Edge(2, 1, 3),
  Edge(3, 0, 7),
  Edge(3, 4, 1),
  Edge(4, 1, 8),
  Edge(4, 3, 8)
]


------------------------------------------------------------------------------------------------------------------------------------------------------
DATASET 3
vertexes = [
  Vertex(0, 'City0'),
  Vertex(1, 'City1'),
  Vertex(2, 'City2'),
  Vertex(3, 'City3'),
  Vertex(4, 'City4')
]
edges = [
  Edge(0, 1, 83),
  Edge(0, 4, 99),
  Edge(0, 1, 2),
  Edge(1, 0, 22),
  Edge(1, 2, 6),
  Edge(1, 4, 47),
  Edge(2, 3, 87),
  Edge(3, 0, 14),
  Edge(3, 1, 33),
  Edge(3, 2, 24),
  Edge(4, 1, 7),
  Edge(4, 3, 77),
  Edge(4, 2, 49)
]


------------------------------------------------------------------------------------------------------------------------------------------------------
DATASET 4
vertexes = [
  Vertex(0, 'City0'),
  Vertex(1, 'City1'),
  Vertex(2, 'City2'),
  Vertex(3, 'City3'),
  Vertex(4, 'City4'),
  Vertex(5, 'City5'),
  Vertex(6, 'City6')
]
edges = [
  Edge(0, 6, 81),
  Edge(0, 4, 24),
  Edge(0, 1, 84),
  Edge(1, 3, 32),
  Edge(1, 5, 49),
  Edge(1, 6, 3),
  Edge(1, 2, 36),
  Edge(1, 4, 23),
  Edge(2, 5, 34),
  Edge(2, 1, 43),
  Edge(2, 4, 76),
  Edge(2, 3, 64),
  Edge(2, 0, 37),
  Edge(2, 6, 31),
  Edge(3, 2, 41),
  Edge(3, 1, 29),
  Edge(3, 0, 9),
  Edge(3, 6, 74),
  Edge(4, 6, 78),
  Edge(4, 1, 64),
  Edge(5, 2, 73),
  Edge(6, 0, 84),
  Edge(6, 3, 20)
]