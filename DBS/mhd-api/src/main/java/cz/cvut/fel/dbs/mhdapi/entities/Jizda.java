package cz.cvut.fel.dbs.mhdapi.entities;

import jakarta.persistence.*;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "jizda",
uniqueConstraints = @UniqueConstraint(name = "jizda_UQ_id_jizdniho_radu_datum",
columnNames = {"id_jizdniho_radu", "datum"}))
public class Jizda {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_jizdy")
    private int idJizdy;

    @ManyToOne
    @JoinColumn(name = "id_jizdniho_radu")
    private JizdniRad jizdniRad;

    @ManyToOne
    @JoinColumn(name = "cislo_vozidla")
    private Vozidlo vozidlo;

    @ManyToOne
    @JoinColumn(name = "id_zamestnance")
    private Ridic ridic;

    @Column(name = "datum", nullable = false)
    private Date datum;

    @OneToMany(mappedBy = "jizda")
    private Set<Nastoupeni> nastoupeni;

    @ManyToMany
    @JoinTable(
            name = "Nastoupeni",
            joinColumns = @JoinColumn(name = "id_jizdy"),
            inverseJoinColumns = @JoinColumn(name = "cislo_kuponu")
    )
    private List<Pasazer> pasazeri;

    public Jizda() {
    }

    public List<Pasazer> getPasazeri() {
        if (pasazeri == null) {
            pasazeri = nastoupeni.stream().map(Nastoupeni::getPasazer).collect(Collectors.toList());
        }
        return pasazeri;
    }

    public int getIdJizdy() {
        return idJizdy;
    }

    public void setIdJizdy(int idJizdy) {
        this.idJizdy = idJizdy;
    }

    public JizdniRad getJizdniRad() {
        return jizdniRad;
    }

    public void setJizdniRad(JizdniRad jizdniRad) {
        this.jizdniRad = jizdniRad;
    }

    public Vozidlo getVozidlo() {
        return vozidlo;
    }

    public void setVozidlo(Vozidlo vozidlo) {
        this.vozidlo = vozidlo;
    }

    public Ridic getRidic() {
        return ridic;
    }

    public void setRidic(Ridic ridic) {
        this.ridic = ridic;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }
}
