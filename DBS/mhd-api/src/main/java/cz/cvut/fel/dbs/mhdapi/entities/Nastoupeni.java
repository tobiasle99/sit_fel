package cz.cvut.fel.dbs.mhdapi.entities;
import cz.cvut.fel.dbs.mhdapi.entities.CompositeKeys.NastoupeniId;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "nastoupeni")
public class Nastoupeni {
    @EmbeddedId
    private NastoupeniId id;

    @ManyToOne
    @MapsId("cisloKuponu")
    @JoinColumn(name = "cislo_kuponu")
    private Pasazer pasazer;

    @ManyToOne
    @MapsId("idJizdy")
    @JoinColumn(name = "id_jizdy")
    private Jizda jizda;

    public Nastoupeni() {
    }

    public Pasazer getPasazer() {
        return pasazer;
    }

    public void setPasazer(Pasazer pasazer) {
        this.pasazer = pasazer;
    }

    public Jizda getJizda() {
        return jizda;
    }

    public void setJizda(Jizda jizda) {
        this.jizda = jizda;
    }
}
