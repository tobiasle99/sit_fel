package cz.cvut.fel.dbs.mhdapi.entities;
import jakarta.persistence.*;


@Entity
@Table(name = "Revizor")
@DiscriminatorColumn(name = "EMP_TYPE")
@DiscriminatorValue("Revizor")
public class Revizor extends Zamestnanec{
}


