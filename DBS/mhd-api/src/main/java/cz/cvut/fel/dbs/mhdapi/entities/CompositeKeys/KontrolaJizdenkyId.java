package cz.cvut.fel.dbs.mhdapi.entities.CompositeKeys;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class KontrolaJizdenkyId implements Serializable {

    @Column(name = "id_zamestnance")
    private Integer idZamestnance;

    @Column(name = "cislo_kuponu")
    private String cisloKuponu;

    // Constructors, getters, setters, equals, and hashCode methods


    public KontrolaJizdenkyId() {
    }

    public KontrolaJizdenkyId(Integer idZamestnance, String cisloKuponu) {
        this.idZamestnance = idZamestnance;
        this.cisloKuponu = cisloKuponu;
    }

    // Make sure to override equals and hashCode methods based on the composite key fields
    // Example implementation:

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KontrolaJizdenkyId that = (KontrolaJizdenkyId) o;
        return Objects.equals(idZamestnance, that.idZamestnance) &&
                Objects.equals(cisloKuponu, that.cisloKuponu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idZamestnance, cisloKuponu);
    }

    // Getters and setters...

    public Integer getIdZamestnance() {
        return idZamestnance;
    }

    public void setIdZamestnance(Integer idZamestnance) {
        this.idZamestnance = idZamestnance;
    }

    public String getCisloKuponu() {
        return cisloKuponu;
    }

    public void setCisloKuponu(String cisloKuponu) {
        this.cisloKuponu = cisloKuponu;
    }
}
