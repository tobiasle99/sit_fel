package cz.cvut.fel.dbs.mhdapi.entities;
import jakarta.persistence.*;

@Entity
@Table(name = "Ridic")
@DiscriminatorColumn(name = "EMP_TYPE")
@DiscriminatorValue("Ridic")
public class Ridic extends Zamestnanec{
    @Column(name = "kategorie_ridicskeho_prukazu")
    private String kategorieRidicskehoPrukazu;

    public Ridic() {
    }

    public String getKategorieRidicskehoPrukazu() {
        return kategorieRidicskehoPrukazu;
    }

    public void setKategorieRidicskehoPrukazu(String kategorieRidicskehoPrukazu) {
        this.kategorieRidicskehoPrukazu = kategorieRidicskehoPrukazu;
    }
}
