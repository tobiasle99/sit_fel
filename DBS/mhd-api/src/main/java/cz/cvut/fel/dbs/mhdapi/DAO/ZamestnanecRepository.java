package cz.cvut.fel.dbs.mhdapi.DAO;

import cz.cvut.fel.dbs.mhdapi.entities.Zamestnanec;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ZamestnanecRepository extends JpaRepository<Zamestnanec, Integer> {
}
