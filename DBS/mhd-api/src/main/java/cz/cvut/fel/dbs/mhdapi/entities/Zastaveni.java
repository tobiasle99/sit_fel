package cz.cvut.fel.dbs.mhdapi.entities;

import jakarta.persistence.*;

import java.sql.Date;
import java.sql.Time;
import java.sql.Time;

@Entity
@Table(name = "zastaveni",
        uniqueConstraints =
        @UniqueConstraint(name = "zastaveni_UQ_id_nastupiste_id_jizdy",
                columnNames = {"id_nastupiste", "id_jizdy"})
)
public class Zastaveni {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_zastaveni")
    private int idZastaveni;

    @ManyToOne
    @JoinColumn(name = "id_nastupiste")
    private Nastupiste nastupiste;

    @ManyToOne
    @JoinColumn(name = "id_jizdy", nullable = false)
    private Jizda jizda;

    @Column(name = "cas", nullable = false)
    private Time cas;

    @Column(name = "datum", nullable = false)
    private Date datum;

    public Zastaveni() {
    }

    public int getIdZastaveni() {
        return idZastaveni;
    }

    public void setIdZastaveni(int idZastaveni) {
        this.idZastaveni = idZastaveni;
    }

    public Nastupiste getNastupiste() {
        return nastupiste;
    }

    public void setNastupiste(Nastupiste nastupiste) {
        this.nastupiste = nastupiste;
    }

    public Jizda getJizda() {
        return jizda;
    }

    public void setJizda(Jizda jizda) {
        this.jizda = jizda;
    }

    public Time getCas() {
        return cas;
    }

    public void setCas(Time cas) {
        this.cas = cas;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    // Constructors, getters, and setters
}
