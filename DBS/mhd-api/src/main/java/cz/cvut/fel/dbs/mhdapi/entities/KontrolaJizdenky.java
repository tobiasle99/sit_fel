package cz.cvut.fel.dbs.mhdapi.entities;
import cz.cvut.fel.dbs.mhdapi.entities.CompositeKeys.KontrolaJizdenkyId;
import jakarta.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name = "kontrola_jizdenky")
public class KontrolaJizdenky {

    @EmbeddedId
    private KontrolaJizdenkyId id;

    @Column(name = "cas")
    private Time cas;

    @Column(name = "datum")
    private Date datum;

    @Column(name = "platny_kupon")
    private Boolean platnyKupon;
    // Constructors, getters, setters, and other entity fields

    public KontrolaJizdenky() {
        // Required default constructor
    }

    public Boolean getPlatnyKupon() {
        return platnyKupon;
    }

    public KontrolaJizdenky(KontrolaJizdenkyId id, Time cas, Date datum) {
        this.id = id;
        this.cas = cas;
        this.datum = datum;
    }

    public Time getCas() {
        return cas;
    }

    public void setCas(Time cas) {
        this.cas = cas;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public KontrolaJizdenkyId getId() {
        return id;
    }

    public void setId(KontrolaJizdenkyId id) {
        this.id = id;
    }
}

