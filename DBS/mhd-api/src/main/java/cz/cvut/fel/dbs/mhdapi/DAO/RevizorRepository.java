package cz.cvut.fel.dbs.mhdapi.DAO;

import cz.cvut.fel.dbs.mhdapi.entities.Revizor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RevizorRepository extends JpaRepository<Revizor, Integer> {
}
