package cz.cvut.fel.dbs.mhdapi.entities;
import jakarta.persistence.*;

@Entity
@Table(name = "nastupiste",
uniqueConstraints = @UniqueConstraint(
        name = "nastupiste_UQ_id_zastavky_znaceni",
        columnNames = {"id_zastavky", "znaceni"}
))
public class Nastupiste {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nastupiste")
    private Integer idNastupiste;

    @ManyToOne
    @JoinColumn(name = "id_zastavky")
    private Zastavka zastavka;

    @Column(name = "znaceni", nullable = false, length = 5)
    private String znaceni;

    public Nastupiste() {
    }

    public Integer getIdNastupiste() {
        return idNastupiste;
    }

    public void setIdNastupiste(Integer idNastupiste) {
        this.idNastupiste = idNastupiste;
    }

    public Zastavka getZastavka() {
        return zastavka;
    }

    public void setZastavka(Zastavka zastavka) {
        this.zastavka = zastavka;
    }

    public String getZnaceni() {
        return znaceni;
    }

    public void setZnaceni(String znaceni) {
        this.znaceni = znaceni;
    }

    // Constructors, getters, and setters
}

