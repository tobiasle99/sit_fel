package cz.cvut.fel.dbs.mhdapi.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "trasa")
public class Trasa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_trasy")
    private int idTrasy;

    @Column(name = "nazev", length = 50, nullable = false)
    private String nazev;

    public Trasa() {
    }

    public int getIdTrasy() {
        return idTrasy;
    }

    public void setIdTrasy(int idTrasy) {
        this.idTrasy = idTrasy;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }
}