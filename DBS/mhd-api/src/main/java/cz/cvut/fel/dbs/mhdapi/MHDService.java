package cz.cvut.fel.dbs.mhdapi;

import cz.cvut.fel.dbs.mhdapi.DAO.*;
import cz.cvut.fel.dbs.mhdapi.entities.CompositeKeys.*;
import cz.cvut.fel.dbs.mhdapi.entities.*;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.SQLDataException;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class MHDService {

    private TrasaRepository trasaRepository;
    private KontrolaJizdenkyRepository kontrolaJizdenkyRepository;
    private VozidloRepository vozidloRepository;
    private PasazerRepository pasazerRepository;
    private JizdaRepository jizdaRepository;
    private RidicRepository ridicRepository;

    @Autowired
    public MHDService(TrasaRepository trasaRepository, KontrolaJizdenkyRepository kontrolaJizdenkyRepository,
                      VozidloRepository vozidloRepository, PasazerRepository pasazerRepository,
                      JizdaRepository jizdaRepository, RidicRepository ridicRepository) {
        this.trasaRepository = trasaRepository;
        this.kontrolaJizdenkyRepository = kontrolaJizdenkyRepository;
        this.vozidloRepository = vozidloRepository;
        this.pasazerRepository = pasazerRepository;
        this.jizdaRepository = jizdaRepository;
        this.ridicRepository = ridicRepository;
    }

    public List<Trasa> getAllRoutes(){
        return trasaRepository.findAll();
    }

    public KontrolaJizdenky addNewTicketCheck(KontrolaJizdenky novaKontrola){
        return kontrolaJizdenkyRepository.save(novaKontrola);
    }

    public List<KontrolaJizdenky> findCheckByIdIdZamestnanceAndIdCisloKuponu(int idZamestnance, String cisloKuponu){
        return kontrolaJizdenkyRepository.findByIdIdZamestnanceAndIdCisloKuponu(idZamestnance, cisloKuponu);
    }

    public List<KontrolaJizdenky> findCheckByIdZamestnance(int idZamestnance){
        return kontrolaJizdenkyRepository.findAllByIdIdZamestnance(idZamestnance);
    }

    public List<KontrolaJizdenky> findCheckByCisloKuponu(String cisloKuponu){
        return kontrolaJizdenkyRepository.findAllByIdCisloKuponu(cisloKuponu);
    }

    @Transactional
    public void executeTicketCheckTransaction(int idZamestnance, String cisloKuponu) {
        kontrolaJizdenkyRepository.findAllById_IdZamestnanceAndId_CisloKuponu(idZamestnance, cisloKuponu);

        LocalDateTime currentTime = LocalDateTime.now();

        KontrolaJizdenky kontrolaJizdenky1 = new KontrolaJizdenky();
        kontrolaJizdenky1.setId(new KontrolaJizdenkyId(idZamestnance, cisloKuponu));
        kontrolaJizdenky1.setCas(Time.valueOf(currentTime.toLocalTime()));
        kontrolaJizdenky1.setDatum(Date.valueOf(currentTime.toLocalDate()));
        kontrolaJizdenkyRepository.save(kontrolaJizdenky1);

        LocalDateTime nextTime = currentTime.plusMinutes(5); // Example: 5 minutes interval

        KontrolaJizdenky kontrolaJizdenky2 = new KontrolaJizdenky();
        kontrolaJizdenky2.setId(new KontrolaJizdenkyId(idZamestnance, cisloKuponu));
        kontrolaJizdenky2.setCas(Time.valueOf(nextTime.toLocalTime()));
        kontrolaJizdenky2.setDatum(Date.valueOf(nextTime.toLocalDate()));
        kontrolaJizdenkyRepository.save(kontrolaJizdenky2);
    }


    public Vozidlo addNewCar(Vozidlo vozidlo) throws SQLDataException {
        int count = vozidloRepository.countByCisloVozidlaOrSpz(vozidlo.getCisloVozidla(), vozidlo.getSPZ());
        if (count > 0) {
            throw new SQLDataException("Car with given SPZ or cislo vozidla already exists");
        }
        return vozidloRepository.save(vozidlo);
    }

    public Vozidlo findCarByCisloVozidla(String cisloVozidla){
        return vozidloRepository.findByCisloVozidla(cisloVozidla);
    }

    public Optional<Pasazer> getPassenger(String cisloKuponu) {
        return pasazerRepository.findPasazerByCisloKuponu(cisloKuponu);
    }

    public List<Jizda> findJizdyByCouponNumber(String couponNumber){
        return jizdaRepository.findJizdyByCouponNumber(couponNumber);
    }

    public Optional<Jizda> findJizdaByIdJizdy(long idJizdy){
        System.out.println(idJizdy);
        return jizdaRepository.findById(idJizdy);
    }

    public Ridic findRidicById(int id){
        return ridicRepository.findRidicByIdZamestnance(id);
//        return ridicRepository.findRidicWithZamestnanecInfo(id);
    }
}
