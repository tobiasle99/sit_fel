package cz.cvut.fel.dbs.mhdapi;

import cz.cvut.fel.dbs.mhdapi.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.sql.SQLDataException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class MHDController {
    MHDService mhdService;

    @Autowired
    public MHDController(MHDService mhdService) {
        this.mhdService = mhdService;
    }

    @GetMapping("/routes")
    public List<Trasa> getAllRoutes(){
        return mhdService.getAllRoutes();
    }

    @PostMapping("/ticketCheck/new")
    public KontrolaJizdenky postNewTicketCheck(@RequestBody KontrolaJizdenky novaKontrola){
        return mhdService.addNewTicketCheck(novaKontrola);
    }

    @GetMapping("/ticketCheck")
    public List<KontrolaJizdenky> findByIdIdZamestnanceAndIdCisloKuponu(@RequestParam(required = false) Integer idZamestnance,
                                                                        @RequestParam(required = false) String cisloKuponu) {
        if (idZamestnance != null && cisloKuponu != null) {
            return mhdService.findCheckByIdIdZamestnanceAndIdCisloKuponu(idZamestnance, cisloKuponu);
        } else if (idZamestnance != null) {
            return mhdService.findCheckByIdZamestnance(idZamestnance);
        } else if (cisloKuponu != null) {
            return mhdService.findCheckByCisloKuponu(cisloKuponu);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid input");
        }
    }


    @PostMapping("/ticketCheck/transaction")
    public void executeTicketCheckTransaction(@RequestParam int idZamestnance,
                                              @RequestParam String cisloKuponu){
        mhdService.executeTicketCheckTransaction(idZamestnance, cisloKuponu);
    }

    @PostMapping("/car/new")
    public Vozidlo addNewCar(@RequestBody Vozidlo car) throws SQLDataException {
        if (car.getSPZ().length() == 8 && car.getCisloVozidla().length() == 4) {
            return mhdService.addNewCar(car);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid input");
        }
    }

    @GetMapping("/car")
    public Vozidlo getCarByCisloVozidla(@RequestParam String cisloVozidla){
        if (cisloVozidla.length() != 4){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid input");
        } else {
            return mhdService.findCarByCisloVozidla(cisloVozidla);
        }
    }

    @GetMapping("/passenger")
    public Optional<Pasazer> getAllPassengers(@RequestParam String cisloKuponu) {
        return mhdService.getPassenger(cisloKuponu);
    }

    @GetMapping("/passenger/trips")
    public List<Jizda> getPassengersTrips(@RequestParam String cisloKuponu){
        return mhdService.findJizdyByCouponNumber(cisloKuponu);
    }

    @GetMapping("/trips")
    public Optional<Jizda> getJizdaByIdJizdy(@RequestParam long idJizdy){
        return mhdService.findJizdaByIdJizdy(idJizdy);
    }

    @GetMapping("/driver")
    public Ridic getDriverById(@RequestParam int id){
        return mhdService.findRidicById(id);
    }
}
