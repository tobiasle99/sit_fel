package cz.cvut.fel.dbs.mhdapi.entities;
import jakarta.persistence.*;

@Entity
@Table(name = "zastavka", uniqueConstraints = @UniqueConstraint(
        name = "zastavka_UQ_jmeno_adresa", columnNames = {"jmeno", "mesto"}
))
public class Zastavka {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_zastavky")
    private Integer idZastavky;

    @Column(name = "jmeno", nullable = false)
    private String jmeno;

    @Column(name = "mesto", nullable = false)
    private String mesto;

    public Zastavka() {
    }

    public Integer getIdZastavky() {
        return idZastavky;
    }

    public void setIdZastavky(Integer idZastavky) {
        this.idZastavky = idZastavky;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }
}
