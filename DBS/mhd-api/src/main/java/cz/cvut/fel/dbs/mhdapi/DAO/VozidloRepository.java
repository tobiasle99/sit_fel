package cz.cvut.fel.dbs.mhdapi.DAO;

import cz.cvut.fel.dbs.mhdapi.entities.Vozidlo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VozidloRepository extends JpaRepository<Vozidlo, String> {
    @Query("SELECT v FROM Vozidlo v WHERE v.cisloVozidla = :cisloVozidla")
    Vozidlo findByCisloVozidla(@Param("cisloVozidla") String cisloVozidla);

    @Query("SELECT COUNT(v) FROM Vozidlo v WHERE v.cisloVozidla = :cisloVozidla OR v.SPZ = :spz")
    int countByCisloVozidlaOrSpz(@Param("cisloVozidla") String cisloVozidla, @Param("spz") String spz);

    @Query("SELECT v FROM Vozidlo v WHERE v.cisloVozidla = :cisloVozidla")
    default Vozidlo findByCisloVozidlaNoUpdate(@Param("cisloVozidla") String cisloVozidla) {
        int count = countByCisloVozidlaOrSpz(cisloVozidla, null);
        if (count > 0) {
            return null; // Car with the given cisloVozidla or Spz already exists
        }
        return findFirstByCisloVozidla(cisloVozidla);
    }

    @Query("SELECT v FROM Vozidlo v WHERE v.cisloVozidla = :cisloVozidla")
    Vozidlo findFirstByCisloVozidla(@Param("cisloVozidla") String cisloVozidla);
}
