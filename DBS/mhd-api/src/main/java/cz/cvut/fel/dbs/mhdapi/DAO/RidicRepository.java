package cz.cvut.fel.dbs.mhdapi.DAO;

import cz.cvut.fel.dbs.mhdapi.entities.Ridic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface RidicRepository extends JpaRepository<Ridic, Long> {
    @Query("SELECT r, z FROM Ridic r JOIN Zamestnanec z ON r.idZamestnance = z.idZamestnance WHERE r.idZamestnance = :id")
    Ridic findRidicWithZamestnanecInfo(@Param("id") int id);

    @Query("SELECT r FROM Ridic r WHERE r.idZamestnance = :idZamestnance")
    Ridic findRidicByIdZamestnance(@Param("idZamestnance") int idZamestnance);
}
