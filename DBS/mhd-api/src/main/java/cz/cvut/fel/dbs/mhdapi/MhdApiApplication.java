package cz.cvut.fel.dbs.mhdapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MhdApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MhdApiApplication.class, args);
	}

}
