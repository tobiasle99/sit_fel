package cz.cvut.fel.dbs.mhdapi.entities;
import jakarta.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "prukaz")
public class Prukaz implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_prukazu")
    private int idPrukazu;

    @Column(name = "typ_prukazu")
    private String typPrukazu;

    @Column(name = "cislo_prukazu")
    private String cisloPrukazu;

    public Prukaz() {
    }

    public int getIdPrukazu() {
        return idPrukazu;
    }

    public String getTypPrukazu() {
        return typPrukazu;
    }

    public void setTypPrukazu(String typPrukazu) {
        this.typPrukazu = typPrukazu;
    }

    public String getCisloPrukazu() {
        return cisloPrukazu;
    }

    public void setCisloPrukazu(String cisloPrukazu) {
        this.cisloPrukazu = cisloPrukazu;
    }
}


