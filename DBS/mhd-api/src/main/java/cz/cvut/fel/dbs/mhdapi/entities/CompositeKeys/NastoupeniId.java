package cz.cvut.fel.dbs.mhdapi.entities.CompositeKeys;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class NastoupeniId implements Serializable {
    @Column(name = "cislo_kuponu")
    private String cisloKuponu;

    @Column(name = "id_jizdy")
    private Long idJizdy;

    public NastoupeniId() {
    }

    public NastoupeniId(String cisloKuponu, Long idJizdy) {
        this.cisloKuponu = cisloKuponu;
        this.idJizdy = idJizdy;
    }

    public String getCisloKuponu() {
        return cisloKuponu;
    }

    public void setCisloKuponu(String cisloKuponu) {
        this.cisloKuponu = cisloKuponu;
    }

    public Long getIdJizdy() {
        return idJizdy;
    }

    public void setIdJizdy(Long idJizdy) {
        this.idJizdy = idJizdy;
    }
}
