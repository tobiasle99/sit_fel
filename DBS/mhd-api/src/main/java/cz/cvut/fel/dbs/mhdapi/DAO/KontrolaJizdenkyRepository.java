package cz.cvut.fel.dbs.mhdapi.DAO;

import cz.cvut.fel.dbs.mhdapi.entities.CompositeKeys.KontrolaJizdenkyId;
import cz.cvut.fel.dbs.mhdapi.entities.KontrolaJizdenky;
import cz.cvut.fel.dbs.mhdapi.entities.Pasazer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface KontrolaJizdenkyRepository extends JpaRepository<KontrolaJizdenky, KontrolaJizdenkyId> {
    KontrolaJizdenky save(KontrolaJizdenky kontrolaJizdenky);

    List<KontrolaJizdenky> findByIdIdZamestnanceAndIdCisloKuponu(int idZamestnance, String cisloKuponu);

    @Query(value = "SELECT * FROM kontrola_jizdenky k WHERE k.id_zamestnance = :idZamestnance AND k.cislo_kuponu = :cisloKuponu", nativeQuery = true)
    List<KontrolaJizdenky> findAllById_IdZamestnanceAndId_CisloKuponu(@Param("idZamestnance") int idZamestnance, @Param("cisloKuponu") String cisloKuponu);

    @Query(value = "SELECT * FROM kontrola_jizdenky k WHERE k.id_zamestnance = :idZamestnance", nativeQuery = true)
    List<KontrolaJizdenky> findAllByIdIdZamestnance(@Param("idZamestnance") int idZamestnance);

    @Query(value = "SELECT * FROM kontrola_jizdenky k WHERE k.cislo_kuponu = :cisloKuponu", nativeQuery = true)
    List<KontrolaJizdenky> findAllByIdCisloKuponu(@Param("cisloKuponu") String cisloKuponu);
}

