package cz.cvut.fel.dbs.mhdapi.entities;
import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "zamestnanec")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="EMP_TYPE",
        discriminatorType = DiscriminatorType.STRING)
public class Zamestnanec {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_zamestnance")
    private int idZamestnance;

    @Column(name = "jmeno")
    private String jmeno;

    @Column(name = "prijmeni")
    private String prijmeni;

    @Column(name = "datum_narozeni")
    private Date datumNarozeni;

    @OneToOne()
    @JoinColumn(name = "id_prukazu", unique = true)
    private Prukaz prukaz;

    // Constructors, getters, and setters

    public Zamestnanec() {
    }

    public Zamestnanec(String jmeno, String prijmeni, Date datumNarozeni, Prukaz prukaz) {
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.datumNarozeni = datumNarozeni;
        this.prukaz = prukaz;
    }

    // Getters and setters

    public long getIdZamestnance() {
        return idZamestnance;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public Date getDatumNarozeni() {
        return datumNarozeni;
    }

    public void setDatumNarozeni(Date datumNarozeni) {
        this.datumNarozeni = datumNarozeni;
    }

    public Prukaz getPrukaz() {
        return prukaz;
    }

    public void setPrukaz(Prukaz prukaz) {
        this.prukaz = prukaz;
    }
}

