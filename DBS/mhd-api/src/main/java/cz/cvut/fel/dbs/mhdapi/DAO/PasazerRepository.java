package cz.cvut.fel.dbs.mhdapi.DAO;

import cz.cvut.fel.dbs.mhdapi.entities.Pasazer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface PasazerRepository extends JpaRepository<Pasazer, String> {
    @Query(value = "SELECT * FROM Pasazer p WHERE p.cislo_kuponu = :cisloKuponu", nativeQuery = true)
    Optional<Pasazer> findPasazerByCisloKuponu(@Param("cisloKuponu") String cisloKuponu);
}

