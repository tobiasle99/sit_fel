package cz.cvut.fel.dbs.mhdapi.entities;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "pasazer")
public class Pasazer implements Serializable {
    @Id
    @Column(name = "cislo_kuponu")
    private String cisloKuponu;

    @Column(name = "jmeno")
    private String jmeno;

    @Column(name = "prijmeni")
    private String prijmeni;

    @Column(name = "kategorie")
    private String kategorie;

    @Column(name = "zona_kuponu")
    private String zonaKuponu;

    @Column(name = "datum_platnosti_kuponu")
    private Date datumPlatnostiKuponu;

    @Column(name = "datum_posledni_kontroly")
    private Date datumPosledniKontroly;

    public Pasazer() {
    }

    public String getCisloKuponu() {
        return cisloKuponu;
    }

    public void setCisloKuponu(String cisloKuponu) {
        this.cisloKuponu = cisloKuponu;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public String getKategorie() {
        return kategorie;
    }

    public void setKategorie(String kategorie) {
        this.kategorie = kategorie;
    }

    public String getZonaKuponu() {
        return zonaKuponu;
    }

    public void setZonaKuponu(String zonaKuponu) {
        this.zonaKuponu = zonaKuponu;
    }

    public Date getDatumPlatnostiKuponu() {
        return datumPlatnostiKuponu;
    }

    public void setDatumPlatnostiKuponu(Date datumPlatnostiKuponu) {
        this.datumPlatnostiKuponu = datumPlatnostiKuponu;
    }

    public Date getDatumPosledniKontroly() {
        return datumPosledniKontroly;
    }

    public void setDatumPosledniKontroly(Date datumPosledniKontroly) {
        this.datumPosledniKontroly = datumPosledniKontroly;
    }


}

