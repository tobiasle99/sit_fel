package cz.cvut.fel.dbs.mhdapi.entities;

import cz.cvut.fel.dbs.mhdapi.entities.Trasa;
import jakarta.persistence.*;

import java.sql.Time;

@Entity
@Table(name = "jizdni_rad",
uniqueConstraints = @UniqueConstraint(
        name = "jizdni_rad_UQ_id_trasy_poradi_jizdy",
        columnNames = {"id_trasy", "poradi_jizdy"}
))
public class JizdniRad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_jizdniho_radu")
    private int idJizdnihoRadu;

    @ManyToOne
    @JoinColumn(name = "id_trasy", nullable = false)
    private Trasa trasa;

    @Column(name = "poradi_jizdy", nullable = false)
    private int poradiJizdy;

    @Column(name = "cas_zacatku", nullable = false)
    private Time casZacatku;

    public JizdniRad() {
    }

    public int getIdJizdnihoRadu() {
        return idJizdnihoRadu;
    }

    public void setIdJizdnihoRadu(int idJizdnihoRadu) {
        this.idJizdnihoRadu = idJizdnihoRadu;
    }

    public Trasa getTrasa() {
        return trasa;
    }

    public void setTrasa(Trasa trasa) {
        this.trasa = trasa;
    }

    public int getPoradiJizdy() {
        return poradiJizdy;
    }

    public void setPoradiJizdy(int poradiJizdy) {
        this.poradiJizdy = poradiJizdy;
    }

    public Time getCasZacatku() {
        return casZacatku;
    }

    public void setCasZacatku(Time casZacatku) {
        this.casZacatku = casZacatku;
    }
}
