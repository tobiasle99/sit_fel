package cz.cvut.fel.dbs.mhdapi.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import cz.cvut.fel.dbs.mhdapi.entities.Jizda;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface JizdaRepository extends JpaRepository<Jizda, Long> {
    @Query("SELECT j FROM Jizda j JOIN j.nastoupeni n JOIN n.pasazer p WHERE p.cisloKuponu = :couponNumber")
    List<Jizda> findJizdyByCouponNumber(@Param("couponNumber") String couponNumber);
}
