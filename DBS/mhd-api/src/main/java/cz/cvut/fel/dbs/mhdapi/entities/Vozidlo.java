package cz.cvut.fel.dbs.mhdapi.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "vozidlo", uniqueConstraints = @UniqueConstraint(name = "vozidlo_UQ_SPZ", columnNames = "SPZ")
)
public class Vozidlo {
    @Id
    @Column(name = "cislo_vozidla", length = 4, nullable = false)
    private String cisloVozidla;

    @Column(name = "spz", length = 8, nullable = false)
    private String SPZ;

    @Column(name = "znacka", length = 20, nullable = false)
    private String znacka;

    @Column(name = "model", length = 50, nullable = false)
    private String model;

    public Vozidlo() {
    }

    public String getCisloVozidla() {
        return cisloVozidla;
    }

    public void setCisloVozidla(String cisloVozidla) {
        this.cisloVozidla = cisloVozidla;
    }

    public String getSPZ() {
        return SPZ;
    }

    public void setSPZ(String SPZ) {
        this.SPZ = SPZ;
    }

    public String getZnacka() {
        return znacka;
    }

    public void setZnacka(String znacka) {
        this.znacka = znacka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}