package cz.cvut.fel.dbs.mhdapi.entities;
import jakarta.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "nadrizenost")
public class Nadrizenost {
    @ManyToOne
    @JoinColumn(name = "id_nadrizeneho")
    private Zamestnanec nadrizeny;

    @Id
    @OneToOne
    @JoinColumn(name = "id_podrizeneho")
    private Zamestnanec podrizeny;

    public Nadrizenost() {
    }

    public Zamestnanec getNadrizeny() {
        return nadrizeny;
    }

    public void setNadrizeny(Zamestnanec nadrizeny) {
        this.nadrizeny = nadrizeny;
    }

    public Zamestnanec getPodrizeny() {
        return podrizeny;
    }

    public void setPodrizeny(Zamestnanec podrizeny) {
        this.podrizeny = podrizeny;
    }
}

