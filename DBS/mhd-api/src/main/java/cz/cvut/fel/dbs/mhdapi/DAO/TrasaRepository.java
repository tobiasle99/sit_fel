package cz.cvut.fel.dbs.mhdapi.DAO;

import cz.cvut.fel.dbs.mhdapi.entities.Trasa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrasaRepository extends JpaRepository<Trasa, Integer> {
}
