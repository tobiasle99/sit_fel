package cz.cvut.fel.dbs;

import org.json.JSONArray;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class RandomDataGenerator {
    public void generateBuses(int loops) throws IOException {
        File busesSQL = new File("buses.sql");
        FileWriter busWrite = new FileWriter(busesSQL);
        List<String> busBrands = new ArrayList<>();
        List<String> busModels = new ArrayList<>();
        busBrands.add("Mercedes-Benz");
        busBrands.add("MAN");
        busBrands.add("Setra");
        busModels.add("12 E");
        busModels.add("Citaro hybrid");
        busModels.add("S 415 HDH");

        Random rand = new Random();
        for (int i = 1000; i < loops+1000; i++){
            String randomBusModel = busModels.get(rand.nextInt(0, busModels.size()));
            String randomBusBrand = busBrands.get(rand.nextInt(0, busBrands.size()));
            String busInsert = "INSERT INTO vozidlo (\"SPZ\", cislo_vozidla, znacka, model) VALUES (\'"+generateSPZ()+"\', "+
            i+", \'"+randomBusBrand+"\', \'"+randomBusModel+"\');\n";
            System.out.println(busInsert);
            busWrite.write(busInsert);
        }
    }

    private String generateSPZ(){
        String SPZ = "";
        Random rand = new Random();
        for (int i = 0; i < 8; i++){
            if (i == 1) {
                char randomLetter = (char) (rand.nextInt(26) + 'A');
                char randomUppercaseLetter = Character.toUpperCase(randomLetter);
                SPZ += randomUppercaseLetter;
            } else if (i == 3){
                SPZ += " ";
            } else {
                SPZ += Integer.toString(rand.nextInt(0,9));
            }
        }
        return SPZ;
    }

    public void generatePassengers() throws IOException {
        File passengersSQL = new File("passengers.sql");
        FileWriter passengersWrite = new FileWriter(passengersSQL);
        String json = new String(Files.readAllBytes(Paths.get("generated_data/names.json")));
        JSONArray namesArray = new JSONArray(json);

        List<String> categories = new ArrayList<>();
        categories.add("\'Senior\'");
        categories.add("\'Student\'");
        categories.add("NULL");

        Random rand = new Random();
        for (int i = 300; i < 1000; i++){
            String firstName = namesArray.getJSONObject(rand.nextInt(0,1000)).getString("first_name");
            String lastName = namesArray.getJSONObject(rand.nextInt(0,1000)).getString("last_name");

            int year = rand.nextInt(101) + 1920; // Generate a random year between 1920 and 2020
            int month = rand.nextInt(12) + 1; // Generate a random month between 1 and 12
            int day = rand.nextInt(28) + 1; // Generate a random day between 1 and 28 (since February has at most 28 days)

            String category = categories.get(rand.nextInt(0, categories.size()));

            LocalDate randomDate = LocalDate.of(year, month, day);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String formattedDate = randomDate.format(formatter);

            char randomLetter = (char) (rand.nextInt(5) + 'A');
            char randomUppercaseLetter = Character.toUpperCase(randomLetter);

            String couponCode = Integer.toString(i);
            while (couponCode.length()<10){
                couponCode = "0"+couponCode;
            }



            String passengerInsert = "INSERT INTO pasazer (cislo_kuponu, jmeno, prijmeni, kategorie, zona_kuponu," +
                    "datum_platnosti_kuponu) VALUES (\'"+couponCode+"\', \'" + firstName + "\', \'" + lastName + "\', "+
        category + ", '" + randomUppercaseLetter + "', '" + formattedDate + "');\n";
            passengersWrite.write(passengerInsert);
        }
    }

    public void generateIDCard() throws IOException {
        Random rand = new Random();
        File idCardSQL = new File("idCards.sql");
        FileWriter idCardsWrite = new FileWriter(idCardSQL);

        List<String> typesOfID = new ArrayList<>();
        typesOfID.add("\'Obcansky prukaz\'");
        typesOfID.add("\'Pas\'");
        typesOfID.add("\'Ridicsky prukaz\'");


        for (int i = 0; i < 1000; i++) {
            String chars = "0123456789";
            StringBuilder sb = new StringBuilder();
            int length = 10;
            for (int j = 0; j < length; j++) {
                int index = rand.nextInt(chars.length());
                char randomChar = chars.charAt(index);
                sb.append(randomChar);
            }
            String id_number = sb.toString();

            String typeOfID = typesOfID.get(rand.nextInt(0, typesOfID.size()));
            String idInsert = "INSERT INTO prukaz (id_prukazu, typ_prukazu, cislo_prukazu) VALUES ("+(i+1)+", " +
                    typeOfID + ", \'" + id_number + "\');\n";
            idCardsWrite.write(idInsert);
        }
    }

    public void generateEmployee() throws IOException {
        File employeesSQL = new File("employees.sql");
        File driversSQL = new File("driver.sql");
        File revisorsSQL = new File("revisors.sql");
        FileWriter employeesWrite = new FileWriter(employeesSQL);
        FileWriter driversWrite = new FileWriter(driversSQL);
        FileWriter revisorsWrite = new FileWriter(revisorsSQL);
        String json = new String(Files.readAllBytes(Paths.get("generated_data/names.json")));
        JSONArray namesArray = new JSONArray(json);

        List<String> categories = new ArrayList<>();

        Random rand = new Random();
        for (int i = 0; i < 1000; i++){
            String firstName = namesArray.getJSONObject(rand.nextInt(0,1000)).getString("first_name");
            String lastName = namesArray.getJSONObject(rand.nextInt(0,1000)).getString("last_name");

            int year = rand.nextInt(80) + 1920; // Generate a random year between 1920 and 2020
            int month = rand.nextInt(12) + 1; // Generate a random month between 1 and 12
            int day = rand.nextInt(28) + 1; // Generate a random day between 1 and 28 (since February has at most 28 days)
            LocalDate randomDate = LocalDate.of(year, month, day);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String formattedDate = randomDate.format(formatter);

            String emloyeeInsert = "INSERT INTO zamestnanec (id_zamestnance, jmeno, prijmeni, id_prukazu, datum_narozeni)" +
                    " VALUES ("+(i+1)+", \'" + firstName + "\', \'" + lastName + "\', "+ (i+1) +", \'" + formattedDate + "\');\n";
            employeesWrite.write(emloyeeInsert);

            if (i < 500){
                char randomLetter = (char) (rand.nextInt(4) + 'A');
                char randomUppercaseLetter = Character.toUpperCase(randomLetter);
                String driverInsert = "INSERT INTO ridic (id_zamestnance, kategorie_ridicskeho_prukazu)" +
                        " VALUES ("+(i+1)+", \'" + randomUppercaseLetter + "\');\n";
                driversWrite.write(driverInsert);
            }
            if (i > 400) {
                String revisorInsert = "INSERT INTO revizor (id_zamestnance)" +
                        " VALUES ("+(i+1)+");\n";
                revisorsWrite.write(revisorInsert);
            }
        }
    }

    public void generateTimetable() throws IOException {
        Random rand = new Random();
        File timetablesSQL = new File("timetables.sql");
        FileWriter timetablesWrite = new FileWriter(timetablesSQL);
        for (int i = 1; i < 5000; i++){
            int line_id = rand.nextInt(1, 501);
            int orderInTimetable = rand.nextInt(1, 20);
            LocalTime randomTime = LocalTime.of(
                    rand.nextInt(0, 24), // hour
                    rand.nextInt(0, 60), // minute
                    rand.nextInt(0, 60)  // second
            );
            String formattedTime = randomTime.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
            String timetableInsert = "INSERT INTO jizdni_rad (id_jizdniho_radu, id_trasy, poradi_jizdy, cas_zacatku)" +
                    " VALUES ("+i+", "+line_id+", "+ orderInTimetable+", \'"+formattedTime+"\');\n";
            timetablesWrite.write(timetableInsert);
        }
    }

    public void generateTrips() throws IOException {
        Random rand = new Random();
        File tripsSQL = new File("trips.sql");
        FileWriter tripsWrite = new FileWriter(tripsSQL);
        for (int i = 1; i < 32000; i++){
            int timetable_id = rand.nextInt(1, 5001);
            int vehicleNumber = rand.nextInt(1000, 5000);
            int employeeID = rand.nextInt(1, 500);
            int year = rand.nextInt(80) + 1920; // Generate a random year between 1920 and 2020
            int month = rand.nextInt(12) + 1; // Generate a random month between 1 and 12
            int day = rand.nextInt(28) + 1; // Generate a random day between 1 and 28 (since February has at most 28 days)
            LocalDate randomDate = LocalDate.of(year, month, day);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String formattedDate = randomDate.format(formatter);
            String tripInsert = "INSERT INTO jizda (id_jizdy, id_jizdniho_radu, cislo_vozidla, id_zamestnance, datum)" +
                    " VALUES ("+i+", "+timetable_id+", "+ vehicleNumber+", "+employeeID+", \'"+formattedDate+"\');\n";
            tripsWrite.write(tripInsert);
        }
    }

    public void generateStops() throws IOException {
        Random rand = new Random();
        File stopSQL = new File("stoppages.sql");
        FileWriter stopsWrite = new FileWriter(stopSQL);
        for (int i = 1; i < 15000; i++){
            int trip_id = rand.nextInt(1, 31994);
            int platform_id = rand.nextInt(1, 15491);

            int year = rand.nextInt(80) + 1920; // Generate a random year between 1920 and 2020
            int month = rand.nextInt(12) + 1; // Generate a random month between 1 and 12
            int day = rand.nextInt(28) + 1; // Generate a random day between 1 and 28 (since February has at most 28 days)
            LocalDate randomDate = LocalDate.of(year, month, day);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String formattedDate = randomDate.format(formatter);

            LocalTime randomTime = LocalTime.of(
                    rand.nextInt(0, 24), // hour
                    rand.nextInt(0, 60), // minute
                    rand.nextInt(0, 60)  // second
            );
            String formattedTime = randomTime.format(DateTimeFormatter.ofPattern("HH:mm:ss"));

            String stopInsert = "INSERT INTO zastaveni (id_zastaveni, id_nastupiste, id_jizdy, cas, datum)" +
                    " VALUES ("+i+", "+platform_id+", "+ trip_id+", \'"+formattedTime+"\', \'"+formattedDate+"\');\n";
            stopsWrite.write(stopInsert);
        }
    }

    public void generateChecks() throws IOException {
        Random rand = new Random();
        File checksSQL = new File("checks.sql");
        FileWriter checksWrite = new FileWriter(checksSQL);
        for (int i = 1; i < 50000; i++){
            int employee_id = rand.nextInt(500, 1000);
            int passenger_id = rand.nextInt(1, 10000);
            String couponCode = Integer.toString(passenger_id);
            while (couponCode.length()<10){
                couponCode = "0"+couponCode;
            }


            int year = rand.nextInt(80) + 1920; // Generate a random year between 1920 and 2020
            int month = rand.nextInt(12) + 1; // Generate a random month between 1 and 12
            int day = rand.nextInt(28) + 1; // Generate a random day between 1 and 28 (since February has at most 28 days)
            LocalDate randomDate = LocalDate.of(year, month, day);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String formattedDate = randomDate.format(formatter);

            LocalTime randomTime = LocalTime.of(
                    rand.nextInt(0, 24), // hour
                    rand.nextInt(0, 60), // minute
                    rand.nextInt(0, 60)  // second
            );
            String formattedTime = randomTime.format(DateTimeFormatter.ofPattern("HH:mm:ss"));

            String checkInsert = "INSERT INTO kontrola_jizdenky (id_zamestnance, cislo_kuponu, cas, datum)" +
                    " VALUES ("+employee_id+", \'"+ couponCode+"\', \'"+formattedTime+"\', \'"+formattedDate+"\');\n";
            checksWrite.write(checkInsert);
        }
    }

    public void generateBosses() throws IOException {
        Random rand = new Random();
        File bossSQL = new File("boss.sql");
        FileWriter bossWrite = new FileWriter(bossSQL);

        for (int i = 1; i < 1000; i++){
            for (int j = i; j < 1000; j++){
                int chance = rand.nextInt(1, 100);
                if (chance > 98){
                    String bossInsert = "INSERT into nadrizenost (id_nadrizeneho, id_podrizeneho) VALUES ("+i+", "+j+");\n";
                    bossWrite.write(bossInsert);
                    break;
                }
            }
        }
    }

    public void generatePassengerInVehicle() throws IOException {
        Random rand = new Random();
        File gotInVehicleSQL = new File("generated_data/gotInVehicle.sql");
        FileWriter gotInVehicleWrite = new FileWriter(gotInVehicleSQL);
        for (int i = 1; i < 1000; i++){
            int trip_id = rand.nextInt(2, 10);
            int passenger_id = rand.nextInt(300, 500);
            String couponCode = Integer.toString(passenger_id);
            while (couponCode.length()<10){
                couponCode = "0"+couponCode;
            }

            String gotOnInsert = "INSERT INTO nastoupeni (cislo_kuponu, id_jizdy)" +
                    " VALUES (\'"+ couponCode+"\', "+trip_id+");\n";
            gotInVehicleWrite.write(gotOnInsert);
        }
    }
    public static void main(String[] args) throws IOException {
        RandomDataGenerator randomDataGenerator = new RandomDataGenerator();
        randomDataGenerator.generateEmployee();

    }
}
