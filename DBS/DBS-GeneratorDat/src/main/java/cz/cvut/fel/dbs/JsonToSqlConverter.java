package cz.cvut.fel.dbs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonToSqlConverter {

    public static void main(String[] args) throws IOException {
        // Read JSON file
        String json = new String(Files.readAllBytes(Paths.get("stops.json")));

        // Parse JSON data
        JSONObject stopsObject = new JSONObject(json);
        JSONArray stopGroups = new JSONArray(stopsObject.getJSONArray("stopGroups"));

        // Create SQL file and writer
        File stationsSQL = new File("stations.sql");
        File stopsSQL = new File("stops.sql");
        File linesSQL = new File("lines.sql");
        FileWriter stopsWriter = new FileWriter(stopsSQL);
        FileWriter stationsWriter = new FileWriter(stationsSQL);
        FileWriter linesWriter = new FileWriter(linesSQL);

        // Alt indexes
        int stop_id = 1;
        int line_id = 1;

        // Addes lines
        List<Integer> lines = new ArrayList<>();

        // Loop through JSON array and write SQL insert statements
        for(int i=0; i<stopGroups.length(); i++) {
            JSONObject stationObject = stopGroups.getJSONObject(i);
            String stationInsert = "INSERT INTO zastavka (id_zastavky, jmeno, mesto) VALUES ("+(i+1)+", \'"+
                    stationObject.getString("uniqueName") + "\', \'" + stationObject.getString("municipality")+"\');\n";
            stationsWriter.write(stationInsert);
            JSONArray stopsArray = stationObject.getJSONArray("stops");
            for (int j = 0; j < stopsArray.length(); j++){
                JSONObject stopObject = stopsArray.getJSONObject(j);
                String platform;
                if (stopObject.has("platform")) {
                    platform = stopObject.getString("platform");
                } else {
                    platform = "1";
                }
                String stopsInsert = "INSERT INTO nastupiste (id_nastupiste, id_zastavky, znaceni) VALUES ("+stop_id+", "+
                        (i+1)+", \'"+platform + "\');\n";
                stop_id++;
                stopsWriter.write(stopsInsert);
                JSONArray linesArray = stopObject.getJSONArray("lines");
                for (int m = 0; m < linesArray.length(); m++){
                    JSONObject lineObject = linesArray.getJSONObject(m);
                    if (!lines.contains(lineObject.getInt("id"))) {
                        lines.add(lineObject.getInt("id"));
                        String linesInsert = "INSERT INTO trasa (id_trasy, nazev) VALUES ("+line_id+", \'"
                                +lineObject.getString("name") + "\');\n";
                        line_id++;
                        linesWriter.write(linesInsert);
                    }
                }
            }
        }

        // Close writer
        linesWriter.close();
        stopsWriter.close();
        stationsWriter.close();
    }

}
